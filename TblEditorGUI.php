<?php

session_start();
ob_start();

require_once './config.php';
require_once './_pdo.php';

require_once './src/D2Functions.php';
require_once './src/D2Crud.php';

error_reporting(E_ERROR | E_PARSE);
set_time_limit(-1);
ini_set('max_input_time', '-1');
ini_set('max_execution_time', '0');

define('DB_FILE', $_SESSION['modname'] . ".db");
try {
    PDO_Connect("sqlite:" . DB_FILE);
} catch (Exception $e) {
    echo "Connection error: " . $e->getMessage();
}

$tbl = $_SESSION['tbl'];
$tableNames = [];
foreach (glob($tbl . "*.tbl") as $filename) {
    $tableInfo = pathinfo($filename);
    $tableNames[] = $tableInfo['filename'];
}

$tableNames = ['patchstring'];

// Create an associative array to store rows for each table
$tableRows = [];

// Iterate over the table names
foreach ($tableNames as $tableName) {
    $crud = new CRUD($tableName);
    $tableRows[$tableName] = $crud->readAll();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://bootswatch.com/4/lumen/bootstrap.min.css">
    <title>Editable Tables</title>
</head>
<body>
    <div class="container">
        <h2>Editable Tables</h2>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <?php $firstTab = true; ?>
            <?php foreach ($tableNames as $tableName): ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo $firstTab ? 'active' : ''; ?>" id="<?php echo $tableName; ?>-tab" data-toggle="tab" href="#<?php echo $tableName; ?>" role="tab" aria-controls="<?php echo $tableName; ?>" aria-selected="<?php echo $firstTab ? 'true' : 'false'; ?>"><?php echo $tableName; ?></a>
                </li>
                <?php $firstTab = false; ?>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content" id="myTabContent">
            <?php $firstTab = true; ?>
            <?php foreach ($tableNames as $tableName): ?>
                <div class="tab-pane fade <?php echo $firstTab ? 'show active' : ''; ?>" id="<?php echo $tableName; ?>" role="tabpanel" aria-labelledby="<?php echo $tableName; ?>-tab">
                    <form method="post" action="TblEditor.php"> <!-- Replace 'process.php' with the PHP file to handle form submission -->
                        <div class="form-row mb-3">
                            <div class="col">
                                <input type="text" name="key" class="form-control" placeholder="Key" required>
                                <input type="hidden" name="tableName" value="<?php echo $tableName?>">
                            </div>
                            <div class="col">
                                <textarea name="string" class="form-control" placeholder="String" required></textarea>
                            </div>
                            <div class="col-auto">
                                <button type="submit" name="add" class="btn btn-success">Add</button>
                            </div>
                        </div>
                    </form><!-- comment -->
                    <form method="post" action=""> <!-- Replace 'process.php' with the PHP file to handle form submission -->
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Key</th>
                                    <th>String</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($tableRows[$tableName] as $index => $row){ ?>
                                    <tr class="table-row-<?php echo $index; ?>">
                                        <td><?php echo $row['rowid']; ?></td>
                                        <td><input type="text" name="key[<?php echo $tableName; ?>][]" class="form-control" value="<?php echo $row['Key']; ?>"></td>
                                        <td><textarea name="string[<?php echo $tableName; ?>][]" class="form-control"><?php echo $row['String']; ?></textarea></td>
                                        <td>
                                            <button type="submit" name="update" class="btn btn-primary" value="<?php echo $row['rowid']; ?>">Update</button>
                                            <button type="submit" name="delete" class="btn btn-danger" value="<?php echo $row['rowid']; ?>">Delete</button>
                                            <input type="hidden" value="<?php echo $row['Key']?>" name="keycode">
                                            <input type="hidden" name="keytable" value="<?php echo $tableName?>">
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <?php $firstTab = false; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
  
            // Handle Delete button click
            $('button[name="delete"]').click(function(event) {
              event.preventDefault(); // Prevent the default form submission

              var key = $(this).next().val(); // Get the value of the input after the Delete button
              var tableName = $(this).next().next().val(); // Get the value of the input after the key

              console.log(key);
              console.log(tableName);

              $.get('/TblEditor.php', { cmd: 'delete', t: tableName, k: key })
                    .done(function(response) {
                      // Handle success response
                      location.reload();
                    })
                    .fail(function(xhr, status, error) {
                      // Handle error response
                    });
              });    
              
              
              // Handle Update button event
            $('button[name="update"]').click(function(event) {
              event.preventDefault(); // Prevent the default form submission

              var key = $(this).next().val(); // Get the value of the input after the Delete button
              var tableName = $(this).next().next().val(); // Get the value of the input after the key

              console.log(key);
              console.log(tableName);

              $.post('/TblEditor.php', { cmd: 'update', t: tableName, k: key })
                    .done(function(response) {
                      // Handle success response
                      location.reload();
                    })
                    .fail(function(xhr, status, error) {
                      // Handle error response
                    });
              });               

        });

 
    </script>
</body>
</html>