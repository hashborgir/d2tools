<?php

ini_set('post_max_size', '2M');
ini_set('upload_max_filesize', '2M');


if (!empty($_FILES)) {

	var_dump($_FILES);
	die();
	
	
	$path = getcwd() . "/";
	$range = range(1, 2.5, mt_rand(3, 8) / 100);
	$item = $_FILES['tmp_name'];

	$tmpdir = pathinfo($path . $item)['filename'];

// undelee and create directory if doing same item over again
	@unlink($tmpdir);
	@mkdir($tmpdir);

	$cmd = "convert ";

// Generate glow frames for item
	foreach ($range as $r) {
		$tmpfile = $path . "$tmpdir/$r-$item";
		exec("glow -a $r -s 40 $item $tmpfile");
	}


// Generate build command to process glow frames for item
// into frame1 glowing up
	foreach ($range as $r) {
		$tmpfile = "$tmpdir/$r-$item";
		$cmd .= "$tmpfile ";
	}
	$cmd .= " +append $tmpdir/frame1.png";
	exec($cmd);

	$cmd = "convert ";
// Generate build command to process glow frames for item
// into frame1 glowing up
	foreach (array_reverse($range) as $r) {
		$tmpfile = "$tmpdir/$r-$item";
		$cmd .= "$tmpfile ";
	}
	$cmd .= "+append $tmpdir/frame2.png";
	exec($cmd);

	$cmd = '';
	$cmd = "convert $tmpdir/frame1.png $tmpdir/frame2.png +append $tmpdir/frame_final.png";
	exec($cmd);
}
?>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Hash Borgir">
		<title>Diablo 2 Animated Item Creator</title>


		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

		<!-- Favicons -->



		<style>
			.bd-placeholder-img {
				font-size: 1.125rem;
				text-anchor: middle;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}

			@media (min-width: 768px) {
				.bd-placeholder-img-lg {
					font-size: 3.5rem;
				}
			}
		</style>
		<!-- Custom styles for this template -->
		<link href="starter-template.css" rel="stylesheet">
	</head>
	<body>


		<main role="main" class="container">

			<div class="row">
				<div class="col">
					<h1>Diablo 2 Animated Item Creator</h1>
					<p>Upload an item graphic in png/bmp/jpg/pcx format. Tool will generate a large file containing all the frames in a row which can be loaded into SixDice, and split at whatever size you need, to generate a DC6 with frames.</p>

					<form action="/GlowItems.php" method="post" enctype="multipart/form-data">
						<input type="file" name="file" class="file">
						<input type="submit" name="submit" value="Upload">
					</form>	

				</div>

			</div>


		</main><!-- /.container -->
		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script></body>
</html>