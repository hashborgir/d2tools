/*

 Copyright (C) 2021 Hash Borgir

 This file is part of D2Modder

 Redistribution and use in source and binary forms, with
 or without modification, are permitted provided that the
 following conditions are met:

 * Redistributions of source code must retain the above
 copyright notice, this list of conditions and the
 following disclaimer.

 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the
 following disclaimer in the documentation and/or other
 materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY!

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
// highlight json dump for item debugger
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


function search() {
    $.get("/ajax/uniqueitems.php?cmd=search&search=" + searchbox.value, function (data) {
        $('.uniqueitems-select').html(data)
    });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function () {
    $('.form-text').hide();
    $('.help').click(function () {
        // $('.form-text').slideToggle();
    });

    $('.op1').val(this.checked);
    $('.op1').change(function () {
        if (this.checked) {
            $('option[disabled="disabled"]').hide();
        } else {
            $('option[disabled="disabled"]').show();
        }
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    // armor/misc/weapons select dropdown change function
    $('.w-select').change(function () {
        $('.a-select,.m-select').each(function (i, v) {
            v.value = '';
            v.required = '';
        });
        x = $(this).find('option:selected').text();
        $("*[name='*type']").val(x);
        $('.itemtype').html("(" + x + ")");

    });
    $('.a-select').change(function () {
        $('.w-select,.m-select').each(function (i, v) {
            v.value = '';
            v.required = '';
        });
        x = $(this).find('option:selected').text();
        $("*[name='*type']").val(x);
        $('.itemtype').html("(" + x + ")");
    });
    $('.m-select').change(function () {
        $('.w-select,.a-select').each(function (i, v) {
            v.value = '';
            v.required = '';
        });
        x = $(this).find('option:selected').text();
        $("*[name='*type']").val(x);
        $('.itemtype').html("(" + x + ")");
    });

    $(".btnconfig").click(function () {
        $(".ctrl-config").hide();
        $("#loading").show();

    });

    $('.help').click(function () {
        $(".fa-help").remove();
        $(this).next().fadeToggle().focus().css({
            "position": "absolute",
            "z-index": "1000",
            "background": "#eee",
            "color": "black !important",
            "border": "1px solid #aaa",
            "width": "300px",
        })
    });

    $('.form-text').click(function () {
        $(this).fadeOut("slow");
    });

    $('input[name="invfile"]').keyup(function () {
        invImg = docpath + '/img/items/' + this.value + ".png";
        $(".item").attr("src", invImg);
    });

    // every time we change a prop dropdown,send to server, grab string, update item display
    $('select[name^="prop"]').change(function () {
        prop = capitalizeFirstLetter($(this).attr("name"));
        propNum = prop.substring(4);
        par = $(this).next().val();
        min = $(this).next().next().val();
        max = $(this).next().next().next().val();

        val = escape(this.value);
        $.get(`/ajax/uniqueitems.php?cmd=getString&prop=${val}&par=${par}&min=${min}&max=${max}`, function (data) {
            console.log(data);
            $(`.item_stats ul .${prop}`).html(data);
        });
    });

    // Properties's par/min/max onchange function, updated item string stats
    function createChangeHandler(propNumber) {
        var prop = "Prop" + propNumber;

        return function () {
            var par = $('.par' + propNumber).val() || 0;
            var min = $('.min' + propNumber).val();
            var max = $('.max' + propNumber).val();
            var val = escape($("*[name='prop" + propNumber + "'").val());

            $.get(`/ajax/uniqueitems.php?cmd=getString&prop=${val}&par=${par}&min=${min}&max=${max}`, function (data) {
                console.log(data);
                $(`.item_stats ul .${prop}`).html(data);
            });
        };
    }
    // Run the loop 12 times for 12 props, and handle the change event
    for (var i = 1; i <= 12; i++) {
        $('.par' + i + ', .min' + i + ', .max' + i).change(createChangeHandler(i));
    }

    // index, lvl, lvl req change functions
    $("input[name='index']").keyup(function () {
        $(".itemindex").html($(this).val());
    });
    $("input[name='code']").keyup(function () {
        $(".itemcode").html("Item Code: (" + $(this).val() + ")");
    });
    $('input[name="lvl"]').keyup(function () {
        $(".itemlvl").html("Level: " + $(this).val());
    });
    $('input[name="lvl req"]').keyup(function () {
        $(".itemlvlreq").html("Level Required: " + $(this).val());
    });



    // cmd = getUniqueItem
    $('.uniqueitems-select').change(function () {

        $(".item").attr("src", "/img/items/1.png");
        $(".item_debug_link").attr('href', "/ajax/uniqueitems.php?cmd=getUniqueItem&index=" + this.value)
        $.get("/ajax/uniqueitems.php?cmd=getUniqueItem&index=" + this.value, function (data) {
            debugData = (JSON.stringify(data, null, 4));
            $('.debug, .debug_preview').html(syntaxHighlight(debugData));
            props = data.props;

            invImg = docpath + '/img/items/' + data.invfile + ".png";
            $(".item").attr("src", invImg);
            $(".item").onerror = function () {
                invImg = "/img/items/" + data.invfile + ".png";
                this.src = invImg;
            };

            type = data["*type"];
            lvlreq = data["lvl req"];
            base = data.baseItemInfo;

            $('.item_stats ul .itemindex').html(data.index);
            $('.item_stats ul .itemtype').html(`(${type})`);
            $('.item_stats ul .itemlvl').html(`Level: ${base.level}`);
            $('.item_stats ul .itemlvlreq').html(`Level Required: ${lvlreq}`);
            $('.item_stats ul .itemcode').html(`Item Code: (${base.code})`);

            if (base.reqstr) {
                $('.item_stats ul .itemstreq').html(`Required Strength: ${base.reqstr}`);
            }
            if (base.reqdex) {
                $('.item_stats ul .itemdexreq').html(`Required Dexterity: ${base.reqdex}`);
            }
            if (base['gemsockets']) {
                $('.item_stats ul .itemgemsockets').html(`Max Sockets: (${base.gemsockets})`);
            }

            // THIS LINE BREAKS hidden fields and set them to blank.
            $('.props-container select, .props-container input').val('');

            // clear some blue text, I forgot what
            $('.item_stats ul .blue').each(function (i, v) {
                $(v).html("");
            });

            // add the rest of the prop li's here
            $.each(props, function (i, v) {
                if (v.string) {
                    $(`.item_stats ul .${i}`).html(v.string[0]);
                }
            });

            $.each(data, function (i, v) {
                $('*[name="' + i + '"]').val(v);
                if (i == 'code') {
                    $('select[name="code[]"]').val(v);
                    $('select[name="code[]"]').prop('required', '');
                }
            });
        });
    });

    // Debug preview slide down box
    $(".btnDebug").click(function () {
        $(".debug_preview").slideToggle();
    });

    // cmd = sortBy
    $('input[name="sort"]').change(function () {
        $.get("/ajax/uniqueitems.php?cmd=sortBy&sort=" + this.value, function (data) {
            $('.uniqueitems-select').html(data)
        });
    });

    // cmd = viewOnly
    $('input[name="view"]').change(function () {
        $.get("/ajax/uniqueitems.php?cmd=viewOnly&view=" + this.value, function (data) {
            $('.uniqueitems-select').html(data)
        });
    });

    searchbox = document.getElementById('search');
    searchbox.addEventListener('input', search);

    $('input[name="theme"]').change(function () {
        modname = $('input[name="modname"]').val();
        $.get("/res/css.php?theme=" + this.value + "&modname=" + modname, function (data) {
            location.reload();
        });
    });

    //genDocUniqueItems
    $("#Unique .btnDocs").click(function () {
        window.open('/genDocs.php?cmd=genDocUniqueItems', '_blank');
    });

    // Delete button event
    $('.btn-delete').click(function (event) {
        event.preventDefault(); // Prevent default button click behavior

        // delete item index
        $.get('/ajax/uniqueitems.php?cmd=delete&index=' + $(".uniqueform input[name='index']").val(), function (response) {
            // Handle the response from the server
            // remove option from list
            $('.uniqueitems-select option[value="' + $(".uniqueform input[name='index']").val() + '"]').remove();
            console.log(response);
        });
    });

    // Save button event
    $('.btn-save').click(function (event) {
        event.preventDefault(); // Prevent default button click behavior

        // Get form data
        var formData = $(".uniqueform").serialize();

        // delete item index
        $.get('/ajax/uniqueitems.php?cmd=save&' + formData, function (response) {
            // Handle the response from the server
            console.log(response);
        });
    });


    // Chars.php Character Editor AJAXIFY
    // Prevent default form submit
    $('form').submit(function (event) {
        event.preventDefault();
    });

    $(".wpcheck").change(function () {
        // Toggle the value between 0 and 1
        var value = $(this).is(":checked") ? 1 : 0;

        // Get the name of the checkbox
        var name = $(this).attr("name");

        // Get the name of the difficulty
        var diff = $(this).attr("diff");

        var parentFormId = $(this).closest("form").attr("id");
        var parentFormClass = $(this).closest("form").attr("class");

        var filePath = $(`#${parentFormId} input[name='filePath']`).val();

        // Construct the URL for the GET request
        var url = "/saveCharacter.php?cmd=wp&name=" + name + "&value=" + value + "&filePath=" + filePath + "&diff=" + diff;


        // Send the GET request
        $.get(url, function (response) {
            // Handle the response if needed
            console.log(response);
        });
    });

    $(".qcheck").change(function () {
        // Toggle the value between 0 and 1
        var value = $(this).is(":checked") ? 1 : 0;

        // Get the name of the checkbox
        var name = $(this).attr("name");

        // Get the name of the difficulty
        var diff = $(this).attr("diff");


        var parentFormId = $(this).closest("form").attr("id");
        var parentFormClass = $(this).closest("form").attr("class");

        var filePath = $(`#${parentFormId} input[name='filePath']`).val();


        // Construct the URL for the GET request
        var url = "/saveCharacter.php?cmd=q&name=" + name + "&value=" + value + "&filePath=" + filePath + "&diff=" + diff;


        // Send the GET request
        $.get(url, function (response) {
            // Handle the response if needed
            console.log(response);
        });
    });


    $(".form-check-input").click(function () {
        var $checkbox = $(this);
        var currentValue = $checkbox.val();

        // Toggle the value between 1 and 0
        var newValue = currentValue === '1' ? '0' : '1';

        // Update the value of the checkbox
        $checkbox.val(newValue);
    });


    // Get the value of the input named "filePath"
    function constructURL(cmd, name, value, filePath) {
        return "/saveCharacter.php?cmd=" + cmd + "&name=" + name + "&value=" + value + "&filePath=" + filePath;
    }

    $(".charform input[name='Difficulty'], .charform .skill, .CharacterName, .CharacterClass, .CharacterLevel, .stats input, .form-check-input, .statsdiv input,.setallskills").change(function () {
        var name = $(this).attr("name");
        var newValue = $(this).val();

        var cmd = $(this).attr("cmd") || name;
        var parentFormId = $(this).closest("form").attr("id");
        var parentFormClass = $(this).closest("form").attr("class");

        var filePath = $(`#${parentFormId} input[name='filePath']`).val();

        var url = constructURL(cmd, name, newValue, filePath);

        console.log(url);
        console.log(newValue)

        $.get(url, function (response) {
            if (cmd == 'CharacterName') {
                location.reload();
            }
            console.log(response);
        });
    });

    $('.q_all, .wp_all').click(function () {
        var parentForm = $(this).closest('form');
        var targetClass = $(this).hasClass('q_all') ? '.qcheck' : '.wpcheck';
        var isChecked = $(this).prop('checked');

        parentForm.find(targetClass).prop('checked', isChecked);
    });


    function updateAllCheckbox(parentFormId, checkboxClass, allCheckboxClass) {
        var allChecked = $(`#${parentFormId} ${checkboxClass}:checked`).length === $(`#${parentFormId} ${checkboxClass}`).length;
        var checkedValue = allChecked ? 'checked' : '';
        var val = allChecked ? 1 : 0;
        $(`#${parentFormId} ${allCheckboxClass}`).prop('checked', checkedValue).val(val);
    }

    $('.wpcheck').change(function () {
        var parentFormId = $(this).closest("form").attr("id");
        updateAllCheckbox(parentFormId, '.wpcheck', '.wp_all');
    });

    $('.qcheck').change(function () {
        var parentFormId = $(this).closest("form").attr("id");
        updateAllCheckbox(parentFormId, '.qcheck', '.q_all');
    });


    $('.setallskills').change(function () {
        var value = $(this).val();
        var parentFormId = $(this).closest("form").attr("id");
        $(`#${parentFormId} .skill`).attr('value', value);
    });



    // colorize the acts
    var acts = ['Act 1', 'Act 2', 'Act 3', 'Act 4', 'Act 5'];
    for (var i = 0; i < acts.length; i++) {
        var act = acts[i];
        var className = 'act' + (i + 1);
        // Find the checkbox with a label containing the act
        $('input[type="checkbox"] + label:contains("' + act + '")').each(function () {
            // Add a class to the parent div
            $(this).parent().addClass(className);
        });
    }

});// end document.ready