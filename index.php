<?php

session_start();
/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_WARNING);
ini_set('log_errors', 1);

include "./_pdo.php";
include "./config.php";
require_once "./vendor/autoload.php";

if (!isset($_SESSION['modname']) || (!file_exists(APP_DB)) || (!file_exists($_SESSION['modname'] . ".db"))) {
    // first load, no active mod, go to switchmods to select mod
    header("Location: /switchMods.php");
} else {
    PDO_Connect("sqlite:" . APP_DB);
    $sql = "SELECT * FROM D2Modder ORDER BY lastused DESC LIMIT 1";
    $lastUsedMod = PDO_FetchRow($sql);

    $_SESSION['modname'] = $lastUsedMod['modname'];
    $_SESSION['path'] = $lastUsedMod['path'];

    $sql = "SELECT theme FROM `D2Modder` WHERE modname= :modname";
    $res = PDO_FetchAll($sql, ['modname' => $_SESSION['modname']]);
   
    if ($res[0]['theme'] == 1) {
        $css = 'dark.css';
    } else {
        $css = '';
    }

    define('FILTER_PROPERTIES_FILE', 'filterProperties.txt');
    define('DB_FILE', $_SESSION['modname'] . ".db");
    define('TXT_PATH', $_SESSION['path']);

    require_once "./src/D2Functions.php";
    require_once "./src/D2Database.php";
    require_once './src/D2Files.php';
    require_once './src/D2TxtParser.php';
    require_once './src/D2ItemDesc.php';
    require_once './src/D2Char.php';
    require_once './src/D2CharStructureData.php';
    
    $D2Files = new D2Files();
    $charFiles = $D2Files->getSaveFiles();

    foreach ($charFiles as $charFile) {
        $charData[] = new D2Char($charFile);  // $charData goes into chars.php tab
        //$charData[] = $_charData->cData;
    }


//     dump($charData[0]->charData);
//     dump($charData[1]->charData);
//     die();

    $idesc = new D2ItemDesc();
    $db = new D2Database();
    $parser = new D2TxtParser();

//	$a = ["bighead1","fallen1","corruptrogue1","goatman1","fallenshaman1","fetish1","cr_archer1","cr_lancer1","vulture1","bloodraven","andariel","smith","vulture1","blunderbore1","duriel","fetishshaman1","councilmember1","mephisto","megademon1","regurgitator1","diablo","hephasto","vilemother1","vilechild1","overseer1","imp1","succubus1","succubuswitch1","putriddefiler1","painworm1","minion1","baalcrab"];
//
//foreach ($a as $x){
//	$z = (PDO_FetchRow("SELECT `hcIdx`,`NameStr` FROM monstats WHERE `Id`='$x'"));
//	echo "<pre>".$z['hcIdx']."\t".$z['NameStr']."<br>";
//}
//
//die();



    $armor = PDO_FetchAll('SELECT * FROM armor WHERE spawnable=1');
    $misc = PDO_FetchAll('SELECT * FROM misc WHERE spawnable=1
        AND type <> "stst"
        AND type <> "sum0"
        AND type <> "prtl"
        AND type <> "xfor"
        AND type <> "rext"
        AND type <> "gext"
        AND type <> "ques"
        AND type <> "key"
        AND type <> "body"
        ');
    $weapon = PDO_FetchAll('SELECT * FROM weapons WHERE spawnable=1');
    $uniqueitems = PDO_FetchAll("SELECT `index`,`code` FROM uniqueitems WHERE `enabled`='1' ORDER BY  `index` ASC");

    $prop = $parser->filterProps('Properties.txt');

    // If there's data, process it and save
    if (!empty($_POST)) {
        // save db name from post into conf file... why?

        require_once './src/D2Char.php';
        $saver = new D2Char();

        // process post
        // combine armor/weapon codes
        foreach ($_POST as $k => $v) {
            $x = str_replace("_", " ", $k);
            $post[$x] = $v;
        }

        if (!empty($post['code'])) {
            array_filter($post['code']);

            if (!empty($post['code'][0])) {
                $post['code'] = $post['code'][0];
            } else {
                $post['code'] = $post['code'][1];
            }
        }


        // last 3 items are not in d2 txt, rid of them
        // why did I set formtype? I can't rememer, need to comment code more


        /*

         *
         *
         * Unique Items Form was posted here
         * 
         * TODO: Move this somewhre else, for fucks sake, why post here? That's just fucking stupid.
         *
         */

        if ($_POST['formtype'] == "uniqueitems") {

            unset($post['formtype']);
            unset($post['submit']);
            unset($post['item']);

            // if ladder or carry1 is 0, set empty field.
            if (!$post['ladder']) {
                $post['ladder'] = '';
            }
            if (!$post['carry1']) {
                $post['carry1'] = '';
            }



            ddump($post);

            $sql = "SELECT rowid,`index` from `uniqueitems` WHERE `index`=\"{$post['index']}\"";

            $res = PDO_FetchRow($sql);

            if ($res['index'] == $post['index']) {
                $sql = "UPDATE `uniqueitems` SET ";
                foreach ($post as $k => $v) {
                    $sql .= "`$k`=\"$v\",";
                }
                $sql = rtrim($sql, ",");
                $sql .= ' WHERE `index`="' . $post['index'] . '"';

                PDO_Execute($sql, [$post['index']]);
                //ddump($x);
            }
            /*
             *
             *
             * Set Items Form was posted here
             *
             */ else {

                $sql = "INSERT INTO `uniqueitems` (";
                foreach ($post as $k => $v) {
                    $sql .= "`$k`,";
                }
                $sql = rtrim($sql, ",");
                $sql .= ") VALUES (";
                foreach ($post as $k => $v) {
                    $sql .= "'$v',";
                }
                $sql = rtrim($sql, ",");
                $sql .= ")";
                ddump($sql);

                // $saver->save($u, $post);
                // $saver->saveTblEnries("UniqueItems.tbl.txt");
            }
        }
        if ($_POST['formtype'] == "setitems") {
            $saver->save($s, $post);
            $saver->saveTblEnries("SetItems.tbl.txt");
        }
    }


    // load app
    require_once './src/index.php';
}
?>