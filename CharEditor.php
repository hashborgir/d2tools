<?php

session_start();
ob_start();

require_once './config.php';
require_once './_pdo.php';

require_once './src/D2Functions.php';
require_once './src/D2ByteReader.php';
require_once './src/D2BitReader.php';
require_once './src/D2Item.php';
require_once './src/D2Char.php';

error_reporting(E_ERROR | E_PARSE);
set_time_limit(-1);
ini_set('max_input_time', '-1');
ini_set('max_execution_time', '0');

define('DB_FILE', $_SESSION['modname'] . ".db");
PDO_Connect("sqlite:" . DB_FILE);

$sql = "SELECT * FROM strings";
$strings = PDO_FetchAssoc($sql);

$sql = "SELECT code,namestr
         FROM armor
         UNION SELECT code,namestr
         FROM misc
         UNION SELECT code,namestr
         FROM weapons";
$namestr = PDO_FetchAssoc($sql);

$sql = "SELECT ID,Stat,CSvBits FROM itemstatcost WHERE Saved=1";
$ISCData = PDO_FetchAll($sql);
foreach ($ISCData as $k => $v) {
    $ISC[$v['ID']] = $v;
}

//$filePath = "D:\Diablo II\MODS\ironman-dev\save\Aldur.d2s";
//$filePath = "D:\Diablo II\MODS\MedianXL2012\save\Lok.d2s";
//$filePath = "D:\Diablo II\MODS\MedianXL2012\save\Pal.d2s";
//$filePath = "D:\Diablo II\MODS\MedianXL2012\save\Test.d2s";


$filePath = "Test.d2s";
$char = new D2Char($filePath);

//$char->setChar("CharacterStatus", "Died", 0);
//$char->setChar("CharacterStatus", "Hardcore", 1);
//$char->setChar("CharacterStatus", "Expansion", 1);
//$char->setChar("LeftmousebuttonskillID", 223);

$char->setAllSkills(0);
//$char->setSkill(3, 20);
//$char->setChar("CharacterClass", "Necromancer"); // 127
//$char->setChar("CharacterProgression", 1); // 0 in normal, 1 finished normal, 2 finished nm, 3 finished hell

//$char->setChar("CharacterStatus", "Died", 1);
$char->setChar("CharacterLevel", 1);
$char->setStat("newskills", 99);
//$char->setStat("strength", 270);
//$char->setStat("energy", 270);
//$char->setStat("dexterity", 270);
//$char->setStat("vitality", 270);
//$char->setStat("hitpoints", 100);
//$char->setStat("maxhp", 150);
//$char->setStat("stamina", 280);
//$char->setStat("maxstamina", 290);
//$char->setStat("mana", 70);
//$char->setStat("maxmana", 200);


//$char->setStat("soulcounter", 80);
//
//$char->setStat("hitpoints", 70);
//$char->setStat("maxhp", 70);
//$char->setStat("stamina", 70);
//$char->setStat("maxstamina", 200);
//
//
//$char->setStat("gold", 0);
//$char->setStat("soulcounter", 40273409479012734098712903479012374091827349081273490172093478);




unset($char); // destroy $char so we can read it again after writing to it to get updated stats

$char = new D2Char($filePath);

var_dump($char->cData);
//var_dump($char->cData['stats']);
