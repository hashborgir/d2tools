<?php

error_reporting(E_ERROR | E_PARSE);
set_time_limit(-1);
ini_set('max_input_time', '-1');
ini_set('max_execution_time', '0');
session_start();
ob_start();

/*
  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
require_once './config.php';
require_once './_pdo.php';
require_once "./src/D2Functions.php";
require_once './src/D2ItemData.php';
require_once './src/D2ItemDesc.php';
require_once './src/D2DocGenerator.php';

$pre = "<pre>";

$idata = new D2ItemData();
$idesc = new D2ItemDesc();
// $idoc = new D2DocGenerator();

define('DB_FILE', $_SESSION['modname'] . ".db");
PDO_Connect("sqlite:" . DB_FILE);

$idata = new D2ItemData();

// set $cmd
if (!empty($_GET['cmd']))
    $cmd = $_GET['cmd'];

// create mod doc directory
$dir = getcwd() . "/docs/{$_SESSION['modname']}";
mkdir($dir, 0777, true);

$idoc = new D2DocGenerator();
$isc = $idoc->getIscProps();
$strings = $idoc->getStrings();
$itemtypesTbl = $idoc->getItemTypesTbl();
$namestr = $idoc->getNameStr();

// htmlTop was huge, so put it into another file and just include it
require_once 'templates/htmlTop.php';
$htmlbottom = "</div></body></html>";

// genDocUniqueItems
if ($cmd == "genDocUniqueItems") {
    $tableTop = '<table border="1" class="utable">
        <thead>
            <tr>
                <th class="gold uname">Name</th>
                <th class="orange ulvl">Lvl</th>
                <th class="orange ulvlreq">Lvl Req</th>
                <th class="yellow utype">Type</th>
                <th class="green ucode">Code</th>
                <th class="uitem">Item</th>
            </tr>
        </thead>
        <tbody>';

    $tableBottom = '</tbody></table>';

    // Grab unique item index only (to save on memory)
    $sql = "SELECT `index` FROM `uniqueitems` WHERE `enabled`='1'";
    $uIndex = PDO_FetchAll($sql);

    $htmlFinal = '';
    $htmlFinal .= $htmltop . $tableTop;

    echo $htmltop;
    echo $tableTop;
    ob_flush();

    // Main loop: grab the data for each uIndex (index from uniqueitems.txt)
    foreach ($uIndex as $u) {
        $item = $idata->uniqueItems($u['index'], true);

        $itemBoxTop = '<tr>';
        $itemBoxTop .= '<td class="gold uname">' . $item['index'] . '</td>';
        $itemBoxTop .= '<td class="orange ulvl">' . $item['lvl'] . '</td>';
        $itemBoxTop .= '<td class="orange ulvlreq">' . $item["lvl req"] . '</td>';
        $itemBoxTop .= '<td class="utype">' . $item["*type"] . '</td>';
        $itemBoxTop .= '<td class="ucode">' . $item['code'] . '</td>';
        $itemBoxTop .= <<<HTML
            <td class="item">
                <div class="center item_desc">
                    <div class="itemimage" style="height: 116px; background: url(img/items/bg.png) center top no-repeat;">
                        <img class="item img-fluid" src="img/items/{$item["invfile"]}.png">
                    </div>
                    <div class="item_stats">
                        <div style="width:auto; margin: 0 auto; background:#111; display:inline-block;">
                            <p>
        HTML;

        $itemBoxBottom = <<<HTML
                            </p>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        HTML;

        echo $itemBoxTop;
        ob_flush();

        $htmlFinal .= $itemBoxTop;

        // Index
        $props = '<ul><li class="statindex gold">' . $item['index'] . '</li>';

        // Type
        $props .= '<li class="stattype gold">(' . $item["*type"] . ')</li>';

        // Level
        $props .= '<li style="color: white;">Level: ' . $item["lvl"] . '</li>';

        // Level Required
        $props .= '<li style="color: white;">Level Required: ' . $item["lvl req"] . '</li>';

        // Item Code
        $props .= '<li style="color: #aaa;">Item Code: ' . $item["code"] . '</li>';

        // Required Strength
        if ($item["reqstr"]) {
            $props .= '<li style="color: white;">' . $item["reqstr"] . '</li>';
        }

        // Required Dexterity
        if ($item["reqdex"]) {
            $props .= '<li style="color: white;">' . $item["reqdex"] . '</li>';
        }

        // Sockets
        $props .= '<li style="color: white;">' . $item["gemsockets"] . '</li>';

        foreach ($item['props'] as $prop) {
            if ($prop['string'][0]) {
                $props .= '<li class="blue">' . $prop['string'][0] . '</li>';
            }
        }

        $props .= '</ul>';

        echo $props;
        echo $itemBoxBottom;
        ob_flush();

        $htmlFinal .= $props . $itemBoxBottom;
    }

    $htmlFinal .= $tableBottom;

    echo $htmlbottom;

    $htmlFinal .= $htmlbottom;
    file_put_contents("$dir/uniqueitems.html", $htmlFinal);
    header("Location: /docs/{$_SESSION['modname']}/uniqueitems.html");
}


// genDocCube
if ($cmd == "genDocCube") {
    $htmlFinal = '';
    $htmlFinal .= $htmltop;
    echo $htmltop;
    ob_flush();
    $table = <<<EOT
	<table border=1 class="cubetable">
          <thead>
			<tr>
				<th>Recipe</th>
				<th>Input 1</th>
				<th> 2</th>
				<th> 3</th>
				<th> 4</th>
				<th> 5</th>
				<th> 6</th>
				<th> 7</th>
				<th>Output 1</th>
				<th> 2</th>
				<th> 3</th>
			</tr>
	     </thead>
	<tbody>
EOT;
    echo $table;
    $htmlFinal .= $table;
    ob_flush();
    $i_params = [
        "any" => "Any Item",
        "low" => "Low Quality",
        "hiq" => "Superior",
        "nor" => "Normal",
        "mag" => "<span class='blue'>Magic<span>",
        "rar" => "<span class='yellow'>Rare<span>",
        "set" => "<span class='green'>Set<span>",
        "uni" => "<span class='gold'>Unique<span>",
        "crf" => "<span class='orange'>Crafted<span>",
        "tmp" => "<span class='orange'>Tempered<span>",
        "eth" => "<span style='color: #ccc;'>Ethereal<span>",
        "noe" => "<span style='color: #ccc;'>Not Ethereal<span>",
        "nos" => "No Socket (for input)",
        "sock" => "Sockets",
        "pre" => "Prefix (ID from magicprefix.txt) - ID => (Row Number - 2)",
        "suf" => "Suffix (ID from magicsuffix.txt) - ID => (Row Number - 2)",
        "rep" => "Repair durability",
        "rch" => "Recharge Quantity",
        "qty" => 'Quantity',
        "upg" => "For inputs only, Item is upgraded.",
        "bas" => "Base item",
        "exc" => "Exceptional",
        "eli" => "Elite",
        "usetype" => "Uses base type of input1 for output",
        "useitem" => "Uses item from input 1",
        "uns" => "Unsocket Item(destroys gems)",
        "rem" => "Unsocket Item(removes gems)",
        "reg" => "Regenerate? Seems to reroll the item",
        "mod" => "Transfers properties from input1 to output item.",
    ];

    // get all cube rows
    // changed enabled=1 to enabled > 0 for CubeOps.dll users

    $sql = "SELECT *
             FROM cubemain
             WHERE enabled > 0
             AND `input 1` <> 'ib3'
             AND `description` NOT LIKE '%Stone x%'
             AND `description` NOT LIKE '%Unstacker%' ";
    $res = PDO_FetchAll($sql);

    // process each cube row here
    foreach ($res as $r) {
        $desc = str_replace('+', '<br>+<br>', $r['description']);
        $desc = str_replace('->', '<br><span class="" style="color: gold;">=<br>', $desc);
        $desc .= "</span>";

        // grab all inputs
        $input_codes = array_filter([
            "input 1" => $r["input 1"],
            "input 2" => $r["input 2"],
            "input 3" => $r["input 3"],
            "input 4" => $r["input 4"],
            "input 5" => $r["input 5"],
            "input 6" => $r["input 6"],
            "input 7" => $r["input 7"],
        ]);

        foreach ($input_codes as $inputNum => $value) {
            if (str_contains($value, ',')) {
                $input_codes[$inputNum] = explode(",", $value); // hpot,qty=3

                $oarray = explode(",", $value); // hpot,qty=3
                // For each input code // hpot,qty=3 like mpot/hpot/magharv
                // if spelldescstr is set, use it. Else use item string tbl
                // first find the item code in either armor, misc, or weapons
                // then get spelldesc/spelldescstr
                // $sql = "SELECT `String` FROM `strings` WHERE Key=\"{$oarray[0]}\"";
                // $str = PDO_FetchOne($sql);
                $str = $strings[$oarray[0]]; // Assuming $strings array is defined
                // if empty str == 4-letter code, call $idoc->getItemName()
                if (empty($str)) {
                    $input_codes[$inputNum][0] = $oarray[0];
                } else {
                    $input_codes[$inputNum][0] = $str;
                }

                // get image
                $input_codes[$inputNum]["img"] = $idoc->getImage($oarray[0]);
            } else {
                $value .= ",";
                $input_codes[$inputNum] = explode(",", $value); // hpot,qty=3

                $oarray = explode(",", $value); // hpot,qty=3
                // For each input code // hpot,qty=3 like mpot/hpot/magharv
                // if spelldescstr is set, use it. Else use item string tbl
                // first find the item code in either armor, misc, or weapons
                // then get spelldesc/spelldescstr
                // $sql = "SELECT `String` FROM `strings` WHERE Key=\"{$oarray[0]}\"";
                // $str = PDO_FetchOne($sql);
                $str = $strings[$oarray[0]]; // Assuming $strings array is defined
                // if empty str == 4-letter code, so just display
                if (empty($str)) {
                    $input_codes[$inputNum][0] = $oarray[0];
                } else {
                    $input_codes[$inputNum][0] = $str;
                }

                // get image
                $input_codes[$inputNum]["img"] = $idoc->getImage($oarray[0]);
            }
        }

        // processs output, output b, output c
        // grab all inputs
        $output_codes = array_filter(
            [
                "output" => str_replace('"', "", $r["output"]),
                "output b" => str_replace('"', "", $r["output b"]),
                "output c" => str_replace('"', "", $r["output c"])
            ]
        );

        // var_dump($output_codes);
        // output_codes is an array
        // output codes of outputs, like output, output b, output c.
        // each output can have 3 or 4 level item code or a amu,mag,pre=331 code
        // outputNum is output a, or output b, or output c, should have been called outputLetter whatever
        foreach ($output_codes as $outputNum => $value) {

            //outputNum will be output, or output b, or c
            // value will be 3/4 letter code or sometimes 'Cow Portal' or words
            // or rin,mag,pre=353
            // if value contains a comma, e.g., rin,mag,pre=353
            // then explode it at the comma
            if (str_contains($value, ',')) {
                $output_codes[$outputNum] = explode(",", $value); // hpot,qty=3
                $oarray = explode(",", $value); // hpot,qty=3

                $str = $strings[$oarray[0]]; // "SELECT `String` FROM `strings` WHERE Key=\"{$oarray[0]}\"";
                // clean str
                // $str = PDO_FetchOne($sql);
                if (str_contains($str, "ÿc")) {
                    $p = "/ÿc[\d:;\/.]/i";
                    $str = preg_replace($p, "", $str);
                }

                // if empty str == 4-letter code, so just display
                if (empty($str)) {
                    $output_codes[$outputNum][0] = ($oarray[0]);
                } else {
                    $output_codes[$outputNum][0] = $str;
                }

                $output_codes[$outputNum]["img"] = $idoc->getImage($oarray[0]);
            } else { // else output code is three/four letter code
                $value .= ",";

                $output_codes[$outputNum] = explode(",", $value); // hpot,qty=3
                $oarray = explode(",", $value); // hpot,qty=3
                $str = $strings[$oarray[0]]; //"SELECT `String` FROM `strings` WHERE Key=\"{$oarray[0]}\"";
                // clean color codes
                // $str = PDO_FetchOne($sql);

                if (str_contains($str, "ÿc")) {
                    $p = "/ÿc[\d:;\/.]/i";
                    $str = preg_replace($p, "", $str);
                }


                // if empty str == 4-letter code, so just display
                if (empty($str)) {
                    $output_codes[$outputNum][0] = ($oarray[0]);
                } else {
                    $output_codes[$outputNum][0] = $str;
                }

                $output_codes[$outputNum]["img"] = $idoc->getImage($oarray[0]);
            }

            //var_dump($output_codes);
            // if output[1] or output[2], output[3], output[4] contains `=' sign
            // parse_str or explode at = to get modifer and value
            // if modifier = pre or suf, find ID in magic prefix/suffix line.
            // if output is array we know it contains modifers and values

            if (is_array($output_codes[$outputNum])) {
                // 0 will always be item code.
                // 1,2,3 can be mods, either can have =
                // if 1,2,3 have = sign, explode and get mod = value

                $itemcode = $output_codes[0];

                $mod = array_filter([$output_codes[$outputNum][1], $output_codes[$outputNum][2], $output_codes[$outputNum][3]]);

                //var_dump($mod);

                foreach ($mod as $m) {
                    if (str_contains($m, '=')) {
                        $e = explode("=", $m);
                        $output_codes[$outputNum]['code'] = $e[0];
                        $output_codes[$outputNum]['value'] = $e[1];
                    }
                }
            }
        }

        $output = array_merge($input_codes, $output_codes);

        // Set description
        $output['desc'] = $desc;

        // process mods for each output
        $m = null;
        for ($counter = 1; $counter <= 5; $counter++) {
            $mods = array_filter([
                "mod $counter" => $r["mod $counter"],
                "mod $counter chance" => $r["mod $counter chance"],
                "mod $counter param" => $r["mod $counter param"],
                "mod $counter min" => $r["mod $counter min"],
                "mod $counter max" => $r["mod $counter max"],
                "b mod $counter" => $r["b mod $counter"],
                "b mod $counter chance" => $r["b mod $counter chance"],
                "b mod $counter param" => $r["b mod $counter param"],
                "b mod $counter min" => $r["b mod $counter min"],
                "b mod $counter max" => $r["b mod $counter max"],
                "c mod $counter" => $r["c mod $counter"],
                "c mod $counter chance" => $r["c mod $counter chance"],
                "c mod $counter param" => $r["c mod $counter param"],
                "c mod $counter min" => $r["c mod $counter min"],
                "c mod $counter max" => $r["c mod $counter max"],
            ]);

            $m[] = array_filter($mods);
        }
        $output['mods'] = array_filter($m);

        $result = [];

        // var_dump($output['mods']);
        foreach ($output['mods'] as $k => $v) {
            $prop = ($v["mod " . $k + 1]);
            $par = ($v["mod " . $k + 1 . " param"]) ?? "";
            $min = ($v["mod " . $k + 1 . " min"]) ?? $max;
            $max = ($v["mod " . $k + 1 . " max"]) ?? $min;

            //var_dump($min);

            $output['mods'][$k]["isc"] = $isc[$prop];
            $params = [
                'string1' => $output['mods'][$k]["isc"]['string1'],
                'string2' => $output['mods'][$k]["isc"]['string2'],
                'descfunc' => $output['mods'][$k]["isc"]['descfunc'],
                'descval' => $output['mods'][$k]["isc"]['descval'],
                "prop" => $prop,
                "par" => $par,
                "min" => $min,
                "max" => $max,
                // "val1" => $funcval['val1'],
                // "func1" => $funcval['func1']
                // 'item' => $return
            ];

            $output['mods'][$k]["str"] = $idesc->getDesc($params);
        }


        //var_dump($output);
        // At this point, $output contains raw accurte data
        // TODO: build output correctly
        // input1-input7,output a,b,c item codes can be found in $strings
        // However, if the codes are 4 letter codes, they are types
        // Type code sare not in $strings (tbl files)
        // Type codes are in ItemTypes in itemtypes.Code
        // Type code's string name entry is in itemtypes.ItemType table--
        // first find in $strings. if not there, look in itemtypes
        //

        $htmlRow = "<tr>";
        $htmlRow .= "<td class='desc'>" . $output['desc'] . "</td>";

        for ($i = 1; $i <= 7; $i++) {

            $htmlRow .= "<td class='_input$i'>"; // replace _input with input to get grey td bg colors back
            if (!empty($output["input $i"]["img"])) {
                $htmlRow .= "<img src='img/items/{$output["input $i"]["img"]}'><br>";
                if (str_contains($output["input $i"][0], "ÿc")) {
                    $p = "/ÿc[\d:;\/.]/i";
                    $output["input $i"][0] = preg_replace($p, "", $output["input $i"][0]);
                }


                // if the item is in itemtypes grab it, it's 4 letter code fron itemtypes
                $key1 = array_search($output["input $i"][0], $itemtypesTbl);

                // if the key's value is in itemtypes
                if ($itemtypesTbl[$key1] == $output["input $i"][0]) {
                    $ostr = implode("<br>", (array_reverse(explode('\n', $key1))));
                } else {
                    $ostr = implode("<br>", (array_reverse(explode('\n', $output["input $i"][0]))));
                }

                $htmlRow .= $ostr . "<br>";
                if (str_contains($output["input $i"][1], "=")) {
                    $htmlRow .= $output["input $i"][1] . "<br>";
                } else {
                    $htmlRow .= $i_params[$output["input $i"][1]] . "<br>";
                }
            }
            $htmlRow .= "</td>";
        }


        $htmlRow .= "<td class='_output1'>"; // change _output to output
        if (!empty($output["output"]["img"])) {
            $htmlRow .= "<img src='img/items/{$output["output"]["img"]}'><br>";
            if (str_contains($output["output"][0], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $output["output"][0] = preg_replace($p, "", $output["output"][0]);
            }

            $htmlRow .= '<span style="color: #AF833B;">' . $output["output"][0] . "</span><br>";            
            $key1 = array_search($output["input $i"][0], $itemtypesTbl);

            if (str_contains($output["output"][1], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $output["output"][1] = preg_replace($p, "", $output["output"][1]);
            }
            if ($i_params[$output["output"][0]]) {
                $htmlRow .= '<span style="color: #AF833B;">' . $i_params[$output["output"][0]] . "</span><br>";
            } else {
                $htmlRow .= $i_params[$output["output"][0]] . "<br>";
            }
            if ($i_params[$output["output"][1]]) {
                $htmlRow .= $i_params[$output["output"][1]] . "<br>";
            } else {
                $htmlRow .= $output["output"][1] . "<br>";
            }

            for ($i = 1; $i <= 5; $i++) {

                if ($output["mods"][0]["mod $i"]) {
                    if (str_contains($output["mods"][0]["str"], "ÿc")) {
                        $p = "/ÿc[\d:;\/.]/i";
                        $output["mods"][0]["str"] = preg_replace($p, "", $output["mods"][0]["str"]);
                    }



                    $htmlRow .= str_replace("\n", "<br>", "<span class='blue'>{$output["mods"][0]["str"]}</span><br><br>");
                }

                if ($output["mods"][0]["mod $i chance"]) {
                    $htmlRow .= "<span style='color: #FFA500'>Chance:{$output["mods"][0]["mod $i chance"]}</span><br>";
                }
            }
        }
        $htmlRow .= "</td>";

        $htmlRow .= "<td class='_output2'>"; // change _output2 to output
        if (!empty($output["output b"]["img"])) {
            $htmlRow .= "<img src='img/items/{$output["output b"]["img"]}'><br>";
            if (str_contains($output["output b"][0], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $output["output b"][0] = preg_replace($p, "", $output["output b"][0]);
            }
            if (str_contains($output["output b"][1], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $output["output b"][1] = preg_replace($p, "", $output["output b"][1]);
            }
            $htmlRow .= $output["output b"][0] . "<br>";
            $htmlRow .= $output["output b"][1] . "<br>";
            for ($i = 1; $i <= 5; $i++) {

                if ($output["mods"][1]["mod $i"]) {
                    $htmlRow .= "{$output["mods"][1]["mod $i"]}<br>";
                }

                if ($output["mods"][1]["mod $i chance"]) {
                    $htmlRow .= "Chance:{$output["mods"][1]["mod $i chance"]}<br>";
                }

                if ($output["mods"][1]["mod $i min"]) {
                    $htmlRow .= "Min:{$output["mods"][1]["mod $i min"]}<br>";
                }
                if ($output["mods"][1]["mod $i max"]) {
                    $htmlRow .= "Max:{$output["mods"][1]["mod $i max"]}";
                }
            }
        }
        $htmlRow .= "</td>";

        $htmlRow .= "<td class='_output3'>"; // change _output3 to normal
        if (!empty($output["output c"]["img"])) {
            $htmlRow .= "<img src='img/items/{$output["output c"]["img"]}'><br>";
            if (str_contains($output["output c"][0], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $output["output c"][0] = preg_replace($p, "", $output["output c"][0]);
            }
            if (str_contains($output["output c"][1], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $output["output c"][1] = preg_replace($p, "", $output["output c"][1]);
            }
            $htmlRow .= $output["output c"][0] . "<br>";
            $htmlRow .= $output["output c"][1] . "<br>";
            for ($i = 1; $i <= 5; $i++) {

                if ($output["mods"][2]["mod $i"]) {
                    $htmlRow .= "{$output["mods"][2]["mod $i"]}<br>";
                }

                if ($output["mods"][2]["mod $i chance"]) {
                    $htmlRow .= "Chance:{$output["mods"][2]["mod $i chance"]}<br>";
                }

                if ($output["mods"][2]["mod $i min"]) {
                    $htmlRow .= "Min:{$output["mods"][2]["mod $i min"]}<br>";
                }
                if ($output["mods"][2]["mod $i max"]) {
                    $htmlRow .= "Max:{$output["mods"][2]["mod $i max"]}";
                }
            }
        }
        $htmlRow .= "</td>";
        $htmlRow .= "</tr>";

        echo $htmlRow;
        ob_flush();

        $htmlFinal .= $htmlRow;
        ob_flush();
        unset($output);
    }// end each row loop

    $tableBottom = '</tbody><table>';
    echo $tableBottom;
    echo $htmlbottom;
    ob_flush();
    $htmlFinal .= $tableBottom . $htmlbottom;

    // echo $htmlFinal;
    // save file and redirect to it
    file_put_contents("$dir/cubemain.html", $htmlFinal);
    header("Location: docs/{$_SESSION['modname']}/cubemain.html");
}

if ($cmd == "getImages") {
    $sql = "SELECT code,invfile,type,type2 FROM armor
            UNION ALL
            SELECT code,invfile,type,type2 FROM misc
            UNION ALL
            SELECT code,invfile,type,type2 FROM weapons";
    $res = PDO_FetchAll($sql);
    $isc = null;
    foreach ($res as $r) {
        $isc1[$r['code']] = $r;
        $isc1[$r['code']]['code'] = $r['prop'];

        $isc2[$r['type']] = $r;
        $isc2[$r['type']]['type'] = $r['prop'];
    }
    $isc = array_merge($isc1, $isc2);

    unset($isc1);
    unset($isc2);
}

// gem doc generator
if ($cmd == "genDocGems") {
    $sql = "
            SELECT g.*,m.invfile,m.type,m.type2,s.String
            FROM gems as g
            LEFT JOIN misc as m
            ON g.code = m.code
            LEFT JOIN strings as s
            ON g.code = s.Key";
    $res = array_filter(PDO_FetchAll($sql));

    foreach ($res as $r) {
        for ($i = 1; $i <= 3; $i++) {
            $params = [
                'string1' => $isc[$r["weaponMod{$i}Code"]]['string1'],
                'string2' => $isc[$r["weaponMod{$i}Code"]]['string2'],
                'gstring1' => $isc[$r["weaponMod{$i}Code"]]['gstring'],
                'gstring2' => $isc[$r["weaponMod{$i}Code"]]['gstring2'],
                'descfunc' => $isc[$r["weaponMod{$i}Code"]]['descfunc'],
                'descval' => $isc[$r["weaponMod{$i}Code"]]['descval'],
                'dgrp' => $isc[$r["weaponMod{$i}Code"]]['dgrp'],
                'dgrpfunc' => $isc[$r["weaponMod{$i}Code"]]['dgrpfunc'],
                'dgrpval' => $isc[$r["weaponMod{$i}Code"]]['dgrpval'],
                "prop" => $isc[$r["weaponMod{$i}Code"]]['prop']
            ];
            // weapons
            $params["par"] = $r["weaponMod{$i}Param"];
            $params["min"] = $r["weaponMod{$i}Min"];
            $params["max"] = $r["weaponMod{$i}Max"];
            if ($r["weaponMod{$i}Code"]) {
                $r["weaponMod{$i}Code"] = $idesc->getDesc($params);
            }

            $params = [
                'string1' => $isc[$r["helmMod{$i}Code"]]['string1'],
                'string2' => $isc[$r["helmMod{$i}Code"]]['string2'],
                'gstring1' => $isc[$r["helmMod{$i}Code"]]['gstring'],
                'gstring2' => $isc[$r["helmMod{$i}Code"]]['gstring2'],
                'descfunc' => $isc[$r["helmMod{$i}Code"]]['descfunc'],
                'descval' => $isc[$r["helmMod{$i}Code"]]['descval'],
                'dgrp' => $isc[$r["helmMod{$i}Code"]]['dgrp'],
                'dgrpfunc' => $isc[$r["helmMod{$i}Code"]]['dgrpfunc'],
                'dgrpval' => $isc[$r["helmMod{$i}Code"]]['dgrpval'],
                "prop" => $isc[$r["helmMod{$i}Code"]]['prop']
            ];

            // helms
            $params["par"] = $r["helmMod{$i}Param"];
            $params["min"] = $r["helmMod{$i}Min"];
            $params["max"] = $r["helmMod{$i}Max"];
            if ($r["helmMod{$i}Code"]) {
                $r["helmMod{$i}Code"] = $idesc->getDesc($params);
            }

            $params = [
                'string1' => $isc[$r["helmMod{$i}Code"]]['string1'],
                'string2' => $isc[$r["helmMod{$i}Code"]]['string2'],
                'gstring1' => $isc[$r["helmMod{$i}Code"]]['gstring'],
                'gstring2' => $isc[$r["helmMod{$i}Code"]]['gstring2'],
                'descfunc' => $isc[$r["helmMod{$i}Code"]]['descfunc'],
                'descval' => $isc[$r["helmMod{$i}Code"]]['descval'],
                'dgrp' => $isc[$r["helmMod{$i}Code"]]['dgrp'],
                'dgrpfunc' => $isc[$r["helmMod{$i}Code"]]['dgrpfunc'],
                'dgrpval' => $isc[$r["helmMod{$i}Code"]]['dgrpval'],
                "prop" => $isc[$r["helmMod{$i}Code"]]['prop']
            ];

            //shields
            $params["par"] = $r["shieldMod{$i}Param"];
            $params["min"] = $r["shieldMod{$i}Min"];
            $params["max"] = $r["shieldMod{$i}Max"];
            if ($r["shieldMod{$i}Code"]) {
                $r["shieldMod{$i}Code"] = $idesc->getDesc($params);
            }

            if (str_contains($r['String'], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $r['String'] = preg_replace($p, "", $r['String']);
            }
        }
        $gems[] = array_filter($r);
    }


    $htmlFinal = '';
    $htmlFinal .= $htmltop;
    //echo $htmltop;
    //ob_flush();
    $table = '<table border=1 class="gemtable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                        <th>Type</th>
                        <th>Mods</th>
                        <th>Weapons</th>
                        <th>Helms</th>
                        <th>Shields</th>
                    </tr>
                </thead>
            <tbody>';
    //echo $table;
    $htmlFinal .= $table;
    //ob_flush();
    // $gems are all the gems, processed, now display them
    foreach ($gems as $g) {

        if ($g['name'] == "Expansion") {
            continue;
        }

        $htmlRow = "<tr>";
        $htmlRow .= "<td class='name'>
						<img style='height: 64px;width:64px;' src='img/items/{$g["invfile"]}.png'><br>
						{$g['String']}
					</td>
				   ";
        $htmlRow .= "<td class='code'>
						{$g['code']}
					</td>
				   ";
        $htmlRow .= "<td class='type'>
						{$g['type']}
					</td>
				   ";
        $htmlRow .= "<td class='nummods'>
						Mods: {$g['nummods']}
					</td>
				   ";
        $htmlRow .= "<td class='weapons blue'>
						<img src='img/weapon.png'><br>
						{$g['weaponMod1Code']}<br>
						{$g['weaponMod2Code']}<br>
						{$g['weaponMod3Code']}<br>
					</td>
				   ";
        $htmlRow .= "<td class='helms gold'>
					<img src='img/helm.png'><br>
						{$g['helmMod1Code']}<br>
						{$g['helmMod2Code']}<br>
						{$g['helmMod3Code']}<br>
					</td>
				   ";
        $htmlRow .= "<td class='shields'>
					<img src='img/shield.png'><br>
						{$g['shieldMod1Code']}<br>
						{$g['shieldMod2Code']}<br>
						{$g['shieldMod3Code']}<br>
					</td>
				   ";

        $htmlRow .= "</tr>";

        $htmlFinal .= $htmlRow;
        //echo $htmlRow;
        // ob_flush();
    }

    $htmlFinal .= $tableBottom . $htmlbottom;
    //echo $tableBottom . $htmlbottom;
    //ob_flush();
    //ob_start();
    file_put_contents("$dir/gems.html", $htmlFinal);
    header("Location: /docs/{$_SESSION['modname']}/gems.html", TRUE, 301);
}


// generate runewords
if ($cmd == 'genDocRunewords') {

    $sql = 'SELECT * FROM runes';
    $runes = array_filter(PDO_FetchAll($sql));

    $sql = 'SELECT code,invfile from misc';
    $rune_inv_codes = array_filter(PDO_FetchAssoc($sql));

    $sql = 'SELECT * FROM strings';
    $strings = array_filter(PDO_FetchAssoc($sql));

    $sql = 'SELECT Itemtype,Code,Equiv1 FROM itemtypes';
    $itypes = array_flip(array_filter(PDO_FetchAssoc($sql)));

    $tableTop = <<<EOT
	<table class="rtable rw">
	  <thead>
		<tr>
		  <th>Name</th>
		  <th>Runes</th>
		  <th>Properties</th>
		</tr>
	  </thead>
	  <tbody>
	EOT;

    foreach ($runes as $rune) {
        // Grab Rune Names

        $name = $strings[$rune['Name']];

        $itype1 = $itypes[$rune['itype1']];
        $itype2 = $itypes[$rune['itype2']];
        $itype3 = $itypes[$rune['itype3']];
        $itype4 = $itypes[$rune['itype4']];
        $itype5 = $itypes[$rune['itype5']];
        $itype6 = $itypes[$rune['itype6']];

        $rr_img[1] = $rune_inv_codes[$rune['Rune1']];
        $rr_img[2] = $rune_inv_codes[$rune['Rune2']];
        $rr_img[3] = $rune_inv_codes[$rune['Rune3']];
        $rr_img[4] = $rune_inv_codes[$rune['Rune4']];
        $rr_img[5] = $rune_inv_codes[$rune['Rune5']];
        $rr_img[6] = $rune_inv_codes[$rune['Rune6']];

        $rr[1] = str_replace('', '', $strings[$rune['Rune1']]);
        $rr[2] = str_replace('', '', $strings[$rune['Rune2']]);
        $rr[3] = str_replace('', '', $strings[$rune['Rune3']]);
        $rr[4] = str_replace('', '', $strings[$rune['Rune4']]);
        $rr[5] = str_replace('', '', $strings[$rune['Rune5']]);
        $rr[6] = str_replace('', '', $strings[$rune['Rune6']]);

        for ($i = 1; $i <= 6; $i++) {
            if (str_contains($rr[$i], "ÿc")) {
                $p = "/ÿc[\d:;\/.]/i";
                $rr[$i] = preg_replace($p, "", $rr[$i]);
            }
        }


        $rr = array_filter($rr);
        $rune_count = count($rr);
        $rune_count_duplicates = array_count_values($rr);

        $rcd = null;
        foreach ($rune_count_duplicates as $k => $v) {
            // rune * count
            $rcd[] = "$k <span class='numrunes blue'>x</span> <span style='font-size: 2em;'>$v</span>";
        }

        for ($i = 1; $i <= 7; $i++) {
            if ($rune["T1Code{$i}"] != '') {
                $params = [
                    "string1" => $isc[$rune["T1Code{$i}"]]["string1"],
                    "string2" => $isc[$rune["T1Code{$i}"]]["string2"],
                    "gstring1" => $isc[$rune["T1Code{$i}"]]["gstring"],
                    "gstring2" => $isc[$rune["T1Code{$i}"]]["gstring2"],
                    "descfunc" => $isc[$rune["T1Code{$i}"]]["descfunc"],
                    "descval" => $isc[$rune["T1Code{$i}"]]["descval"],
                    "dgrp" => $isc[$rune["T1Code{$i}"]]["dgrp"],
                    "dgrpfunc" => $isc[$rune["T1Code{$i}"]]["dgrpfunc"],
                    "dgrpval" => $isc[$rune["T1Code{$i}"]]["dgrpval"],
                    "prop" => $isc[$rune["T1Code{$i}"]]["prop"]
                ];
                $params["par"] = $rune["T1Param{$i}"];
                $params["min"] = $rune["T1Min{$i}"];
                $params["max"] = $rune["T1Max{$i}"];

                $props[$i] = $idesc->getDesc($params);
            }
        }

        $props = array_unique($props);
        $rr_img = array_filter($rr_img);
        $img_s = '';
        foreach ($rr_img as $k) {
            $img_s .= "<img class='rune-img' src='img/items/{$k}.png'>";
        }
        $tableRowsRunes .= <<<EOT
        <tr class="rw">
          <td>
              <p class="rw-name gold">$name</p>
              <p class="rw-type">$itype1<br> $itype2<br> $itype3<br> $itype4<br> $itype5<br> $itype6</p>
          </td>
          <td>
              <p class="rw-rune">
                         <p>$img_s</p>
                         $rcd[0] <br>
                         $rcd[1] <br>
                         $rcd[2] <br>
                         $rcd[3] <br>
                         $rcd[4] <br>
                         $rcd[5] <br>
                         <!--<p class="rw-count">$ rune_count</p>-->
              </p></td>
                <td>
            <ul class="rprops blue">
                            <li style="font-size: 140%;margin: 5px;" class="gold">$name</li>
                            <li> $props[1] </li>
                            <li> $props[2] </li>
                            <li> $props[3] </li>
                            <li> $props[4] </li>
                            <li> $props[5] </li>
                            <li> $props[6] </li>
                            <li> $props[7] </li>
            </ul>
          </td>
        </tr>
        EOT;
    }

    echo $htmltop;
    echo $tableTop;
    echo $tableRowsRunes;
    echo "</tbody></table>";
    echo $htmlbottom;

    $htmlFinal = $htmltop . $tableTop . $tableRowsRunes . $tableBottom;

    file_put_contents("$dir/runewords.html", $htmlFinal);
    header("Location: docs/{$_SESSION['modname']}/runewords.html");
}


// generate item list page
if ($cmd == "genDocMisc") {

    $filter = "smh";

    $sql = "SELECT DISTINCT * FROM misc WHERE spawnable='1'";
    //       LEFT JOIN gems ON misc.code = gems.code
    //WHERE gems.code IS NULL AND misc.spawnable=1
    // ";// AND misc.namestr NOT LIKE '%$filter%'

    $res = PDO_FetchAll($sql);
    $res = array_reverse($res);

    //$sql = "SELECT * FROM armor WHERE spawnable=1";
    //$res = array_merge(PDO_FetchAll($sql), $res);
    //$sql = "SELECT * FROM weapons WHERE spawnable=1";
    //$res = array_merge(PDO_FetchAll($sql), $res);
    // ddump(array_keys($res[0]));


    $tableTop = <<<EOT
    <table border=1 class="mtable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Lvl</th>
                <th>Lvl Req</th>
                <th>Item</th>
            </tr>
        </thead>
    <tbody>
EOT;
    $htmlFinal = null;
    $htmlFinal = $htmltop . $tableTop;

    $tr = null;

    foreach ($res as $r) {

        $data = null;
        $stats = null;

        //ddump($r);
        // if hammers, then get itemcode's string
        if ($r['namestr'] == 'smh') {
            $data['name'] = $strings[$r['namestr']] . "<br>" . $strings[$r['code']];

            $p = "/ÿc[\d:;\/.]/i";
            $data['name'] = preg_replace($p, "", $data['name']);
            $data['name'] = array_reverse(explode('\n', $data['name']));
            $data['name'] = (implode('\n', $data['name']));
        } else {
            $data['name'] = $strings[$r['namestr']];
        }


        if (str_contains($data['name'], "ÿc")) {
            $p = "/ÿc[\d:;\/.]/i";
            $data['name'] = preg_replace($p, "", $data['name']);
        }
        $name = array_reverse(explode('\n', $data['name']));
        $data['name'] = (implode('<br>', $name));

        // later display these in a li
        $img = "img/items/{$r['invfile']}.png";

        $data['level'] = "Level: <span class=''>{$r['level']}</span>";
        $data['levelreq'] = "Level Required: <span class=''>{$r['levelreq']}</span>";

        if ($r['rarity']) {
            $data['rarity'] = $armorRack = "Can drop from Armor Racks";
        }

        if ($r['speed'] > 0) {
            $data['speed'] = "Slows your character down";
        }
        if ($r['durability']) {
            $data['durability'] = "Base durability: <span class=''>{$r['durability']}</span>";
        }

        if ($r['hasinv']) {

            $sql = "SELECT MaxSock40 FROM itemtypes WHERE `Code`='{$r['type']}'";
            $data["maxsock"] = PDO_FetchOne($sql);

            if ($r['gemapplytype'] == "0") {
                $data['gemapplytype'] = "Socketing gives <span class='gold'>Weapon</span> stats";
            } else if ($r['gemapplytype'] == "1") {
                $data['gemapplytype'] = "Socketing gives <span class='gold'>Armor/Helm</span> stats";
            } else if ($r['gemapplytype'] == "2") {
                $data['gemapplytype'] = "Socketing gives <span class='gold'>Shield</span> stats";
            }

            $data['hasinv'] = "Sockets <span class='blue' style='' font-size: 1.25em;'>({$data['maxsock']})</span>";
        }

        if ($r['Transmogrify']) {
            $tmogtype = ($strings[$r['TMogType']]);
            $data['Transmogrify'] = "Can right click transmogrify into ";
            $p = "/ÿc[\d:;\/.]/i";
            $data['Transmogrify'] = preg_replace($p, "", $data['Transmogrify']);
            $tmogtype = preg_replace($p, "", $tmogtype);

            $tmogtype = array_reverse(explode('\n', $tmogtype));
            $tmogtype = (implode(' ', $tmogtype));
            $tmogtype = str_replace(" Ess", '<br>Ess', $tmogtype);
            $tmogtype = str_replace(" Cube", '<br>Cube', $tmogtype);
            $data['tmogtype'] = $tmogtype;

            $transmogrify = array_reverse(explode('\n', $data['Transmogrify']));
            $data['Transmogrify'] = (implode('<br>', $transmogrify));
        }
        if (!empty($r['useable'])) {
            $data['useable'] = "Can be right clicked";
        }

        if ($r['throwable']) {
            $data['throwable'] = "Can be thrown";
        }
        if ($r['unique']) {
            $data['unique'] = "Can only spawn as a unique item.";
        }
        if ($r['stackable']) {
            $data['stackable'] = "Can be stacked";
        }

        if ($r['quest']) {
            $data['quest'] = "Quest Item";
        }
        if ($r['spelldesc']) {
            $data['spelldesc'] = $strings[$r['spelldescstr']];
            $p = "/ÿc[\d:;\/.]/i";
            $data['spelldesc'] = preg_replace($p, "", $data['spelldesc']);

            $spelldesc = array_reverse(explode('\n', $data['spelldesc']));
            $data['spelldesc'] = (implode('<br>', $spelldesc));
        }

        if ($r['nameable']) {
            $data['nameable'] = "<span class=''>Can be personalized by Anya</span>";
        }

        $statFields = ['stat1', 'stat2', 'stat3'];
        $calcFields = ['calc1', 'calc2', 'calc3'];

        foreach ($statFields as $field) {
            if (isset($r[$field])) {
                $stats[$field] = "{$r[$field]}";
            }
        }

        foreach ($calcFields as $field) {
            if (isset($r[$field])) {
                $stats[$field] = "{$r[$field]}";
            }
        }



        $data = array_filter($data);
        if ($stats)
            $stat = array_filter($stats);

        $tr .= <<<EOT
         <tr>
            <td>{$data['name']}</td>
            <td>{$data['level']}</td>
            <td>{$data['levelreq']}</td>
            <td style="background: black; font-weight: normal; text-align: center;">
                <div style="margin-left: 40px;">
                    <img style="max-height: 200%;" src="$img" />
                    <p style="font-size: 1.5em;" class="gold">{$data['name']}</p>
                </div>
                <ul style="font-weight: normal; font-family:">
                     <li class="">Item {$data['level']}</li>
                     <li class="">{$data['levelreq']}</li>
                     <li class="text-success">{$data['rarity']}</li>
                     <li class="blue">{$data['speed']}</li>
                     <li class="blue">{$data['durability']}</li>
                     <li class="">{$data['hasinv']}</li>
                     <li class="blue">{$data['gemapplytype']}</li>
                     <li class="blue">{$data['throwable']}</li>
                     <li class="blue">{$data['useable']}</li>
                     <li class="blue">{$data['unique']}</li>
                     <li class="blue">{$data['stackable']}</li>
                     <li class="text-info">{$data['Transmogrify']}{$data['tmogtype']}</li>
                     <li class="text-secondary">{$data['quest']}</li>
                     <li class="text-info">{$data['nameable']}</li>
                     <li class="blue">{$data['spelldesc']}</li>
                     <li class="blue">{$stats['stat1']} <span class="gold" style="font-size: 1.25em;">{$stats['calc1']}</span></li>
                     <li class="blue">{$stats['stat2']} <span class="gold" style="font-size: 1.25em;">{$stats['calc2']}</span></li>
                     <li class="blue">{$stats['stat3']} <span class="gold" style="font-size: 1.25em;">{$stats['calc3']}</span></li>
               </ul>
               </p>

             </td>
         </tr>
        EOT;
    }

    $htmlFinal = $htmltop . $tableTop . $tr . $tableBottom . $htmlbottom;

    file_put_contents("$dir/misc.html", $htmlFinal);
    header("Location: docs/{$_SESSION['modname']}/misc.html");
}



// Armor.txt docs generator
if ($cmd == 'genDocArmor') {

    $tableTop = <<<EOT
    <style>
        body { font-family: Tahoma,Arial,Sans-Serif}
        table { font-family: ;}
    </style>
    <table border=1 class="mtable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Lvl</th>
                <th>Required Lvl</th>
                <th>Required Str</th>
                <th>Min AC</th>
                <th>Max AC</th>
                <th>Block Chance</th>
            </tr>
        </thead>
    <tbody>
EOT;
    $tableBottom = '</tbody>	</table>';

    $sql = "SELECT * FROM armor WHERE spawnable=1 AND namestr != ''";
    $res = PDO_FetchAll($sql);

    foreach ($res as $r) {

        $name = (!empty($strings[$r['namestr']])) ? $strings[$r['namestr']] : $r['name'];

        if ($r['indestructible'] == '1') {
            $indestructable = "Yes";
        } else {
            $indestructable = "No";
        }

        $imgUnique = (!empty($r['uniqueinvfile'])) ? "<br><img src='img/items/{$r['uniqueinvfile']}.png'>" : "";
        $imgSet = (!empty($r['setinvfile'])) ? "<br><img src='img/items/{$r['setinvfile']}.png'>" : "";

        if (!empty($r['auto prefix'])) {
            $sql = "SELECT * FROM automagic WHERE `group` = ?";
            $ret = PDO_FetchRow($sql, [$r['auto prefix']]);

            // ddump($isc[$ret['mod1code']]);

            for ($i = 1; $i <= 3; $i++) {
                if ($ret["mod{$i}code"] != '') {

                    // ddump($isc[$ret["mod{$i}code"]]['string1']);
                    $params = [
                        "string1" => $isc[$ret["mod{$i}code"]]["string1"],
                        "string2" => $isc[$ret["mod{$i}code"]]["string2"],
                        "gstring1" => $isc[$ret["mod{$i}code"]]["gstring"],
                        "gstring2" => $isc[$ret["mod{$i}code"]]["gstring2"],
                        "descfunc" => $isc[$ret["mod{$i}code"]]["descfunc"],
                        "descval" => $isc[$ret["mod{$i}code"]]["descval"],
                        "dgrp" => $isc[$ret["mod{$i}code"]]["dgrp"],
                        "dgrpfunc" => $isc[$ret["mod{$i}code"]]["dgrpfunc"],
                        "dgrpval" => $isc[$ret["mod{$i}code"]]["dgrpval"],
                        "prop" => $isc[$ret["mod{$i}code"]]["prop"]
                    ];
                    $params["par"] = $ret["mod{$i}param"];
                    $params["min"] = $ret["mod{$i}min"];
                    $params["max"] = $ret["mod{$i}max"];

                    $props[$i] = $idesc->getDesc($params);
                }
            }

            // dump($props);
            $autoprefix = "Automagic/Auto prefix Mods";
        }

        $strbonus = (!empty($r['StrBonus'])) ? "Strength Bonus: <span class='white'>{$r['StrBonus']}" : "";

        $block = (!empty($r['block'])) ? "Block: <span class='white'>{$r['block']}</span>" : "";

        $tr .= <<<EOT
          <tr>
            <td style="text-align: left;">
                <ul>
                    <li class="white h1">$name</li>
                    <li class=""><img src="img/items/{$r['invfile']}.png">$imgC</li>
                    <li class="white">Defense: <span class="blue">[{$r['minac']} - {$r['maxac']}]</span></li>
                    <li class="white">Weight: <span class="blue">{$r['speed']}</span></li>
                    <li class="white">Level: <span class="blue">{$r['level']}</span></li>
                    <li class="white">Required Level: <span class="blue">{$r['levelreq']}</span></li>
                    <li class="white">Required Strength: <span class="blue">{$r['reqstr']}</span></li>
                    <li class="white">Indestructable: <span class="blue">$indestructable</span></li>
                    <li class="white">Durability: <span class="blue">{$r['durability']}</span></li>
                    <li class="gold">Cost: <span class="white">{$r['cost']}</span></li>

                    <li class="white">Normal:
                        <spap class="white">{$strings[$r['normcode']]}</span>
                    </li>
                    <li class="blue">Exceptional:<br>
                        <span class="white">{$strings[$r['ubercode']]}</span>
                        $imgSet
                    </li>
                    <li class="blue">Elite:<br>
                        <span class="white">{$strings[$r['ultracode']]}</span>
                        $imgUnique
                    </li>
                    <li class="blue">Sockets: <span class="white">{$r['gemsockets']}</span></li>
                    <li class="blue" style="margin-top: 30px;">$autoprefix<span class="white">{$props[0]}</span></li>
                    <li class="blue"><span class="white">{$props[1]}</span></li>
                    <li class="blue" style="margin-bottom: 30px;"><span class="white">{$props[2]}</span></li>
                    <li class="blue">$strbonus</li>
                    <li class="blue">$block</li>
                </ul>
            </td>
            <td>Level: <br> {$r['level']}</td>
            <td>Level Required: <br> {$r['levelreq']}</td>
            <td>Requird Strength: <br> {$r['reqstr']}</td>
            <td>Min AC: <br> {$r['minac']}</td>
            <td>Max AC: <br> {$r['maxac']}</td>
            <td>Block: <br> {$r['block']}</td>
          </tr>
EOT;
    }

    $htmlFinal = $htmltop . $tableTop . $tr . $tableBottom . $htmlbottom;

    ob_flush();
    echo $htmlFinal;

    file_put_contents("$dir/armor.html", $htmlFinal);
    header("Location: docs/{$_SESSION['modname']}/armor.html");
}


// Generate weapons.txt docs
if ($cmd == 'genDocWeapons') {
    $tableTop = <<<EOT
    <style>
        body { font-family: Tahoma,Arial,Sans-Serif}
        table { font-family: ;}
    </style>
    <table border=1 class="mtable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Lvl</th>
                <th>Required Lvl</th>
                <th>Required Str</th>
                <th>Min AC</th>
                <th>Max AC</th>
            </tr>
        </thead>
    <tbody>
EOT;
    $tableBottom = '</tbody>	</table>';

    $sql = "SELECT * FROM weapons WHERE spawnable=1 AND namestr != ''";
    $res = PDO_FetchAll($sql);

    foreach ($res as $r) {
        $name = (!empty($strings[$r['namestr']])) ? $strings[$r['namestr']] : $r['name'];

        if ($r['indestructible'] == '1') {
            $indestructable = "Yes";
        } else {
            $indestructable = "No";
        }

        $imgUnique = (!empty($r['uniqueinvfile'])) ? "<br><img src='img/items/{$r['uniqueinvfile']}.png'>" : "";
        $imgSet = (!empty($r['setinvfile'])) ? "<br><img src='img/items/{$r['setinvfile']}.png'>" : "";

        if (!empty($r['auto prefix'])) {
            $sql = "SELECT * FROM automagic WHERE `group` = ?";
            $ret = PDO_FetchRow($sql, [$r['auto prefix']]);

            // ddump($isc[$ret['mod1code']]);

            for ($i = 1; $i <= 3; $i++) {
                if ($ret["mod{$i}code"] != '') {

                    // ddump($isc[$ret["mod{$i}code"]]['string1']);
                    $params = [
                        "string1" => $isc[$ret["mod{$i}code"]]["string1"],
                        "string2" => $isc[$ret["mod{$i}code"]]["string2"],
                        "gstring1" => $isc[$ret["mod{$i}code"]]["gstring"],
                        "gstring2" => $isc[$ret["mod{$i}code"]]["gstring2"],
                        "descfunc" => $isc[$ret["mod{$i}code"]]["descfunc"],
                        "descval" => $isc[$ret["mod{$i}code"]]["descval"],
                        "dgrp" => $isc[$ret["mod{$i}code"]]["dgrp"],
                        "dgrpfunc" => $isc[$ret["mod{$i}code"]]["dgrpfunc"],
                        "dgrpval" => $isc[$ret["mod{$i}code"]]["dgrpval"],
                        "prop" => $isc[$ret["mod{$i}code"]]["prop"]
                    ];
                    $params["par"] = $ret["mod{$i}param"];
                    $params["min"] = $ret["mod{$i}min"];
                    $params["max"] = $ret["mod{$i}max"];

                    $props[$i] = $idesc->getDesc($params);
                }
            }

            // dump($props);
            $autoprefix = "Automagic/Auto prefix Mods";
        }

        $strbonus = (!empty($r['StrBonus'])) ? "Strength Bonus: <span class='white'>{$r['StrBonus']}" : "";

        if (empty($r['mindmg']) OR empty($r['maxdmg'])) {
            $mindmg = $r['2handmindam'];
            $maxdmg = $r['2handmaxdam'];
        } else {
            $mindmg = $r['mindmg'];
            $maxdmg = $r['maxdmg'];
        }

        $tr .= <<<EOT
          <tr>
            <td style="text-align: left">
                <ul>
                    <li class="white h1">$name</li>
                    <li class=""><img src="img/items/{$r['invfile']}.png"></li>
                    <li class="white">Damage: <span class="blue">[$mindmg - $maxdmg]</span></li>
                    <li class="white">Weight: <span class="blue">{$r['speed']}</span></li>
                    <li class="white">Level: <span class="blue">{$r['level']}</span></li>
                    <li class="white">Required Level: <span class="blue">{$r['levelreq']}</span></li>
                    <li class="white">Required Strength: <span class="blue">{$r['reqstr']}</span></li>
                    <li class="white">Indestructable: <span class="blue">$indestructable</span></li>
                    <li class="white">Durability: <span class="blue">{$r['durability']}</span></li>
                    <li class="gold">Cost: <span class="white">{$r['cost']}</span></li>

                    <li class="blue">Normal:
                        <spap class="white">{$strings[$r['normcode']]}</span>
                    </li>
                    <li class="blue">Exceptional:
                        <span class="white">{$strings[$r['ubercode']]}</span>
                        $imgSet
                    </li>
                    <li class="blue">Elite:
                        <span class="white">{$strings[$r['ultracode']]}</span>
                        $imgUnique
                    </li>
                    <li class="blue">Sockets: <span class="white">{$r['gemsockets']}</span></li>
                    <li class="blue" style="margin-top: 30px;">$autoprefix<span class="white">{$props[0]}</span></li>
                    <li class="blue"><span class="white">{$props[1]}</span></li>
                    <li class="blue" style="margin-bottom: 30px;"><span class="white">{$props[2]}</span></li>
                    <li class="blue">$strbonus</li>
                    <li class="blue">$block</li>
                </ul>
            </td>
            <td>Level: <br> {$r['level']}</td>
            <td>Level Required: <br> {$r['levelreq']}</td>
            <td>Requird Strength: <br> {$r['reqstr']}</td>
            <td>Min Dmg: <br> {$mindmg}</td>
            <td>Max Dmg: <br> {$maxdmg}</td>
          </tr>
EOT;
    }



    $htmlFinal = $htmltop . $tableTop . $tr . $tableBottom . $htmlbottom;

    ob_flush();
    echo $htmlFinal;

    file_put_contents("$dir/weapons.html", $htmlFinal);
    header("Location: docs/{$_SESSION['modname']}/weapons.html");
}