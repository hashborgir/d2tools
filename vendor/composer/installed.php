<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'bd7a2efed7e2365f0ddffa28f04b1dd752ae988d',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'bd7a2efed7e2365f0ddffa28f04b1dd752ae988d',
            'dev_requirement' => false,
        ),
        'formr/formr' => array(
            'pretty_version' => 'v1.4.4',
            'version' => '1.4.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../formr/formr',
            'aliases' => array(),
            'reference' => 'ff1375f1723297f05acf4947612e863c5085e8e7',
            'dev_requirement' => false,
        ),
    ),
);
