<?php
/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

// Second step after D2Config. If we made it here, D2Modder.db should
// have been created. Config writes active mod to session.

session_start();
include "./_pdo.php";
include "./config.php";

function processFilesManuallyInSqlite() {
    $bin = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? getcwd() . DIRECTORY_SEPARATOR . "bin" . DIRECTORY_SEPARATOR . "sqlite3.exe" : "/usr/bin/sqlite3";
    $dbfile = getcwd() . DIRECTORY_SEPARATOR . DB_FILE;
    $cubemain = TXT_PATH . "CubeMain.txt";
    $misc = TXT_PATH . "Misc.txt";

    exec("$bin $dbfile \".separator \\\"\\t\\\"\" \".import \\\"$cubemain\\\" cubemain\"");
    exec("$bin $dbfile \".separator \\\"\\t\\\"\" \".import \\\"$misc\\\" misc\"");
}

// check to see if config db exists or if for some reason it doesn't exist
if (file_exists(APP_DB)) {
    if (file_exists($_SESSION['modname'] . ".db")) {
        unlink($_SESSION['modname'] . ".db"); // delete old mod db file
    }
    // Set CONSTANTS (if D2Modder.db exists, D2Config set the session mod correctly)
    define('FILTER_PROPERTIES_FILE', 'filterProperties.txt');
    define('DB_FILE', $_SESSION['modname'] . ".db");
    define('TXT_PATH', $_SESSION['path']);

    // require src
    require_once "./src/D2Functions.php";
    require_once "./src/D2Database.php";
    require_once './src/D2Files.php';
    require_once './src/D2TxtParser.php';
    require_once './src/D2Tbl.php';

    // Create objects
    $files = new D2Files();
    $parser = new D2TxtParser();
    $db = new D2Database();

    // Parse all files
    foreach ($files->files as $k => $v) {
        $data[$v] = $parser->parseFile($v);
    }


    // Write all parse data to mod db
    foreach ($data as $k => $v) {
        $db->createTables($k, $v);
        $db->fillsTables($k, $v);
    }

    // Process Files Manually In Sqlite
    processFilesManuallyInSqlite();

    function toPngAll() {
        $q = realpath("bin\qdc6.exe");
        $iPath = $_SESSION['modpath'] . "\data\global\items";
        $oPath = realpath("docs\\{$_SESSION['modname']}\img\\items");
        exec("$q --first-frame-only \"$iPath\" -o \"$oPath\"");
    }

    if (!is_dir("docs\\{$_SESSION['modname']}")) {
        mkdir("docs\\{$_SESSION['modname']}");
        mkdir("docs\\{$_SESSION['modname']}\\fonts", 0777, TRUE);
        mkdir("docs\\{$_SESSION['modname']}\\img\items", 0777, TRUE);
        mkdir("docs\\{$_SESSION['modname']}\\res", 0777, TRUE);
    }
    rcopy(realpath('docs/template/fonts'), realpath("docs\\{$_SESSION['modname']}\\fonts"));
    rcopy(realpath('docs/template/img'), realpath("docs\\{$_SESSION['modname']}\\img"));
    rcopy(realpath('docs/template/res'), realpath("docs\\{$_SESSION['modname']}\\res"));

    toPngAll();

    /*
      Process tbl files
     */
    $tbl = $_SESSION['tbl'];
    // ddump($tbl);
    /*
      $string = D2Tbl::getStrings($tbl."string.tbl");
      $stringExpansion = D2Tbl::getStrings($tbl."expansionstring.tbl");
      $stringPatch = D2Tbl::getStrings($tbl."patchstring.tbl");
      $strings = array_merge($stringPatch, $stringExpansion, $string);
     */
    $strs = ["dummy" => "dummy"];
    foreach (glob($tbl . "*.tbl") as $filename) {
        $strings = D2Tbl::getStrings($filename);
        if (!empty($strings))
            $strs = array_merge($strs, $strings);
    }
    $db->writeTbl($strs);

    // write each table to invidual db table
    $tbl = $_SESSION['tbl'];
    foreach (glob($tbl . "*.tbl") as $filename) {
        $tblName = pathinfo($filename)['filename'];
        $strings = D2Tbl::getStrings($filename);
        if (!empty($strings)) {
            $db->writeTbls($tblName, $strings);
        }
        
    }
} else {
    // if config db does not exist, go to configure page
    header("Location: /src/D2Config.php");
}
require_once './src/D2Strings.php';        
new D2Strings();


// put in html redirect as backup, because
// for some odd reason windows gives
// an error on header() but linux does not.
?>
<!doctype html>
<html lang="en">
    <head>
        <meta http-equiv="refresh" content="0; URL=/" />
    </head>
    <body>
    </body>
</html>