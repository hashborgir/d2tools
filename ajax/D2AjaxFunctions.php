<?php

function getIscStrings($iscStat) {
	$sql = "
SELECT 
	(
		SELECT String
		FROM itemstatcost as i
		LEFT JOIN strings AS `s` ON `i`.descstrpos = `s`.`Key`
		WHERE `Stat` = '$iscStat'
	) 
AS string1,
	(
		SELECT String
		FROM itemstatcost as i
		LEFT JOIN strings AS `s` ON `i`.DescStr2 = `s`.`Key`
		WHERE `Stat` = '$iscStat'
	)
AS string2,
(
		SELECT descfunc
		FROM itemstatcost as i
		LEFT JOIN strings AS `s` ON `i`.descstrpos = `s`.`Key`
		WHERE `Stat` = '$iscStat'
	) 
AS descfunc,
(
		SELECT descval
		FROM itemstatcost as i
		LEFT JOIN strings AS `s` ON `i`.descstrpos = `s`.`Key`
		WHERE `Stat` = '$iscStat'
	) 
AS descval				
";

	return PDO_FetchRow($sql);
}
