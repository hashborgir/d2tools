<?php

include "./config.php";
require_once "../src/D2Functions.php";
require_once '../src/D2ItemData.php';
require_once '../src/D2ItemDesc.php';
$idata = new D2ItemData();
$idesc = new D2ItemDesc();
/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

$cmd = $_GET['cmd'] ?? '';
$sort = $_GET['sort'] ?? '';
$view = $_GET['view'] ?? '';

if ($_GET['cmd'] === "getUniqueItem") {
    $index = $_GET['index'];
    echo $idata->uniqueItems($index);
}

if ($cmd === "sortBy") {
    $sql = "SELECT `index`, `$sort` FROM uniqueitems WHERE enabled='1' ORDER BY ? ASC";
    $res = PDO_FetchAll($sql, [$sort]);

    // Sort the results if the sorting is based on 'index'
    if ($sort === 'index') {
        sort($res);
    }

    // Generate HTML options based on the sorted results
    $html = '';
    foreach ($res as $r) {
        if ($sort === 'index') {
            $html .= "<option value=\"{$r['index']}\">{$r['index']}</option>";
        } else {
            $html .= "<option value=\"{$r['index']}\">$r[$sort] - {$r['index']}</option>";
        }
    }

    // Output the generated HTML
    echo $html;
}




//@cmd = viewOnly

if ($cmd == "viewOnly") {

    $table = 'misc';
    $sql = "SELECT uniqueitems.`index`, uniqueitems.`code`, misc.`type`
FROM uniqueitems
LEFT JOIN misc ON uniqueitems.`code` = misc.`code`
WHERE `type` IS NOT NULL AND uniqueitems.`code`='$view'";
    if ($view == 'armo') {
        $table = 'armor';
        $sql = "SELECT uniqueitems.`index`, uniqueitems.`code`, $table.`code`
FROM uniqueitems
LEFT JOIN $table ON uniqueitems.`code` = $table.`code`
WHERE `type` IS NOT NULL AND uniqueitems.`code`= $table.`code` AND $table.`code` != '' ORDER BY `index`";
    }
    if ($view == 'weap') {
        $table = 'weapons';
        $sql = "SELECT uniqueitems.`index`, uniqueitems.`code`, $table.`code`
FROM uniqueitems
LEFT JOIN $table ON uniqueitems.`code` = $table.`code`
WHERE `type` IS NOT NULL AND uniqueitems.`code`= $table.`code` AND $table.`code` != '' ORDER BY `index`";
    }
    if ($view == "char") { // charm
        $sql .= " OR uniqueitems.`code`='cm1' OR uniqueitems.`code`='cm2' OR uniqueitems.`code`='cm3'";
    }
    $res = PDO_FetchAll($sql);
    $html = '';
    foreach ($res as $r) {
        if ($sort == 'index') {
            $html .= "<option value=\"{$r['index']}\">{$r['index']}</option>";
        } else {
            $html .= "<option value=\"{$r['index']}\">{$r['index']}</option>";
        }
    }
    echo $html;
}

// @cmd = search
if ($cmd == 'search') {
    $search = $_GET['search'];
    $sql = "SELECT * FROM uniqueitems WHERE `index` LIKE '%$search%' OR code LIKE '%$search%' AND enabled=1 ORDER BY `index`";
    $res = PDO_FetchAll($sql);

    $html = '';
    foreach ($res as $r) {
        if ($sort == 'index') {
            $html .= "<option value=\"{$r['index']}\">{$r['index']}</option>";
        } else {
            $html .= "<option value=\"{$r['index']}\">{$r['index']}</option>";
        }
    }
    echo $html;
}


// @cmd = getString
// @arg = property, par, min, max
if ($cmd == 'getString') {
    $prop = $_GET['prop'];

    // stat2,stat3,stat4,stat5,stat6,stat7
    if (!empty($prop)) {
        $sql = "SELECT stat1 FROM properties WHERE code = ?";
        $stat = array_filter(PDO_FetchRow($sql, [$prop]));
        $s = array_filter($idata->getIscStrings($prop));

        // now combine isc strings with par, min, max
        $params = array_merge($_GET, $s);
        $return = $idesc->getDesc($params);
    }
    /*  This goes into D2ItemDesc->getDesc();
      $s1 = $params['string1'];
      $s2 = $params['string2'];
      $prop = $params['prop'];
      $par = $params['par'];
      $min = (int) $params['min'];
      @return string
     */

    header('Content-Type: application/json');
    echo json_encode($return, JSON_INVALID_UTF8_IGNORE);
}

function saveFile() {
    $bin = '..\bin\sqlite3.exe ';
    $dbfile = '../' . $_SESSION['modname'] . ".db";
    $outputFile = TXT_PATH . 'uniqueitems.txt';

    // Prepare the command
    $command = escapeshellarg($bin) . " " . escapeshellarg($dbfile) . ' ".mode tabs" ".header on" "SELECT * FROM uniqueitems;" > ' . escapeshellarg($outputFile);
    $output = exec($command);
    var_dump($command);
    return $output;
}

if ($cmd == 'delete') {

    $sql = "DELETE FROM uniqueitems WHERE `index` = \"{$_GET['index']}\";";
    $res = PDO_Execute($sql);
    saveFile();
    header('Content-Type: application/json');
    echo json_encode($res, JSON_INVALID_UTF8_IGNORE);
}


if ($cmd == "save") {
    $_GET['code'] = array_filter($_GET['code']);
    $_GET['code'] = $_GET['code'][0];
    unset($_GET['formtype']);
    unset($_GET['submit']);
    unset($_GET['item']);
    unset($_GET['cmd']);
    
    // if ladder or carry1 is 0, set empty field.
    $_GET['ladder'] = $_GET['ladder'] ?? '';
    $_GET['carry1'] = $_GET['carry1'] ?? '';

    $index = $_GET['index'];
    $sql = "SELECT COUNT(*) FROM uniqueitems WHERE `index` = ?";

    $indexExists = PDO_FetchOne($sql, [$index]);
    $columns = array_keys($_GET);

    // Build the INSERT query
    $insertQuery = 'INSERT INTO uniqueitems (';
    foreach ($_GET as $k => $v) {
        $column = str_replace("_", " ", $k);
        $insertQuery .= "`$column`, ";
    }
    $insertQuery = rtrim($insertQuery, ", ");
    $insertQuery .= ") VALUES (";
    foreach ($_GET as $k => $v) {
        $insertQuery .= "\"$v\", ";
    }
    $insertQuery = rtrim($insertQuery, ", ");
    $insertQuery .= ")";

    // Build the UPDATE query
    $updateQuery = 'UPDATE uniqueitems SET ';

    foreach ($_GET as $k => $v) {
        $column = str_replace("_", " ", $k);
        $updateQuery .= "`$column` = \"$v\", ";
    }
    $updateQuery = rtrim($updateQuery, ", ");

    $updateQuery .= " WHERE `index`=\"$index\"";

    if ($indexExists) {
        $res = PDO_Execute($updateQuery);
    } else {
        $res = PDO_Execute($insertQuery);
    }


    $output = saveFile();

    header('Content-Type: application/json');
    echo json_encode($output, JSON_INVALID_UTF8_IGNORE);
}