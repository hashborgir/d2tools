<?php
/*

	Copyright (C) 2021 Hash Borgir

	This file is part of D2Modder 

	Redistribution and use in source and binary forms, with
	or without modification, are permitted provided that the
	following conditions are met:

	* Redistributions of source code must retain the above
	  copyright notice, this list of conditions and the
	  following disclaimer.

	* Redistributions in binary form must reproduce the above
	  copyright notice, this list of conditions and the
	  following disclaimer in the documentation and/or other
	  materials provided with the distribution.

	* This software must not be used for commercial purposes 
	* without my consent. Any sales or commercial use are prohibited
	* without my express knowledge and consent.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY! 

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
	CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
session_start();
include "./_pdo.php";
include "./config.php";

require_once 'src/D2Functions.php';

// if config db exists, connect to it and grab list of mods
if (file_exists(APP_DB)) {
	PDO_Connect("sqlite:".APP_DB);

	$sql = "SELECT * FROM D2Modder";
	$mods = PDO_FetchAll($sql);

	if (empty($mods)){
		header("Location: /src/D2Config.php");
	}
	
	if (!empty($_POST)) {
		$sql = "SELECT * FROM D2Modder WHERE `modname`='{$_POST['modname']}'";
		$mod = PDO_FetchRow($sql);

		if (isset($_POST['deletemod'])) {
			$sql = "DELETE FROM D2Modder WHERE modname='{$_POST['modname']}'";
			PDO_Execute($sql);
			unlink($_POST['modname'] . ".db");
            rrmdir(realpath("docs\\{$_POST['modname']}"));
			header("Refresh:0");
		} else {
			$time = time();
			$sql = "UPDATE `D2Modder` SET `lastused`=$time WHERE modname='{$_POST['modname']}'";
			PDO_Execute($sql);
                $_SESSION['modname'] = $mod['modname'];
                $_SESSION['path'] = $mod['path'];
                $_SESSION['modpath'] = $mod['modpath'];
                $_SESSION['savepath'] = $mod['savepath'];
                $_SESSION['tbl'] = $mod['tbl'];
                $_SESSION['docpath'] = 'docs/'.$_SESSION['modname'];
                
			// ddump($_SESSION);
            
			header("Location: /");
		}
	}
} else {
	// if config db does not exist, go to config page
	header("Location: /src/D2Config.php");
}
?>
<!doctype html>
<html lang="en">
	<?php
	/* Require the <head> section */
    $css = '';
	require_once "src/head.php";
	?>
    <body style="background: white;">
		<div class="center container container-top">

			<img src="img/Diablo2.png" style="float:left"><h1 syle="display:inline; font-weight: 900"><?php echo $title . " " . $version; ?><span style="font-family: Lato !important; font-size: 14px;"> <?php echo " By" . $author ?></span></h1>	
			<a class="btn btn-outline-danger" style="color:red; font-size: 18px;float:right;" href="/">X</a>
			<hr style="margin: 60px;">
			<div class="offset-2 col-8">
				<h2>Manage Mods</h2>
				<div style="margin-top: 20px;" class="ctrl-config">
					<form class="" enctype="multipart/form-data" style="font-family: Lato; font-size: 14pt;text-align:center;" action="" method="post">
						<p>
							<select id="mods" name="modname" required="required" class="custom-select">							<?php foreach ($mods as $mod) { ?>
									<option value="<?php echo $mod['modname'] ?>">
										<?php echo $mod['modname'] ?>: <?php echo $mod['path'] ?>
									</option>
								<?php } ?>
							</select>
						</p>
						<p>
							<input type="submit" value="Select" name="submit" class="btn-success btn">
                        			<a style="font-weight: bold;" class="btn btn-info" href="/src/D2Config.php">Setup New Mod</a>
							<a style="font-weight: bold;" class="btn btn-warning" href="/">Cancel</a>
							<input style="float:right;" type="submit" value="Delete Mod" name="deletemod" class="btn btn-danger">
						</p>
						<p style="font-family: lato;font-size: 14px;">Set active mod for D2Modder. <br>If you delete a mod, select another mod to make it active. <br>Deleting a mod here will only delete the .db database file, and remove the mod entry from D2Modder. <br>Your mod files will not be deleted.</p>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>