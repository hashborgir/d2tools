<?php

$htmltop = <<<EOT
        
<!doctype html>
<html lang="en">
    <head>

	<meta property="og:title" content="Diablo 2: Ironman Mod by Hash">
	<meta property="og:site_name" content="Ironman Mod">
	<meta property="og:url" content="https://im.stoned.io">
	<meta property="og:description" content="An old school Ironman rules style gameplay mod for Diablo 2! New game mechanics!">
	<meta property="og:type" content="website">
	<meta property="og:image" content="https://gitlab.com/hashborgir/d2-ironman/-/raw/master/Screenshots/TitleScreen.png">
   
        
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="res/bootstrap.min.css">
        <link rel="stylesheet" href="res/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="stylesheet" href="https://bootswatch.com/4/sketchy/bootstrap.min.css">
        <link rel="stylesheet" href="res/style.css">
        <style>

        body{
             background: white;
             font-family: ;
        }
         table, table th, table td, table tr {
             background: #000;
             color: #ccc;
        }
         .item {
             text-align: center;
             position:relative;
             right:0px;
             margin: 0 auto;
        }
         .item_desc {
             height: 600px;
             width: 620px;
             position: relative;
             left: 0px;
             top: 0px;
             text-align: center;
             padding: 10px;
             text-transform: uppercase;
        }
         .item_stats{
             width: auto;
             height: auto;
             background: #111;
             min-height: 260px;
        }
         .item_desc {
             height: auto;
             text-align: center;
             padding: 10px;
             text-transform: uppercase;
        }
         .green {
             color: #00FF00;
            /*Set items*/
        }
         .gold{
             color: #A59263;
            /*Set items*/
        }
         .orange{
             color: #FFA500;
            /*Set items*/
        }
         .yellow {
             color: #FFFF00;
            /*Set items*/
        }
         .blue {
             color: #4169E1;
        }
         .red { color: red;}
         .purple { color: purple}
         .white { color: white}
         .black {color: black}
         td {
             height: 240px;
             text-align: center;
             vertical-align: middle;
             padding: 10px;
        }
         .uname {
             background: #222;
        }
         .ulvl {
        }
         .ulvlreq {
        }
         .utype {
             background: #111;
        }
         .ucode {
        }
         .uitem {
        }
         .rprops {
             background: #222 !important;
             border: 1px solid #ccc;
             padding: 30px 5px;
        }
         .rw {
             text-align: center;
             border: 1px solid #ccc;
             margin: 10px;
             padding: 10px;
             background: #111;
        }
         .rw-name {
             font-size: 28px;
             text-align: center;
        }
         .rw-type {
             font-family: ExocetLight;
        }
         .rw-rune {
             color: #ccc;
        }
         .rw-count {
             background: #222;
             border: 1px solid #ccc;
             border-radius: 50;
             display: block;
             height: 60px;
             width: 60px;
             line-height: 60px;
             -moz-border-radius: 30px;
            /* or 50% */
             border-radius: 30px;
            /* or 50% */
             margin: 0 auto;
             color: white;
             text-align: center;
             font-size: 2em;
        }
         .rune-img {
             padding: 1px;
             margin:1px;
             height: 48px;
             width: 48px;
        }
         table {
        }
         .desc {
             font-family: Tahoma,Arial,Sans;
             background: #222;
        }
         .input1,.input3,.input5,.input7{
             background: #000;
        }
         .input2, .input4, .input6 {
             background: #000;
        }
         .output1{
             background: #222;
        }
         .output2 {
             background: #222;
        }
         .output3 {
             background: #222;
        }
         td {
             height: 240px;
             text-align: center;
             vertical-align: middle;
             padding: 10px;
        }
 
        .gemtable td img {
            max-height: 64px;
        }

        table {
            width: 100%;
        }
        
        
        body {
          overflow-x: ; /* Hide scrollbars */
        }        
        
        nav { margin-bottom: 60px;}
        
        input[type=search] { width: 320px !important; background: #222; color: white;}
        
        </style>

        <script src="res/jquery-3.5.1.min.js"></script>
        <script src="res/bootstrap.bundle.min.js"></script>

        <link rel="stylesheet" type="text/css" href="res/datatables.min.css"/>
        <script type="text/javascript" src="res/datatables.min.js"></script>

        <script>
            $(document).ready(function () {
                $('.cubetable').dataTable();
                $('.utable').dataTable();
                $('.gemtable').dataTable();
                $('.rtable').dataTable();
                $('.mtable').dataTable();
        
        
            userAgent = window.navigator.userAgent
            if(userAgent.includes("Windows") == true) { 
               $('.itemimage').css("margin-left","40px");
               // $('nav').remove();
            }
        
            });
        </script>

        <title>D2Modder</title>
    </head>

    <body>
        <div class="">           
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <p class="navbar-brand">{$_SESSION['modname']}</p>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav amenu">

                <li class="nav-item">
                  <a class="nav-link btn btn-info" target="_blank" href="cubemain.html">Cube Recipes</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn btn-warning" target="_blank" href="gems.html">Gems</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn btn-success" target="_blank" href="runewords.html">Runewords</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link btn btn-danger" target="_blank" href="uniqueitems.html">Unique Items</a>
                </li>						  
                <li class="nav-item">
                  <a class="nav-link btn btn-info" target="_blank" href="misc.html">Misc Items</a>
                </li>	
               </ul>
            </div>
            </nav>            
EOT;
