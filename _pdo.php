<?php
/**
 * PDO helper functions.
 *
 * This code provides helper functions for working with PDO.
 * It includes functions for connecting to a database, executing queries,
 * fetching rows and values, and retrieving error information.
 *
 * @package PDO_Helper
 * @copyright Copyright (c) 2012-2014 The PHP Desktop authors
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @link http://code.google.com/p/phpdesktop/ Website
 * 
 * Commented and cleaned by Hash Borgir 
 */

/**
 * Connects to the database using PDO.
 *
 * @param string $dsn The Data Source Name, a string that describes the database source.
 * @param string $user The username for the database connection.
 * @param string $password The password for the database connection.
 * @return void
 */
function PDO_Connect($dsn, $user = "", $password = "")
{
    global $PDO;
    $PDO = new PDO($dsn, $user, $password);
    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    $PDO->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
}

/**
 * Fetches a single value from a query.
 *
 * @param string $query The SQL query to execute.
 * @param array|null $params An array of values to bind to the query parameters.
 * @return mixed|null The fetched value, or null if no value is found.
 */
function PDO_FetchOne($query, $params = null)
{
    global $PDO;
    if (isset($params)) {
        $stmt = $PDO->prepare($query, [PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]);
        $stmt->execute($params);
    } else {
        $stmt = $PDO->query($query);
    }
    $row = $stmt->fetch(PDO::FETCH_NUM);
    if ($row) {
        return $row[0];
    } else {
        return null;
    }
}

/**
 * Fetches a single row from a query.
 *
 * @param string $query The SQL query to execute.
 * @param array|null $params An array of values to bind to the query parameters.
 * @return array|false The fetched row as an associative array, or false if no row is found.
 */
function PDO_FetchRow($query, $params = null)
{
    global $PDO;
    if (isset($params)) {
        $stmt = $PDO->prepare($query, [PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]);
        $stmt->execute($params);
    } else {
        $stmt = $PDO->query($query);
    }
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

/**
 * Fetches all rows from a query.
 *
 * @param string $query The SQL query to execute.
 * @param array|null $params An array of values to bind to the query parameters.
 * @return array The fetched rows as an associative array.
 */
function PDO_FetchAll($query, $params = null)
{
    global $PDO;
    if (isset($params)) {
        $stmt = $PDO->prepare($query, [PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]);
        $stmt->execute($params);
    } else {
        $stmt = $PDO->query($query);
    }
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Fetches all rows from a query and groups them by a specific column.
 *
 * @param string $query The SQL query to execute.
 * @param array|null $params An array of values to bind to the query parameters.
 * @return array The fetched rows as an associative array, grouped by the specified column.
 */
function PDO_FetchAllAssoc($query, $params = null)
{
    global $PDO;
    if (isset($params)) {
        $stmt = $PDO->prepare($query, [PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]);
        $stmt->execute($params);
    } else {
        $stmt = $PDO->query($query);
    }
    return $stmt->fetchAll(PDO::FETCH_COLUMN | PDO::FETCH_GROUP);
}

/**
 * Fetches key-value pairs from a query.
 *
 * @param string $query The SQL query to execute.
 * @param array|null $params An array of values to bind to the query parameters.
 * @return array The fetched key-value pairs as an associative array.
 */
function PDO_FetchAssoc($query, $params = null)
{
    global $PDO;
    if (isset($params)) {
        $stmt = $PDO->prepare($query, [PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]);
        $stmt->execute($params);
    } else {
        $stmt = $PDO->query($query);
    }
    $rows = $stmt->fetchAll(PDO::FETCH_NUM);
    $assoc = [];
    foreach ($rows as $row) {
        $assoc[$row[0]] = $row[1];
    }
    return $assoc;
}

/**
 * Executes a query.
 *
 * @param string $query The SQL query to execute.
 * @param array|null $params An array of values to bind to the query parameters.
 * @return PDOStatement|false The PDOStatement object representing the executed query, or false on failure.
 */
function PDO_Execute($query, $params = null)
{
    global $PDO;
    if (isset($params)) {
        $stmt = $PDO->prepare($query, [PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => false]);
        $stmt->execute($params);
        return $stmt;
    } else {
        return $PDO->query($query);
    }
}

/**
 * Returns the last inserted row ID.
 *
 * @return string The last inserted row ID.
 */
function PDO_LastInsertId()
{
    global $PDO;
    return $PDO->lastInsertId();
}

/**
 * Returns information about the last error that occurred on the database connection.
 *
 * @return array The error information as an array.
 */
function PDO_ErrorInfo()
{
    global $PDO;
    return $PDO->errorInfo();
}
?>
