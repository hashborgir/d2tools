<?php

/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes 
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 *
 */
class D2Database {

    /**
     * D2Database constructor.
     */
    public function __construct() {
        PDO_Connect("sqlite:" . DB_FILE); // Connect to the SQLite database using the specified file.
    }

    /**
     * Create tables based on file and data.
     *
     * @param string $file The file name.
     * @param array $data The data array.
     * @return void
     */
    public function createTables($file, $data) {
        $tableName = basename($file); // Extract the base name of the file.
        $tableName = strtolower(substr($tableName, 0, -4)); // Convert the table name to lowercase and remove the file extension.
        $sql = '';

        if (!empty($data[0])) { // Check if the data array is not empty.
            $sql = "CREATE TABLE IF NOT EXISTS `$tableName` ("; // Start building the SQL query to create a table.

            foreach ($data[0] as $k => $v) { // Iterate over the columns and their corresponding values.
                if (is_numeric($v)) { // Check if the value is numeric.
                    $dataType = "INT"; // Set the data type as INT.
                } else {
                    $dataType = "VARCHAR(255)"; // Set the data type as VARCHAR(255).
                }
                $sql .= "`$k` $dataType DEFAULT '',"; // Add the column and its data type to the SQL query.
            }

            $sql = rtrim($sql, ","); // Remove the trailing comma.
            $sql .= ")"; // Close the table creation query.
            $res = PDO_Execute($sql); // Execute the query to create the table.
        }
    }

    /**
     * Fill tables based on file and data.
     *
     * @param string $file The file name.
     * @param array $data The data array.
     * @return void
     */
    public function fillsTables($file, $data) {
        $tableName = basename($file); // Extract the base name of the file.
        $tableName = strtolower(substr($tableName, 0, -4)); // Convert the table name to lowercase and remove the file extension.
        $sql = '';

        if (!empty($data)) { // Check if the data array is not empty.
            foreach ($data as $d) { // Iterate over the data rows.
                if (!empty($d)) { // Check if the row is not empty.
                    $sql = "INSERT INTO `$tableName` ("; // Start building the SQL query to insert into the table.

                    foreach ($d as $k => $v) { // Iterate over the column names and values.
                        $sql .= "`$k`" . ","; // Add the column name to the SQL query.
                    }

                    $sql = rtrim($sql, ","); // Remove the trailing comma.
                    $sql .= ") ";
                }
            }
        }

        if (!empty($data)) { // Check if the data array is not empty.
            $sql .= "VALUES ";

            foreach ($data as $d) { // Iterate over the data rows.
                if (!empty($d)) { // Check if the row is not empty.
                    $sql .= "(";

                    foreach ($d as $k => $v) { // Iterate over the column values.
                        $sql .= '"' . $v . '"' . ","; // Add the value to the SQL query.
                    }

                    $sql = rtrim($sql, ","); // Remove the trailing comma.
                    $sql .= "), ";
                }
            }

            $sql = rtrim($sql, ", "); // Remove the trailing comma and space.
            $sql .= ";";

            $res = PDO_Execute($sql); // Execute the query to insert data into the table.
        }
    }

    /**
     * Write data to the 'strings' table.
     *
     * @param array $data The data array.
     * @return void
     */
    public function writeTbl($data) {
        $sql = 'CREATE TABLE IF NOT EXISTS `strings` (`Key` VARCHAR(255), `String` VARCHAR(255));'; // Create the 'strings' table if it doesn't exist.
        $res = PDO_Execute($sql); // Execute the query to create the table.

        $sql = "INSERT INTO `strings` (`Key`,`String`) VALUES ";

        foreach ($data as $k => $v) { // Iterate over the key-value pairs in the data array.
            $sql .= "(\"$k\",\"$v\"),"; // Add the key-value pair to the SQL query.
        }

        $sql = rtrim($sql, ", "); // Remove the trailing comma and space.
        $sql .= ";";

        $res = PDO_Execute($sql); // Execute the query to insert data into the 'strings' table.
    }

    /**
     * Write data to a specific table.
     *
     * @param string $table The table name.
     * @param array $data The data array.
     * @return void
     */
    public function writeTbls($table, $data) {
        $sql = "CREATE TABLE IF NOT EXISTS `$table` (`Key` VARCHAR(255), `String` VARCHAR(255));"; // Create the specified table if it doesn't exist.
        $res = PDO_Execute($sql); // Execute the query to create the table.

        $sql = "INSERT INTO `$table` (`Key`,`String`) VALUES ";

        foreach ($data as $k => $v) { // Iterate over the key-value pairs in the data array.
            $sql .= "(\"$k\",\"$v\"),"; // Add the key-value pair to the SQL query.
        }

        $sql = rtrim($sql, ", "); // Remove the trailing comma and space.
        $sql .= ";";

        $res = PDO_Execute($sql); // Execute the query to insert data into the specified table.
    }

    /**
     * Get a string value based on the key.
     *
     * @param string $key The key.
     * @return mixed The fetched row.
     */
    public function getString($key) {
        $sql = "SELECT String FROM `strings` WHERE `Key`='$key'"; // Construct the SQL query to retrieve the string value.
        $res = PDO_FetchRow($sql); // Execute the query and fetch the row.
        return $res; // Return the fetched row.
    }
}

