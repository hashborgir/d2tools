<?php

/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 *
 */
class D2ItemDesc {
	/*

	  DescFunc: the function used for generating the description for this stat, this works like the descfuncs in SkillDesc.txt pretty much. See below for an explanation of individual functions, these are listed the way they would show up if DescVal is set to 1. This follows the syntax Char used in the old fileguide, but includes the functions he didn't cover.


	  1 - +[value] [string1]
	  2 - [value]% [string1]
	  3 - [value] [string1]
	  4 - +[value]% [string1]
	  5 - [value*100/128]% [string1]
	  6 - +[value] [string1] [string2]
	  7 - [value]% [string1] [string2]
	  8 - +[value]% [string1] [string2]
	  9 - [value] [string1] [string2]
	  10 - [value*100/128]% [string1] [string2]
	  11 - Repairs 1 Durability In [100 / value] Seconds
	  12 - +[value] [string1]
	  13 - +[value] to [class] Skill Levels
	  14 - +[value] to [skilltab] Skill Levels ([class] Only)
	  15 - [chance]% to case [slvl] [skill] on [event]
	  16 - Level [sLvl] [skill] Aura When Equipped
	  17 - [value] [string1] (Increases near [time])
	  18 - [value]% [string1] (Increases near [time])
	  19 - this is used by stats that use Blizzard's sprintf implementation (if you don't know what that is, it won't be of interest to you eitherway I guess), look at how prismatic is setup, the string is the format that gets passed to their sprintf spinoff.
	  20 - [value * -1]% [string1]
	  21 - [value * -1] [string1]
	  22 - [value]% [string1] [montype] (warning: this is bugged in vanilla and doesn't work properly, see CE forum)
	  23 - [value]% [string1] [monster]
	  24 - used for charges, we all know how that desc looks
	  25 - not used by vanilla, present in the code but I didn't test it yet
	  26 - not used by vanilla, present in the code but I didn't test it yet
	  27 - +[value] to [skill] ([class] Only)
	  28 - +[value] to [skill]



	  DescVal: Controls whenever and if so in what way the stat value is shown,

	 * 0 = doesn't show the value of the stat,
	 * 1 = shows the value of the stat infront of the description,
	 * 2 = shows the value of the stat after the description.

	  DescStrPos: The string used for the description when the stat value is positive.

	  DescStrNeg: The string used for the description when the stat value is negative.

	  DescStr2: An additional string used by some DescFuncs, usually used as a suffix or for aditional info (such as per character level etc).
	 *
	 *
	 *
	 * DATA SAMPLE
	 *
	 *
	 *
	  array (size=174)
	  'item_absorbcold' =>
	  array (size=6)
	  'Stat' => string 'item_absorbcold' (length=15)
	  'ID' => string '149' (length=3)
	  'descfunc' => string '1' (length=1)
	  'descval' => string '1' (length=1)
	  'descstrpos' => string 'ModStr5n' (length=8)
	  'String' => string 'Cold Absorb' (length=11)
	  'item_absorbcold_percent' =>
	  array (size=6)
	  'Stat' => string 'item_absorbcold_percent' (length=23)
	  'ID' => string '148' (length=3)
	  'descfunc' => string '2' (length=1)
	  'descval' => string '2' (length=1)
	  'descstrpos' => string 'ModStr5m' (length=8)
	  'String' => string 'Cold Absorb' (length=11)
	  'item_absorb_cold_perlevel' =>
	  array (size=6)
	  'Stat' => string 'item_absorb_cold_perlevel' (length=25)
	  'ID' => string '234' (length=3)
	  'descfunc' => string '6' (length=1)
	  'descval' => string '1' (length=1)
	  'descstrpos' => string 'ModStre9p' (length=9)
	  'String' => string 'Absorbs Cold Damage' (length=19)
	  'item_absorbfire' =>
	  array (size=6)
	  'Stat' => string 'item_absorbfire' (length=15)
	  'ID' => string '143' (length=3)
	  'descfunc' => string '1' (length=1)
	  'descval' => string '1' (length=1)
	  'descstrpos' => string 'ModStr5h' (length=8)
	  'String' => string 'Fire Absorb' (length=11)
	 *
	 *

	 */

    /**
     * @var string
     */
    public $str = '';
// takes value, param.
// value == Value to show on this stat
//descfunc 14

    /**
     * @var
     */
    public $skilltabsDesc;
    /**
     * @var
     */
    public $skilltabsDescClean;
    /**
     * @var string[]
     */
    public $skilltabs = [
		//ama
		'0' => 'StrSklTabItem3',
		'1' => 'StrSklTabItem2',
		'2' => 'StrSklTabItem1',
		// sor
		'3' => 'StrSklTabItem15',
		'4' => 'StrSklTabItem14',
		'5' => 'StrSklTabItem13',
		//nec
		'6' => 'StrSklTabItem8',
		'7' => 'StrSklTabItem7',
		'8' => 'StrSklTabItem9',
		//pal
		'9' => 'StrSklTabItem6',
		'10' => 'StrSklTabItem5',
		'11' => 'StrSklTabItem4',
		//bar
		'12' => 'StrSklTabItem11',
		'13' => 'StrSklTabItem12',
		'14' => 'StrSklTabItem10',
		//dru
		'15' => 'StrSklTabItem16',
		'16' => 'StrSklTabItem17',
		'17' => 'StrSklTabItem18',
		// ass
		'18' => 'StrSklTabItem19',
		'19' => 'StrSklTabItem20',
		'20' => 'StrSklTabItem21'
	];
    /**
     * @var string[]
     */
    public $charClass = [
		"ama" => "Amazon",
		"sor" => "Sorceress",
		"nec" => "Necromancer",
		"pal" => "Paladin",
		"bar" => "Barbarian",
		"dru" => "Druid",
		"ass" => "Assassin"
	];

	/*
	 *
	 * 	Prepare values for descfunc
	  These are prop funcs from properties.txt

	  val1 to val7: Parameter to the property function, if the function can
	  use it (see class skills properties).

	  func1 to func7: Function used to assign a value to a property

	 * min: Description of the min value

	 * max: Description of the max value


	  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *
	 *
	  APPENDIX: Functions of Properties.txt

	  Here are some functions used by the properties.txt to link
	  param/min/max value to the actual bonus of the properties.
	 *
	 *
	  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


	  1 - Applies a value to a stat, can use SetX parameter.
	  2 - defensive function only, similar to 1 ???
	  3 - Apply the same min-max range as used in the previous function block
	  (see res-all).

	  4 - not used ???
	  5 - Dmg-min related ???
	  6 - Dmg-max related ???
	  7 - Dmg% related ???
	  8 - ??? use for speed properties (ias, fcr, etc ...)
	  9 - Apply the same param and value in min-max range, as used in the
	  previous function block.

	  10 - skilltab skill group ???
	  11 - event-based skills ???
	  12 - random selection of parameters for parameter-based stat ???
	  13 - durability-related ???
	  14 - inventory positions on item ??? (related to socket)
	  15 - use min field only
	  16 - use max field only
	  17 - use param field only
	  18 - Related to /time properties.
	  19 - Related to charged item.
	  20 - Simple boolean stuff. Use by indestruct.
	  21 - Add to group of skills, group determined by stat ID, uses ValX parameter.
	  22 - Individual skill, using param for skill ID, random between min-max.
	  23 - ethereal
	  24 - property applied to character or target monster ???
	  25--32 can be used in custom code.  Check plugin documentation for syntax.

	 *
	 * 	@prep
	 *
	 *
	 */

    /**
     * @param $par
     * @param $min
     * @param $max
     * @return void
     */
    public function prep($par, $min, $max) {

	}

	/*
	 *  @getDesc
	 *
	  @return string
	 *
	 *
	 * 	probably non real need to code this function.
	  Just put this in getDesc()

	  if par
	 * 		use par
	 *  else
	 *      use min


	 *

	 */

    /**
     *
     */
    public function __construct() {
		
	}


    /**
     * @param $params
     * @return false|string
     */
    public function getDesc($params = []) {
		if (empty($params))
			return false;

		//ddump($params);

		$s1 = $params['string1'];
		$s2 = $params['string2'];
		$prop = $params['prop'];
		$par = $params['par'];
		$min = (int) $params['min'];
		$v = $par;
		
//		if ($params['dgrp']){
//			$s1 = $params['gstring1'];
//			$s2 = $params['gstring2'];
//			$params['descfunc'] = $params['dgrpfunc'];
//			$params['descval'] = $params['dgrpval'];
//		}
	

		if (empty($par)) {
			$v = $min;
		}

		$max = (int) $params['max'];

		if ($min == $max) {
			$max = '';
		}

		if (empty($this->skilltabsDesc)){
		
			foreach ($this->skilltabs as $s) {
				$sql = "SELECT String FROM strings WHERE Key=?";
				$this->skilltabsDesc[] = PDO_FetchRow($sql, [$s]);
			}
		}
		//ddump($this->skilltabsDescClean);

		/*
		 * Descval 1
		 *
		 *
		 * $format = 'The %s contains %d monkeys';
		  echo sprintf($format, $num, $location);
		 *
		 *
		 *
		 */
		if ($params['descval'] == 1 || $params['descval'] == '') {

			if ($params['descfunc'] == 1) {
				if (!empty($max)) {
					$max = "-" . $max;
				}
				$this->str = "+$min$max $s1";
			}

			if ($params['descfunc'] == 2) {
				$this->str = "$v% $s1";
			}

			if ($params['descfunc'] == 3) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 4) {
				$this->str = "+$v% $s1";
			}

			if ($params['descfunc'] == 5) {
				$this->str = ($v * 100 / 128) . "  $s1";
			}

			if ($params['descfunc'] == 6) {
				$this->str = "+$par $s1 $s2";
			}

			if ($params['descfunc'] == 7) {
				$this->str = "$v% $s1 $s2";
			}
			if ($params['descfunc'] == 8) {
				$this->str = "+$v% $s1 $s2";
			}

			if ($params['descfunc'] == 9) {
				$this->str = "$v $s1 $s2";
			}

			if ($params['descfunc'] == 10) {
				$this->str = ($v * 100 / 128) . "  $s1 $s2";
			}

			if ($params['descfunc'] == 11) {

				//var_dump($params);

				if ($par == '') {
					$min = (int) $min;
					$this->str = sprintf($s1, (100 / $min));
				} else {
					$par = (int) $par;
					$this->str = sprintf($s1, (100 / $par));
				}
			}

			if ($params['descfunc'] == 12) {
				$this->str = "+$v $s1";
			}

			if ($params['descfunc'] == 13) {
				$chars = [
					"ama" => "ModStr3a",
					"pal" => "ModStr3b",
					"nec" => "ModStr3c",
					"sor" => "ModStr3d",
					"bar" => "ModStr3e",
					"dru" => "ModStre8a",
					"ass" => "ModStre8b"
				];
				$sql = "SELECT `String` FROM `strings` WHERE `Key`='{$chars[$prop]}'";
				$s1 = PDO_FetchOne($sql);
				$this->str = "+$min $s1";
			}

			if ($params['descfunc'] == 14) {



				$sql = "SELECT class
					FROM `charstats`
					WHERE `StrSkillTab1`='{$this->skilltabs[$par]}' OR
					`StrSkillTab2`='{$this->skilltabs[$par]}' OR
					`StrSkillTab3`='{$this->skilltabs[$par]}'";

				$class = PDO_FetchOne($sql);

				//ddump($class);

				$s1 = $this->skilltabsDesc[$par]['String'];

				//ddump($s1);

				$this->str = sprintf($s1, $min);

				if (!isset($par)) {
					$this->str = "Par is not set.";
				}
			}
			if ($params['descfunc'] == 15) {
				$sql = "SELECT skill FROM `skills` WHERE `Id`='$par'";
				$skill = PDO_FetchOne($sql);                                                                                               
				$this->str = sprintf($s1, $min, $params['max'], $skill);                                                                       
			}

			if ($params['descfunc'] == 16) {
                    $sql = "SELECT skill FROM `skills` WHERE `Id`='$par'";
				$aura = PDO_FetchOne($sql);                 
				if (!$max) {
					$this->str = "Level $min $aura Aura When Equipped ";
				} else {
					$this->str = "Level $min to $max $aura Aura When Equipped ";
				}
			}

			if ($params['descfunc'] == 17) {
				$this->str = "$v $s1 (Increases near [time])";
			}

			if ($params['descfunc'] == 18) {
				$this->str = "$v% $s1 (Increases near [time])";
			}

			if ($params['descfunc'] == 19) {
				$this->str = "";
			}

			if ($params['descfunc'] == 20) {
				$this->str = ($v) . "% $s1";
			}

			if ($params['descfunc'] == 21) {
				$this->str = ($v * -1) . " $s1";
			}
			if ($params['descfunc'] == 22) {
				$this->str = "$v% $s1 [MonType]";
			}

			if ($params['descfunc'] == 23) {
				$this->str = "$v% $s1 [Monster]";
			}

			if ($params['descfunc'] == 24) {
				$string = str_replace("\\", "", $s1);
				$sql = "SELECT skill FROM `skills` WHERE `Id`='$par'";
				$skill = PDO_FetchOne($sql);
				$this->str = "+$min to Skill Charges Or Something";
				$this->str = sprintf("Level $max $skill " . $string, $min, $min);
			}

			if ($params['descfunc'] == 25) {
				$this->str = "";
			}

			if ($params['descfunc'] == 26) {
				$this->str = "";
			}

			if ($params['descfunc'] == 27) {
				$sql = "SELECT skill,charclass FROM `skills` WHERE `Id`='$par'";
				$res = PDO_FetchRow($sql);
				$sql = "SELECT `skill` FROM `skills` WHERE `skilldesc`='$par' OR `Id`='$par'";
				$skill = PDO_FetchOne($sql);
				$class = $this->charClass[$res['charclass']];

				$this->str = "+$min to $skill ($class Only)";
			}

			if ($params['descfunc'] == 28) {
				$sql = "SELECT `skill` FROM `skills` WHERE `skilldesc`='$par' OR `Id`='$par'";
				$skill = PDO_FetchOne($sql);

				$this->str = "+$min to $skill ";
			}
		}
		/*
		 * Descval 0
		 *
		 *
		 *
		 *
		 *
		 *
		 *
		 */ else if ($params['descval'] == 0) {
			if ($params['descfunc'] == 1) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 2) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 3) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 4) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 5) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 6) {
				$this->str = "$s1 $s2";
			}

			if ($params['descfunc'] == 7) {
				$this->str = "$s1 $s2";
			}
			if ($params['descfunc'] == 8) {
				$this->str = "$s1 $s2";
			}

			if ($params['descfunc'] == 9) {
				$this->str = "$s1 $s2";
			}

			if ($params['descfunc'] == 10) {
				$this->str = "$s1 $s2";
			}

			if ($params['descfunc'] == 11) {
				$this->str = "Repairs 1 Durability In X seconds";
			}

			if ($params['descfunc'] == 12) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 13) {
				$this->str = "to [class] Skill Levels";
			}

			if ($params['descfunc'] == 14) {
				$this->str = "to [skilltab] Skill Levels ([class] Only)";
			}
			if ($params['descfunc'] == 15) {
				$this->str = "[chance]to cast [slvl] [skill] on [event]";
			}

			if ($params['descfunc'] == 16) {
                    $sql = "SELECT skill FROM `skills` WHERE `Id`='$par'";
				$aura = PDO_FetchOne($sql);                 
				if (!$max) {
					$this->str = "Level $min $aura Aura When Equipped ";
				} else {
					$this->str = "Level $min to $max $aura Aura When Equipped ";
				}
			}

			if ($params['descfunc'] == 17) {
				$this->str = "$s1 (Increases near [time])";
			}

			if ($params['descfunc'] == 18) {
				$this->str = "$s1 (Increases near [time])";
			}

			if ($params['descfunc'] == 19) {
				$this->str = "";
			}

			if ($params['descfunc'] == 20) {
				$this->str = "$s1";
			}

			if ($params['descfunc'] == 21) {
				$this->str = "$s1";
			}
			if ($params['descfunc'] == 22) {
				$this->str = "$s1 [MonType]";
			}

			if ($params['descfunc'] == 23) {
				$this->str = "$s1 [Monster]";
			}

			if ($params['descfunc'] == 24) {
				$this->str = "to Skill Charges Or Something";
			}

			if ($params['descfunc'] == 25) {
				$this->str = "";
			}

			if ($params['descfunc'] == 26) {
				$this->str = "";
			}

			if ($params['descfunc'] == 27) {
				$sql = "SELECT skill,charclass FROM `skills` WHERE `Id`='$par'";
				$res = PDO_FetchRow($sql);
				$this->str = "+$min to {$res['skill']} ({$this->charClass[$res['charclass']]} Only)";
			}

			if ($params['descfunc'] == 28) {
				$this->str = "to [skill] ";
			}
		}
		/*
		 * Descval 2
		 *
		 *
		 *
		 *
		 *
		 *
		 */ else if ($params['descval'] == 2) {
			if ($params['descfunc'] == 1) {
				$this->str = "$s1 +$v";
			}

			if ($params['descfunc'] == 2) {
				$this->str = "$s1 $v%";
			}

			if ($params['descfunc'] == 3) {
				$this->str = "$s1 $v";
			}

			if ($params['descfunc'] == 4) {
				$this->str = "$s1 $v%";
			}

			if ($params['descfunc'] == 5) {
				$this->str = "$s1" . ($v * 100 / 128);
			}

			if ($params['descfunc'] == 6) {
				$this->str = "$s1 $s2 +%v";
			}

			if ($params['descfunc'] == 7) {
				$this->str = "$s1 $s2 $v% ";
			}
			if ($params['descfunc'] == 8) {
				$this->str = "$s1 $s2 +$v% ";
			}

			if ($params['descfunc'] == 9) {
				$this->str = "$s1 $s2 $v";
			}

			if ($params['descfunc'] == 10) {
				$this->str = "$s1 $s2" . ($v * 100 / 128);
			}

			if ($params['descfunc'] == 11) {
				$this->str = "Repairs 1 Durability In X seconds";
			}

			if ($params['descfunc'] == 12) {
				$this->str = "$s1 +$v ";
			}

			if ($params['descfunc'] == 13) {
				$this->str = "to [class] Skill Levels +$v";
			}

			if ($params['descfunc'] == 14) {

				$sql = "SELECT class
					FROM `charstats`
					WHERE `StrSkillTab1`='{$this->skilltabs[$par]}' OR
					`StrSkillTab2`='{$this->skilltabs[$par]}' OR
					`StrSkillTab3`='{$this->skilltabs[$par]}'";

				$class = PDO_FetchOne($sql);

				$this->str = "{$this->skilltabsDescClean[$par]['String']} ($class Only) +$max";
			}
			if ($params['descfunc'] == 15) {
				$this->str = "[chance]% to case [slvl] [skill] on [event]";
			}

			if ($params['descfunc'] == 16) {
				$this->str = "Level [sLvl] [skill] Aura When Equipped ";
			}

			if ($params['descfunc'] == 17) {
				$this->str = "s1 (Increases near [time]) $v $";
			}

			if ($params['descfunc'] == 18) {
				$this->str = "$s1 (Increases near [time]) $v% ";
			}

			if ($params['descfunc'] == 19) {
				$this->str = "";
			}

			if ($params['descfunc'] == 20) {
				$this->str = "% $s1" . ($v * -1);
			}

			if ($params['descfunc'] == 21) {
				$this->str = "$s1" . ($v * -1);
			}
			if ($params['descfunc'] == 22) {
				$this->str = "$s1 [MonType] $v% ";
			}

			if ($params['descfunc'] == 23) {
				$this->str = "s1 [Monster] $v%";
			}

			if ($params['descfunc'] == 24) {
				$this->str = "to Skill Charges Or Something +$v ";
			}

			if ($params['descfunc'] == 25) {
				$this->str = "";
			}

			if ($params['descfunc'] == 26) {
				$this->str = "";
			}

			if ($params['descfunc'] == 27) {
				$sql = "SELECT skill,charclass FROM `skills` WHERE `Id`='$par'";
				$res = PDO_FetchRow($sql);
				$this->str = "+$min to {$res['skill']} ({$this->charClass[$res['charclass']]} Only)";
			}

			if ($params['descfunc'] == 28) {
				$this->str = "+$v to [skill] ";
			}
		}



		return $this->str;
	}

}
