<?php

require_once 'D2BitReader.php';

/**
 * Class D2CharItem
 */
class D2CharItem {
    /**
     * @var string The raw binary data of the item
     */
    private string $bits;
    
    // Item properties
    
    /**
     * @var string The name of the base item
     */
    public string $basename = '';
    
    /**
     * @var string The name string of the item
     */
    public string $item_name = '';
    
    /**
     * @var string The rank of the item (normal/exceptional/elite)
     */
    public string $item_rank = '';
    
    /**
     * @var string The basic type of the item (armor/weapon/misc)
     */
    public string $item_type = '';
    
    // Flags
    
    /**
     * @var int The identification flag (0 or 1)
     */
    public int $identified = 0;
    
    /**
     * @var int The number of sockets the item has
     */
    public int $sockets = 0;
    
    /**
     * @var int The ear flag (0 or 1)
     */
    public int $ear = 0;
    
    /**
     * @var int The starter flag (0 or 1)
     */
    public int $starter = 0;
    
    /**
     * @var int The compact flag (0 or 1)
     */
    public int $compact = 0;
    
    /**
     * @var mixed The ethereal flag
     */
    public $ethereal;
    
    /**
     * @var mixed The personalized flag
     */
    public $personalized;
    
    /**
     * @var mixed The runeword flag
     */
    public $runeword;
    
    /**
     * @var mixed The version flag
     */
    public $version;
    
    /**
     * @var string The name of the runeword
     */
    public string $runeword_name = '';
    
    /**
     * @var string The title of the runes used in the runeword
     */
    public string $runes_title = '';
    
    // Placement
    
    /**
     * @var int The location of the item
     */
    public int $location = 0;
    
    /**
     * @var int The body location of the item
     */
    public int $body = 0;
    
    /**
     * @var int The column location of the item
     */
    public int $col = 0;
    
    /**
     * @var int The row location of the item
     */
    public int $row = 0;
    
    /**
     * @var int The container flag (0 or 1)
     */
    public int $container = 0;
    
    /**
     * @var mixed The parent flag
     */
    public $parent;
    
    /**
     * @var mixed The storage flag
     */
    public $storage;
    
    /**
     * @var mixed The body part flag
     */
    public $bodypart;
    
    /**
     * @var int The height of the inventory
     */
    public int $invH = 0;
    
    /**
     * @var int The width of the inventory
     */
    public int $invW = 0;
    
    /**
     * @var int The number of rows in the belt
     */
    public int $beltrows = 1;
    
    // Features
    
    /**
     * @var string The item code (3-letter code from txt)
     */
    public string $item_code = '';
    
    /**
     * @var int The number of filled sockets
     */
    public int $SocketsFilled = 0;
    
    /**
     * @var int The number of sockets the item can have
     */
    public int $SocketsNum = 0;
    
    /**
     * @var bool The socketable flag (gem, rune, jewel)
     */
    public bool $socketable = false;
    
    /**
     * @var string The fingerprint of the item
     */
    public string $fingerprint = '';
    
    /**
     * @var int The item level
     */
    public int $itemlvl = 0;
    
    /**
     * @var int The quality of the item
     */
    public int $quality = 0;
    
    /**
     * @var bool The charm flag (true or false)
     */
    public bool $isCharm = false;
    
    /**
     * @var bool The jewel flag (true or false)
     */
    public bool $isJewel = false;
    
    /**
     * @var string The rank of the magic item (normal/magic/rare/crafted/set/unique)
     */
    public string $magic_rank = 'Normal';
    
    /**
     * @var int The set item ID from txt
     */
    public int $set_id = 0;
    
    /**
     * @var string The set item
     */
    public string $set_item = '';
    
    /**
     * @var int The set name (if it is a set item)
     */
    public int $set_name = 0;
    
    /**
     * @var string The name of the person (ear's name)
     */
    public string $personname = '';
    
    /**
     * @var int The quest difficulty (-1 if not applicable)
     */
    public int $questdif = -1;
    
    /**
     * @var mixed The gold
     */
    public $gold;
    
    /**
     * @var string The global unique identifier (GUID) of the item
     */
    public string $GUID = '';
    
    /**
     * @var int The defense value of the item
     */
    public int $defense = 0;
    
    /**
     * @var int The minimum damage of the item
     */
    public int $mindam = 0;
    
    /**
     * @var int The maximum damage of the item
     */
    public int $maxdam = 0;
    
    /**
     * @var int The second minimum damage of the item
     */
    public int $mindam2 = 0;
    
    /**
     * @var int The second maximum damage of the item
     */
    public int $maxdam2 = 0;
    
    /**
     * @var int The minimum missile damage of the item
     */
    public int $mindammi = 0;
    
    /**
     * @var int The maximum missile damage of the item
     */
    public int $maxdammi = 0;
    
    /**
     * @var int The maximum durability of the item
     */
    public int $MaxDur = 0;
    
    /**
     * @var int The current durability of the item
     */
    public int $CurDur = 0;
    
    /**
     * @var int The required level to use the item
     */
    public int $reqlvl = 0;
    
    /**
     * @var int The required strength to use the item
     */
    public int $reqstr = 0;
    
    /**
     * @var int The required dexterity to use the item
     */
    public int $reqdex = 0;
    
    /**
     * @var int The speed of the item
     */
    public int $speed = 0;
    
    /**
     * @var int The throwing value of the item
     */
    public int $throwing = 0;
    
    /**
     * @var int The stackable value of the item
     */
    public int $stackable = 0;
    
    /**
     * @var string The character name (ear's name)
     */
    public string $charName = '';
    
    /**
     * @var mixed The graphic file name
     */
    public $gfx;
    
    /**
     * @var int The transform indexes for color remap base item (-1 if not applicable)
     */
    public int $baseTrans = -1;
    
    /**
     * @var int The transform indexes for color remap magic item (-1 if not applicable)
     */
    public int $magicTrans = -1;
    
    /**
     * @var mixed The type column from txt
     */
    public $type;
    
    /**
     * @var string The spell description for potions
     */
    public string $spelldesc = '';
    
    /**
     * @var mixed The link to item properties from txt
     */
    public $ditem;
    
    /**
     * @var int The damage multiplier of the item
     */
    public int $dammult = 100;
    
    /**
     * @var int The minimum damage addition of the item
     */
    public int $damminadd = 0;
    
    /**
     * @var int The maximum damage addition of the item
     */
    public int $dammaxadd = 0;
    
    /**
     * @var int The defense multiplier of the item
     */
    public int $defmult = 100;
    
    /**
     * @var int The defense addition of the item
     */
    public int $defadd = 0;
    
    /**
     * @var int[] The resistances of the item (phy, mag, fire, light, cold, poison)
     */
    public array $resist = [0, 0, 0, 0, 0, 0];
    
    /**
     * @var int[] The attributes of the item (str, dex, vit, ene)
     */
    public array $attributes = [0, 0, 0, 0];
    
    /**
     * @var array The socketed items collected in the above function because the item has only data for itself and gems/runes/jewels are standalone
     */
    public array $SocketItems = [];
    
    /**
     * @var array The item variable properties
     */
    public array $properties = [];
    
    /**
     * @var array The properties IDs list
     */
    public array $propids = [];

    /**
     * D2CharItem constructor.
     *
     * @param string $bits The item bits
     */
    public function __construct(string $bits)
    {
        if ($bits === '') {
            return false;
        }

        $this->bits = $bits;
        return $this->parseItem();
    }

    /**
     * Parses the item.
     *
     * @return void
     */
    public function parseItem()
    {
        // Implementation goes here
    }
}
