<?php

error_reporting(E_ERROR | E_PARSE);
set_time_limit(-1);
ini_set('max_input_time', '-1');
ini_set('max_execution_time', '0');
session_start();
ob_start();

/**
 *
 */
define('DB_FILE', $_SESSION['modname'] . ".db");
PDO_Connect("sqlite:" . DB_FILE);

/**
 *
 */
class D2CharStructureData {

    /**
     * @var
     */
    public $skills;

    /**
     * @var string[]
     */
    public $class = [
        0 => 'Amazon',
        1 => 'Sorceress',
        2 => 'Necromancer',
        3 => 'Paladin',
        4 => 'Barbarian',
        5 => 'Druid',
        6 => 'Assassin'
    ];

    /**
     * @var string[]
     */
    public $characterStatus = [
        0 => '',
        1 => '',
        2 => 'Hardcore',
        3 => 'Died',
        4 => '',
        5 => 'Expansion',
        6 => 'Ladder',
        7 => ''
    ];

    /**
     * @var string[]
     */
    public $characterProgressionClassicHC = [
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => 'Count / Countess',
        5 => 'Count / Countess',
        6 => 'Count / Countess',
        7 => 'Count / Countess',
        8 => 'Duke / Duchess',
        9 => 'Duke / Duchess',
        10 => 'Duke / Duchess',
        11 => 'Duke / Duchess',
        12 => 'Duke / Duchess',
        13 => '',
        14 => '',
        15 => '',
    ];

    /**
     * @var string[]
     */
    public $characterProgressionClassic = [
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => 'Sir / Dame',
        5 => 'Sir / Dame',
        6 => 'Sir / Dame',
        7 => 'Sir / Dame',
        8 => 'Lord / Lady',
        9 => 'Lord / Lady',
        10 => 'Lord / Lady',
        11 => 'Lord / Lady',
        12 => 'Baron / Baroness',
        13 => '',
        14 => '',
        15 => '',
    ];

    /**
     * @var string[]
     */
    public $characterProgressionExp = [
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => 'Slayer',
        6 => 'Slayer',
        7 => 'Slayer',
        8 => 'Slayer',
        9 => '',
        10 => 'Champion',
        11 => 'Champion',
        12 => 'Champion',
        13 => 'Champion',
        14 => 'Patriarch / Matriarch	',
        15 => 'Patriarch / Matriarch	',
    ];

    /**
     * @var string[]
     */
    public $characterProgressionExpHC = [
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => 'Destroyer',
        6 => 'Destroyer',
        7 => 'Destroyer',
        8 => 'Destroyer',
        9 => '',
        10 => 'Conqueror',
        11 => 'Conqueror',
        12 => 'Conqueror',
        13 => 'Conqueror',
        14 => 'Guardian',
        15 => 'Guardian',
    ];

    /**
     * @var int[]
     */
    public $offsets = [
        0 => 4, // Identifier
        4 => 4, // Version ID
        8 => 4, // File size
        12 => 4, // Checksum
        16 => 4, // Active weapon
        20 => 16, // Character Name
        36 => 1, // Character Status
        37 => 1, // Character progression
        38 => 2, // Unknown
        40 => 1, // Character Class
        41 => 2, // Unknown
        43 => 1, // Character Level
        44 => 4, // Unknown
        48 => 4, // Last played
        52 => 4, // Unknown
        56 => 64, // Assigned skills
        120 => 4, // Left mouse button skill ID
        124 => 4, // Right mouse button skill ID
        128 => 4, // Left swap mouse button skill ID
        132 => 4, // Right swap mouse button skill ID
        136 => 32, // Character menu appearance
        168 => 3, // Difficulty
        171 => 4, // Map ID
        175 => 2, // Unknown
        177 => 2, // Mercenary dead
        179 => 4, // Mercenary ID
        183 => 2, // Mercenary Name ID
        185 => 2, // Mercenary type
        187 => 4, // Mercenary experience
        191 => 144, // Unknown
        335 => 298, // Quests
        633 => 81, // Waypoints
        714 => 51, // NPC Introductions
    ];

    /**
     * @var string[]
     */
    public $qNorm = [
        //345 => 'introWarriv',
        347 => 'Act 1 - Den_Of_Evil',
        349 => 'Act 1 - Sisters_Burial_Grounds',
        351 => 'Act 1 - Tools_Of_The_Trade',
        353 => 'Act 1 - The_Search_For_Cain',
        355 => 'Act 1 - The_Forgotten_Tower',
        357 => 'Act 1 - Sisters_To_The_Slaughter',
        //359 => 'traveledToAct2',
        //361 => 'introJerhyn',
        363 => 'Act 2 - Radaments_Lair',
        365 => 'Act 2 - TheHoradric_Staff',
        367 => 'Act 2 - Tainted_Sun',
        369 => 'Act 2 - Arcane_Sanctuary',
        371 => 'Act 2 - The_Summoner',
        373 => 'Act 2 - The_Seven_Tombs',
        //375 => 'traveledToAct3',
        //377 => 'introHratli',
        379 => 'Act 3 - LamEsens_Tome',
        381 => 'Act 3 - Khalims_Will',
        383 => 'Act 3 - Blade_Of_The_Old_Religion',
        385 => 'Act 3 - The_Golden_Bird',
        387 => 'Act 3 - The_Blackened_Temple',
        389 => 'Act 3 - The_Guardian',
        //391 => 'traveledtoAct4',
        //393 => 'introToAct4',
        395 => 'Act 4 - The_Fallen_Angel',
        397 => 'Act 4 - Terrors_End',
        399 => 'Act 4 - Hell_Forge',
        //401 => 'traveledToAct5',
        //403 => 'empty31',
        //405 => 'empty32',
        //407 => 'empty33',
        //409 => 'completedTerrorsEnd',
        //411 => 'empty21',
        //413 => 'empty22',
        415 => 'Act 5 - Siege_On_Harrogath',
        417 => 'Act 5 - Rescue_On_MountArreat',
        419 => 'Act 5 - Prison_Of_Ice',
        421 => 'Act 5 - Betrayal_Of_Harrogath',
        423 => 'Act 5 - Rite_Of_Passage',
        425 => 'Act 5 - Eve_Of_Destruction',
        // read 425, pointer at 427, + 14 = 441 qNM offset
    ];

    /**
     * @var string[]
     */
    public $qNM = [
        //441 => 'introWarrivNM',
        443 => 'Act 1 - Den_Of_Evil_NM',
        445 => 'Act 1 - Sisters_Burial_Grounds_NM',
        447 => 'Act 1 - Tools_Of_The_Trade_NM',
        449 => 'Act 1 - The_Search_For_Cain_NM',
        451 => 'Act 1 - The_Forgotten_Tower_NM',
        453 => 'Act 1 - Sisters_To_The_Slaughter_NM',
        //455 => 'traveledToAct2',
        //457 => 'introJerhyn',
        459 => 'Act 2 - Radaments_Lair_NM',
        461 => 'Act 2 - The_Horadric_Staff_NM',
        463 => 'Act 2 - Tainted_Sun_NM',
        465 => 'Act 2 - Arcane_Sanctuary_NM',
        467 => 'Act 2 - The_Summoner_NM',
        469 => 'Act 2 - The_SevenTombs_NM',
        //471 => 'traveledToAct3',
        //473 => 'introHratli',
        475 => 'Act 3 - Lam_Esens_Tome_NM',
        477 => 'Act 3 - Khalims_Will_NM',
        479 => 'Act 3 - Blade_Of_The_OldReligion_NM',
        481 => 'Act 3 - The_Golden_Bird_NM',
        483 => 'Act 3 - The_Blackened_Temple_NM',
        485 => 'Act 3 - The_Guardian_NM',
        //487 => 'traveledtoAct4',
        //489 => 'introToAct4',
        491 => 'Act 4 - The_Fallen_Angel_NM',
        493 => 'Act 4 - Terrors_End_NM',
        495 => 'Act 4 - Hell_Forge_NM',
        //497 => 'traveledToAct5',
        //499 => 'empty31',
        //501 => 'empty32',
        //503 => 'empty33',
        //505 => 'completedTerrorsEnd',
        //507 => 'empty21',
        //509 => 'empty22',
        511 => 'Act 5 - Siege_On_Harrogath',
        513 => 'Act 5 - Rescue_On_MountArreat',
        515 => 'Act 5 - Prison_Of_Ice',
        517 => 'Act 5 - Betrayal_Of_Harrogath',
        519 => 'Act 5 - Rite_Of_Passage',
        521 => 'Act 5 - Eve_Of_Destruction',
        // read 521, pointer at 523, + 14 = 537 qHell offset
    ];

    /**
     * @var string[]
     */
    public $qHell = [
        //537 => 'introWarriv',
        539 => 'Act 1 - Den_Of_Evil_Hell',
        541 => 'Act 1 - Sisters_Burial_Grounds_Hell',
        543 => 'Act 1 - Act 5 - Tools_Of_The_Trade_Hell',
        545 => 'Act 1 - The_Search_For_Cain_Hell',
        547 => 'Act 1 - The_Forgotten_Tower_Hell',
        549 => 'Act 1 - Sisters_To_The_Slaughter_Hell',
        //551 => 'traveledToAct2',
        //553 => 'introJerhyn',
        555 => 'Act 2 - Radaments_Lair_Hell',
        557 => 'Act 2 - The_Horadric_Staff_Hell',
        559 => 'Act 2 - Tainted_Sun_Hell',
        561 => 'Act 2 - Arcane_Sanctuary_Hell',
        563 => 'Act 2 - The_Summoner_Hell',
        565 => 'Act 2 - The_SevenTombs_Hell',
        //567 => 'traveledToAct3',
        //569 => 'introHratli',
        571 => 'Act 3 - Lam_Esens_Tome_Hell',
        573 => 'Act 3 - KhalimsWill_Hell',
        575 => 'Act 3 - Blade_Of_The_Old_Religion_Hell',
        577 => 'Act 3 - The_Golden_Bird_Hell',
        579 => 'Act 3 - The_Blackened_Temple_Hell',
        581 => 'Act 3 - The_Guardian_Hell',
        //583 => 'traveledtoAct4',
        //585 => 'introToAct4',
        587 => 'Act 4 - The_Fallen_Angel_Hell',
        589 => 'Act 4 - Terrors_End_Hell',
        591 => 'Act 4 - Hell_Forge_Hell',
        //593 => 'traveledToAct5',
        //595 => 'empty31',
        //597 => 'empty32',
        //599 => 'empty33',
        //601 => 'completedTerrorsEnd',
        //603 => 'empty21',
        //605 => 'empty22',
        607 => 'Act 5 - Siege_On_Harrogath',
        609 => 'Act 5 - Rescue_On_MountArreat',
        611 => 'Act 5 - Prison_Of_Ice',
        613 => 'Act 5 - Betrayal_Of_Harrogath',
        615 => 'Act 5 - Rite_Of_Passage',
        617 => 'Act 5 - Eve_Of_Destruction',
    ];

    /**
     * @var string[]
     */
    public $version = [
        71 => "1.00 through v1.06",
        87 => "1.07 or Expansion Set v1.08",
        89 => "standard game v1.08",
        92 => "v1.09 (both the standard game and the Expansion Set.)",
        96 => "v1.10+"
    ];

    /**
     * @var int
     */
    public $wpOffsetsNorm = 643;

    /**
     * @var int
     */
    public $wpOffsetsNM = 667;

    /**
     * @var int
     */
    public $wpOffsetsHell = 691;

    /**
     * @var string[]
     * These are bit positions
     */
    public $wpNames = [
        0 => 'Act 1 - Rogue_Encampment',
        1 => 'Act 1 - Cold_Plains',
        2 => 'Act 1 - Stony_Field',
        3 => 'Act 1 - Dark_Wood',
        4 => 'Act 1 - Black_Marsh',
        5 => 'Act 1 - Outer_Cloister',
        6 => 'Act 1 - Jail_level_1',
        7 => 'Act 1 - Inner_Cloister',
        8 => 'Act 1 - Catacombs_level_2',
        9 => 'Act 2 - Lut_Gholein',
        10 => 'Act 2 - Sewers_level_2',
        11 => 'Act 2 - Dry_Hills',
        12 => 'Act 2 - Halls_of_the_Dead_level_2',
        13 => 'Act 2 - Far_Oasis',
        14 => 'Act 2 - Lost_City',
        15 => 'Act 2 - Palace_Cellar_level_1',
        16 => 'Act 2 - Arcane_Sanctuary',
        17 => 'Act 2 - Canyon_of_the_Magi',
        18 => 'Act 3 - Kurast_Docks',
        19 => 'Act 3 - Spider_Forest',
        20 => 'Act 3 - Great_Marsh',
        21 => 'Act 3 - Flayer_Jungle',
        22 => 'Act 3 - Lower_Kurast',
        23 => 'Act 3 - Kurast_Bazaar',
        24 => 'Act 3 - Upper_Kurast',
        25 => 'Act 3 - Travincal',
        26 => 'Act 3 - Durance_of_Hate_level_2',
        27 => 'Act 4 - Pandemonium_Fortress',
        28 => 'Act 4 - City_of_the_Damned',
        29 => 'Act 4 - River_of_Flames',
        30 => 'Act 5 - Harrogath',
        31 => 'Act 5 - Frigid_Highlands',
        32 => 'Act 5 - Arreat_Plateau',
        33 => 'Act 5 - Crystalline_Passage',
        34 => 'Act 5 - Halls_of_Pain',
        35 => 'Act 5 - Glacial_Trail',
        36 => 'Act 5 - Frozen_Tundra',
        37 => "Act 5 - The_Ancients_Way",
        38 => 'Act 5 - Worldstone_Keep_level_2'
    ];
    public $wpNames_flipped = array(
        'Act_1_-_Rogue_Encampment' => 0,
        'Act_1_-_Cold_Plains' => 1,
        'Act_1_-_Stony_Field' => 2,
        'Act_1_-_Dark_Wood' => 3,
        'Act_1_-_Black_Marsh' => 4,
        'Act_1_-_Outer_Cloister' => 5,
        'Act_1_-_Jail_level_1' => 6,
        'Act_1_-_Inner_Cloister' => 7,
        'Act_1_-_Catacombs_level_2' => 8,
        'Act_2_-_Lut_Gholein' => 9,
        'Act_2_-_Sewers_level_2' => 10,
        'Act_2_-_Dry_Hills' => 11,
        'Act_2_-_Halls_of_the_Dead_level_2' => 12,
        'Act_2_-_Far_Oasis' => 13,
        'Act_2_-_Lost_City' => 14,
        'Act_2_-_Palace_Cellar_level_1' => 15,
        'Act_2_-_Arcane_Sanctuary' => 16,
        'Act_2_-_Canyon_of_the_Magi' => 17,
        'Act_3_-_Kurast_Docks' => 18,
        'Act_3_-_Spider_Forest' => 19,
        'Act_3_-_Great_Marsh' => 20,
        'Act_3_-_Flayer_Jungle' => 21,
        'Act_3_-_Lower_Kurast' => 22,
        'Act_3_-_Kurast_Bazaar' => 23,
        'Act_3_-_Upper_Kurast' => 24,
        'Act_3_-_Travincal' => 25,
        'Act_3_-_Durance_of_Hate_level_2' => 26,
        'Act_4_-_Pandemonium_Fortress' => 27,
        'Act_4_-_City_of_the_Damned' => 28,
        'Act_4_-_River_of_Flames' => 29,
        'Act_5_-_Harrogath' => 30,
        'Act_5_-_Frigid_Highlands' => 31,
        'Act_5_-_Arreat_Plateau' => 32,
        'Act_5_-_Crystalline_Passage' => 33,
        'Act_5_-_Halls_of_Pain' => 34,
        'Act_5_-_Glacial_Trail' => 35,
        'Act_5_-_Frozen_Tundra' => 36,
        'Act_5_-_The_Ancients_Way' => 37,
        'Act_5_-_Worldstone_Keep_level_2' => 38,
    );

    /**
     * @var int[]|string[]
     */
    public $_qNorm;

    /**
     * @var int[]|string[]
     */
    public $_qNM;

    /**
     * @var int[]|string[]
     */
    public $_qHell;

    /**
     *
     */
    public function __construct() {
        $sql = "
        SELECT
            skills.Id,
            skills.skilldesc,
            skilldesc.skilldesc,
            skilldesc.`str name`,
            `strings`.`Key`,
            strings.`String`
        FROM skills, skilldesc, strings
        WHERE skills.skilldesc = skilldesc.skilldesc
            AND skilldesc.`str name` = strings.Key
        ";
        $res = PDO_FetchAll($sql);

        foreach ($res as $r) {
            $this->skills[$r['Id']] = $r['String'];
        }


        $this->_qNorm = array_flip($this->qNorm);
        $this->_qNM = array_flip($this->qNM);
        $this->_qHell = array_flip($this->qHell);
    }

}
