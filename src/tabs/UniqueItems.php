<div style="height: 40px; margin: 40px 10px;"><h2>Unique Item Maker</h2></div>
<div class="row">
	<div class="col sortby">Sort By:<br>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="sort" id="sort_0" type="radio" class="custom-control-input" value="lvl"> 
			<label for="sort_0" class="custom-control-label">Level</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="sort" id="sort_1" type="radio" class="custom-control-input" value="index"> 
			<label for="sort_1" class="custom-control-label">Name</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="sort" id="sort_2" type="radio" class="custom-control-input" value="*type"> 
			<label for="sort_2" class="custom-control-label">Type</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="sort" id="sort_3" type="radio" class="custom-control-input" value="code"> 
			<label for="sort_3" class="custom-control-label">Item Code</label>
		</div>
		<hr>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="view" id="view_0" type="radio" class="custom-control-input" value="rin"> 
			<label for="view_0" class="custom-control-label">Rings</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="view" id="view_1" type="radio" class="custom-control-input" value="amu"> 
			<label for="view_1" class="custom-control-label">Amulets</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="view" id="view_2" type="radio" class="custom-control-input" value="weap"> 
			<label for="view_2" class="custom-control-label">Weapons</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="view" id="view_3" type="radio" class="custom-control-input" value="armo"> 
			<label for="view_3" class="custom-control-label">Armors</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="view" id="view_4" type="radio" class="custom-control-input" value="jew"> 
			<label for="view_4" class="custom-control-label">Jewels</label>
		</div>
		<div class="custom-control custom-radio custom-control-inline">
			<input name="view" id="view_5" type="radio" class="custom-control-input" value="char"> 
			<label for="view_5" class="custom-control-label">Charms</label>
		</div>		
	</div>
	<div class="col">
		<div>
			<div class="center item_desc" style="">

				<div style="height: 116px;background: url(/img/bg.png) center top no-repeat;">
					<a href="" target="_blank" class="item_debug_link">
						<img style="" class="item img-fluid" src="">
					</a>				

				</div>
				<div class="item_stats" style="">
					<div class="">
						<ul>
							<li class="itemindex statindex"></li>
							<li class="itemtype stattype"></li>
							<li class="itemlvl statlvlreq"></li>
							<li class="itemlvlreq statlvlreq"></li>
							<li class="itemcode code" style="color:#D49E43;"></li>
							<li class="itemstreq statlvlreq"></li>
							<li class="itemdexreq statlvlreq"></li>
							<li class="itemgemsockets statlvlreq"></li>
							<li class="blue Prop1"></li>
							<li class="blue Prop2"></li>
							<li class="blue Prop3"></li>
							<li class="blue Prop4"></li>
							<li class="blue Prop5"></li>
							<li class="blue Prop6"></li>
							<li class="blue Prop7"></li>
							<li class="blue Prop8"></li>
							<li class="blue Prop9"></li>
							<li class="blue Prop10"></li>
							<li class="blue Prop11"></li>
							<li class="blue Prop12"></li>							
						</ul>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>
<div class="row">
	<div class="col-4">
		<button style="margin: 20px;" class="btn btn-danger btnDebug">Debug Info</button>
<!--		<button style="margin: 20px;" class="btn btn-info btnDocs">Generate Docs Info</button>-->
		<pre class="debug_preview" style="display:none; background: #eee; height: 420px;width: 1123px;"></pre>
	</div>	
</div>	
<div class="row" style="margin-bottom:40px;">
	<div class="col" style="">
		<p>Search for Item</p>
		<input style="border: 1px solid #999; padding: 10px;width: 420px;" id="search" type="text custom-control-inline" placeholder="Search item" name="search">
	</div>
</div>

<div class="row" style="">
	<div class="col-5">
		<select style="height: 160px;padding:10px;margin-bottom: 20px;" name="uniqueitems" class="custom-select uniqueitems-select" multiple>
			<!--<option value=""></option>-->
			<?php foreach ($uniqueitems as $u) { ?>
				<option value="<?php echo $u['index'] ?>"><?php echo $u['index'] ?></option>
			<?php } ?>
		</select>	
	</div>		
</div>
<form class="uniqueform" action="" method="post">
	<div class="form-group row">

		<div class="col-2" style="background: #fec;">
			<p>Index</p>
			<div class="input-group">
				<input  name="index"  type="text" aria-describedby="indexHelpBlock" required="required" class="form-control input-uindex">
			</div><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Index: the ID pointer that is referenced by the game in TreasureClassEx.txt and CubeMain.txt, this column also contains the string-key used in the TBL files.</span>
		</div>
        
		<div class="col-2" style="background: #eed;">
			<p>Version:</p>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="version" id="version_0" type="radio" aria-describedby="versionHelpBlock" required="required" class="custom-control-input" value="0">
				<label for="version_0" class="custom-control-label">0</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="version" id="version_1" type="radio" aria-describedby="versionHelpBlock" required="required" class="custom-control-input" value="1">
				<label for="version_1" class="custom-control-label">1</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="version" id="version_2" type="radio" aria-describedby="versionHelpBlock" required="required" class="custom-control-input" value="100" checked>
				<label for="version_2" class="custom-control-label">100</label>
			</div><br><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Version: Switch, what game version was this unique item added in, 0 referes to real classic Diablo II (1.00-1.06), 1 refers to new classic Diablo II (1.07-1.11) and 100 refers to the Expansion Set. Items with 100 will be unable to drop in Classic Diablo II.</span>
		</div>


		<div class="col-2" style="background: #def;">
			<p>Enabled:</p>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="enabled" id="enabled_0"  type="radio" class="custom-control-input" value="0" aria-describedby="enabledHelpBlock" required="required">
				<label for="enabled_0" class="custom-control-label">0</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="enabled" id="enabled_1"  type="radio" class="custom-control-input" value="1" aria-describedby="enabledHelpBlock" required="required" checked>
				<label for="enabled_1" class="custom-control-label">1</label>
			</div><br><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Ladder: Boolean, 1 = item available only on the realms (enabled), 0 = item available both in single player/open games, TCP/IP and on the realms.</span>
		</div>


		<div class="col-2" style="background: #dac;">

			<p>Ladder:</p>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="ladder" id="ladder_0" type="radio" class="custom-control-input" value="0" aria-describedby="ladderHelpBlock" required="required" checked>
				<label for="ladder_0" class="custom-control-label">0</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="ladder" id="ladder_1" type="radio" class="custom-control-input" value="1" aria-describedby="ladderHelpBlock" required="required">
				<label for="ladder_1" class="custom-control-label">1</label>
			</div><br><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Ladder: Boolean, 1 = item available only on the realms (ladder), 0 = item available both in single player/open games, TCP/IP and on the realms.</span>
		</div>


		<div class="col-2" style="background: #dec;">
			<p>Rarity</p>
			<div class="input-group">
				<input  name="rarity"  type="text" aria-describedby="rarityHelpBlock" class="form-control">
			</div><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Rarity: chance to pick this unique item if more then one unique item of the same base item exist, this uses the common rarity/total_rarity formula, so if you have two unique rings, one with a rarity of 100 the other with a rarity of 1, then the first will drop 100/101 percent of the time (99%) and the other will drop 1/101 percent of the time (1%), rarity can be anything between 1 and 255 (rarity of less then 1 will be set to 1 by the code).</span>
		</div>

		<div class="col-2" style="background: #fde;">
			<p>No Limit:</p>
			<select  name="nolimit" class="custom-select" aria-describedby="nolimitHelpBlock">
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
			</select><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">NoLimit: Boolean, 0 = can drop only once per game, 1 = can drop more then once per game.</span>

		</div>

		<div class="col-2" style="background: #edd;">
			<p>Lvl</p>
			<input  name="lvl"  type="text" aria-describedby="lvlHelpBlock" required="required" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Lvl: the quality level of this unique item. Monsters, cube recipes, vendors, objects and the like most be at least this level or higher to be able to drop this item, otherwise they would drop a rare item with enhanced durability.</span>
		</div>


		<div class="col-2" style="background: #edd;">
			<p>LvlReq</p>
			<input  name="lvl req"  type="text" aria-describedby="lvlreqHelpBlock" required="required" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Lvl Req: the character level required to use this unique item.</span>
		</div>


		<div class="col-2" style="background: #bbb;">
			<p>Armor</p>
			<select class="a-select custom-select code" name="code[]"  required="required">
				<option value=""></option>
				<?php
				foreach ($armor as $a)
					if ($a['spawnable']) {
						echo '<option value="' . $a['code'] . '">' . $a['name'] . '</option>';
					} else {
						echo '<option disabled value="' . $a['code'] . '">' . $a['name'] . '</option>';
					}
				?>
			</select>
		</div>

		<div class="col-2" style="background: #bbb;">
			<p>Misc</p>
			<select class="m-select custom-select code" name="code[]"  required="required">
				<option value=""></option>
				<?php
				foreach ($misc as $a)
					if ($a['spawnable']) {
						echo '<option value="' . $a['code'] . '">' . $a['name'] . '</option>';
					} else {
						echo '<option disabled value="' . $a['code'] . '">' . $a['name'] . '</option>';
					}
				?>
			</select>
		</div>

		<div class="col-2" style="background: #bbb;">
			<p>Weapon</p>
			<select class="w-select custom-select code" name="code[]"  required="required">
				<option value=""></option>
				<?php
				foreach ($weapon as $a) {
					echo '<option value="' . $a['code'] . '">' . $a['name'] . '</option>';
				}
				?>
			</select>
		</div>

		<input type="hidden" name="*type" value="">  <br>
		<input type="hidden" name="*uber" value=""><br>		
		<div class="col-2" style="background: #edf;">
			<p>Carry 1</p>
			<select  name="carry1" class="custom-select" aria-describedby="carry1HelpBlock">
				<option value="" selected></option>
				<option value="0">0</option>
				<option value="1">1</option>
			</select><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Carry1: Boolean, 0 = allow the player to hold as many of this item as he wants, 1 = allow the player to hold a single copy only. In reality this just prevents the player from picking up the item when it is dropped on the floor and it prevents the player from putting this item in the trading window.</span>

		</div>


		<div class="col-2" style="background: #def;">
			<p>CostMult</p>
			<input  value="" name="cost mult"  type="text" aria-describedby="costmultHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Cost Mult: the base item's price is multiplied by this value when sold, repaired or bought from a vendor.</span>
		</div>


		<div class="col-2" style="background: #def;">
			<p>CostAdd</p>
			<input  value="" name="cost add"  type="text" aria-describedby="costaddHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Cost Add: after the price has been multiplied, this amount of gold is added to the price on top.</span>
		</div>


		<div class="col-2" style="background: #fde;">
			<p>CharTransform</p>
			<input  name="chrtransform"  type="text" aria-describedby="chrtransformHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">ChrTransform: palette shift to apply to the the DCC component-file and the DC6 flippy-file (whenever or not the color shift will apply is determined by Weapons.txt, Armor.txt or Misc.txt). This is an ID pointer from Colors.txt.</span>
		</div>


		<div class="col-2">
			<p>InvTransform</p>
			<input  name="invtransform" type="text"  aria-describedby="invtransformHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">InvTransform: palette shift to apply to the the DC6 inventory-file (whenever or not the color shift will apply is determined by Weapons.txt, Armor.txt or Misc.txt). This is an ID pointer from Colors.txt.</span>
		</div>


		<div class="col-2" style="background: #eef;">
			<p>Flippy File</p>
			<input  name="flippyfile"  type="text" aria-describedby="flippyfileHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">FlippyFile: overrides the flippyfile specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains the file name of the DC6 flippy animation.</span>
		</div>


		<div class="col-2" style="background: #eef;">
			<p>Inv File</p>
			<input  name="invfile"  type="text" aria-describedby="invfileHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">InvFile: overrides the invfile and uniqueinvfile specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains the file name of the DC6 inventory graphic.</span>
		</div>


		<div class="col-2" style="background: #fed;">
			<p>DropSound</p>
			<input  name="dropsound"  type="text" aria-describedby="dropsoundHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">DropSound: overrides the dropsound (the sound played when the item hits the ground) specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains an ID pointer from Sounds.txt.</span>
		</div>


		<div class="col-2" style="background: #fed;">
			<p>DropSFXFrame</p>
			<input  name="dropsfxframe"  type="text" aria-describedby="dropsfxframeHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">DropSfxFrame: how many frames after the flippy animation starts playing will the associated drop sound start to play. This overrides the values in Weapons.txt, Armor.txt or Misc.txt.</span>
		</div>

		<div class="col-2" style="background: #fed;">
			<p>UseSound</p>
			<input  name="usesound"  type="text" aria-describedby="usesoundHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span><span  class="form-text">UseSound: overrides the usesound (the sound played when the item is consumed by the player) specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains an ID pointer from Sounds.txt.</span>
		</div>
	</div>
	<div class="form-group row props props-container">
		<?php
		$html = '';

		foreach (range(01, 12) as $p) {
			?>
			<div class="col-2" style="background: #ddd;">
				<p>Prop <?php echo $p ?>:</p>
				<select class="custom-select prop<?php echo $p ?>-select" name="prop<?php echo $p ?>">
					<option class="PropFirst" value=""></option>
					<?php
					foreach ($prop as $a) {
						echo '<option value="' . $a . '">' . $a . '</option>';
					}
					?>
				</select>

				<input name="par<?php echo $p ?>" placeholder="Par<?php echo $p ?>" type="number" aria-describedby="par<?php echo $p ?>HelpBlock" class="form-control par<?php echo $p ?>">

				<input name="min<?php echo $p ?>" placeholder="min<?php echo $p ?>" type="number" aria-describedby="min<?php echo $p ?>HelpBlock" class="form-control min<?php echo $p ?>">

				<input name="max<?php echo $p ?>" placeholder="max<?php echo $p ?>" type="number" aria-describedby="max<?php echo $p ?>HelpBlock" class="form-control max<?php echo $p ?>">

			</div>
			<?php
		}
		?>
	</div>
		<input type="hidden" name="*eol" value="0">
		<!-- distinguish between forms -->
		<input type="hidden" name="formtype" value="uniqueitems">	
	<?php require_once 'optionSubmit.php'; ?>
	<input  type="hidden" name="item" value="">
</form>

<ul>
    <li><a target="_blank" href="https://d2mods.info/forum/viewtopic.php?t=34455">Intro to the D2 Files and File Guide/Tutorial Masterlist</a></li>
	<li><a target="_blank"  href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=2">Armor.txt by Kingpin et al. [ex Nefarius] (accurate)</a></li>
	<li><a  target="_blank" href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=345">Properties.txt by Joel (revised by Myhrginoc 8/22/06)</a></li>
	<li><a target="_blank"  href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=346">Weapons.txt by Kingpin and Ric Faith (accurate)</a></li>
	<li><a target="_blank"  href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=386">UniqueItems.txt by Nefarius (accurate)</a></li>
</ul>