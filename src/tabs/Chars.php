<div style="text-align: center; margin-top: 20px;">
    <div class="container">
        <ul class="nav nav-tabs" id="CTabs" role="tablist">
            <?php
            foreach ($charData as $c) {
                $tabs = '';
                // ddump($c);

                $tabs .= <<<EOT
            <li class="nav-item" role="presentation">
                <a class="nav-link btn btn-outline-danger" style="background: #ddd; border: 1px solid #888;" id="{$c->cData['CharacterName']}-tab" data-toggle="tab" href="#{$c->cData['CharacterName']}" role="tab" aria-controls="{$c->cData['CharacterName']}" aria-selected="true"><img height="64" width="" src="img/chars/{$c->cData['CharacterClass']}.gif"><span class="chartab">{$c->cData['CharacterName']}</span></a>
            </li>
EOT;
                echo $tabs;
            }
            ?>
        </ul>
    </div>
</div>

<div class="tab-content" id="CTabContent">
    <?php
    session_start();
    ob_start();

    require_once './config.php';
    require_once './_pdo.php';

    require_once './src/D2Functions.php';
    require_once './src/D2ByteReader.php';
    require_once './src/D2BitReader.php';
    require_once './src/D2Item.php';
    require_once './src/D2Char.php';

    error_reporting(E_ERROR | E_PARSE);
    set_time_limit(-1);
    ini_set('max_input_time', '-1');
    ini_set('max_execution_time', '0');

    define('DB_FILE', $_SESSION['modname'] . ".db");
    PDO_Connect("sqlite:" . DB_FILE);

    foreach ($charData as $c) {

        $quests = '';
        $accordionCounter = 1;

        foreach ($c->cData['Quests'] as $quest) {
            foreach ($quest as $difficulty => $q) {
                $isExpanded = ($accordionCounter === 1) ? 'show' : '';
                $collapseId = 'collapseq-' . $accordionCounter;
                $headingId = 'headingq-' . $accordionCounter;

                $quests .= "<div class='card'>";
                $quests .= "<div class='card-header' id='{$headingId}'>";
                $quests .= "<h5 class='mb-0'>";
                $quests .= "<button class='btn btn-link' data-toggle='collapse' data-target='#{$collapseId}' aria-expanded='" . ($accordionCounter === 1 ? 'true' : 'false') . "' aria-controls='{$collapseId}'>{$difficulty}</button>";
                $quests .= "</h5>";
                $quests .= "</div>";

                $quests .= "<div id='{$collapseId}' class='collapse {$isExpanded}' aria-labelledby='{$headingId}' data-parent='#accordionExample'>";
                $quests .= "<div class='card-body'>";

                foreach ($q as $k => $v) {
                    $kD = str_replace("_", " ", $k);
                    $kD = str_replace([" NM", " Hell"], "", $kD);
                    $checked = ($v == 1) ? 'checked' : '';

                    $quests .= "<div class='qcheckgroup'>";
                    $quests .= "<input class='qcheck' diff='{$difficulty}' type='checkbox' value='1' name='{$k}' id='{$c->cData['CharacterName']}-{$k}' {$checked}>";
                    $quests .= "<label for='{$c->cData['CharacterName']}-{$k}'>{$kD}</label><br>";
                    $quests .= "</div>";
                }

                $quests .= "</div>";
                $quests .= "</div>";

                $quests .= "</div>";
                $accordionCounter++;
            }
        }

        $wps = '';
        //$wps .= "<input type='radio' value='1' name='wp_all' id='wp_all'>";
        //$wps .= "<label style='font-size: 1.3em;color:green;' for='wp_all'>Enable All Waypoints</label><br>";
        //$wps .= "<input type='radio' value='0' name='wp_all' id='wp_all_off'>";
        //$wps .= "<label style='font-size: 1.3em; color: red;' for='wp_all_off'>Disable All Waypoints</label><hr>";

        $wps = '';
        $accordionCounter = 1;

        foreach ($c->cData['Waypoints'] as $diff => $waypoints) {
            $isExpanded = ($accordionCounter === 1) ? 'show' : '';
            $collapseId = 'collapsewps-' . $accordionCounter;
            $headingId = 'headingwps-' . $accordionCounter;

            $wps .= "<div class='card'>";
            $wps .= "<div class='card-header' id='{$headingId}'>";
            $wps .= "<h5 class='mb-0'>";
            $wps .= "<button class='btn btn-link' data-toggle='collapse' data-target='#{$collapseId}' aria-expanded='" . ($accordionCounter === 1 ? 'true' : 'false') . "' aria-controls='{$collapseId}'>{$diff}</button>";
            $wps .= "</h5>";
            $wps .= "</div>";

            $wps .= "<div id='{$collapseId}' class='collapse {$isExpanded}' aria-labelledby='{$headingId}' data-parent='#accordionExample'>";
            $wps .= "<div class='card-body'>";

            array_pop($waypoints);
            $wp_count = 0;
            foreach ($waypoints as $k => $v) {
                $kD = str_replace("_", " ", $k);
                $kD = str_replace([" NM", " Hell"], "", $kD);
                $checked = ($v == 1 && $k != '') ? 'checked ' : '';

                $wps .= "<div class='wcheckgroup'>";
                $wps .= "<input diff='{$diff}' class='wpcheck' type='checkbox' value='1' name='{$wp_count}' id='{$c->cData['CharacterName']}-{$k}' {$checked}>";
                $wps .= "<label for='{$c->cData['CharacterName']}-{$k}'>{$kD}</label><br>";
                $wps .= "</div>";
                $wp_count++;
            }

            $wps .= "</div>";
            $wps .= "</div>";

            $wps .= "</div>";
            $accordionCounter++;
        }

        $classOptions = [
            'Amazon', 'Assassin', 'Barbarian', 'Druid', 'Paladin', 'Necromancer', 'Sorceress'
        ];
        $option = '';

        foreach ($classOptions as $class) {
            $selected = ($c->cData['CharacterClass'] == $class) ? 'selected' : '';
            $option .= "<option value='$class' $selected>$class</option>";
        }

        $difficulties = [
            'Normal' => '0',
            'Finished Normal' => '1',
            'Finished Nightmare' => '2',
            'Finished Hell' => '3'
        ];

        $radio = '';

        foreach ($difficulties as $difficulty => $id) {
            $checked = ($c->cData['Difficulty'][$difficulty] == 1) ? 'checked' : '';
            $radio .= "<input $checked type='radio' id='{$c->cData['CharacterName']}-$difficulty' name='Difficulty' value='$id' class='$difficulty'>";
            $radio .= "<label for='{$c->cData['CharacterName']}-$difficulty'>$difficulty</label><br>";
        }

        $skills = '';
        /*
          'skills' =>
          array (size=1)
          'skills' =>
          array (size=52)
          0 =>
          array (size=7)
          'id' => int 36
          'skill' => string 'Fire Bolt' (length=9)
          'points' => int 0
          'page' => int 3
          'row' => int 1
          'col' => int 2
          'icon' => int 0
         */
//        $skillcounter = 1;
//        foreach ($c->cData['skills']['skills'] as $k => $skill) {
//            $skills .= "<input cmd='skills' style='width: 64px;' class='skill-$k skill' name='$skillcounter' type='number' min='0' max='255' value='{$skill['points']}'>:  {$skill['skill']}<hr>";
//            $skillcounter++;
//        }

        $skillsByPage = array();
        foreach ($c->cData['skills']['skills'] as $skNum => $skill) {
            $page = $skill['page'];
            if (!isset($skillsByPage[$page])) {
                $skillsByPage[$page] = array();
            }
            $skillsByPage[$page][$skNum] = $skill;
        }

        $skillDivs = '';
        $accordionCounter = 1;
        foreach ($skillsByPage as $page => $skills) {
            $isExpanded = ($accordionCounter === 1) ? 'show' : '';
            $collapseId = 'collapseskills-' . $accordionCounter;
            $headingId = 'headingskills-' . $accordionCounter;

            $skillDivs .= '<div class="card">';
            $skillDivs .= '<div class="card-header" id="' . $headingId . '">';
            $skillDivs .= '<h5 class="mb-0">';
            $skillDivs .= '<button class="btn btn-link" data-toggle="collapse" data-target="#' . $collapseId . '" aria-expanded="' . ($accordionCounter === 1 ? 'true' : 'false') . '" aria-controls="' . $collapseId . '">Page ' . $page . '</button>';
            $skillDivs .= '</h5>';
            $skillDivs .= '</div>';

            $skillDivs .= '<div id="' . $collapseId . '" class="collapse ' . $isExpanded . '" aria-labelledby="' . $headingId . '" data-parent="#accordionExample">';
            $skillDivs .= '<div class="card-body">';
            foreach ($skills as $skillNumber => $skill) {
                $skillNumber++;
                $skillDivs .= "<input cmd='skills' style='width: 64px;' class='skill-{$skill['id']} skill' name='$skillNumber' type='number' min='0' max='255' value='{$skill['points']}'>:  {$skill['skill']}<hr>";
            }
            $skillDivs .= '</div>';
            $skillDivs .= '</div>';

            $skillDivs .= '</div>';
            $accordionCounter++;
        }


        $statuses = [
            'Died' => 'CharacterStatusDied',
            'Expansion' => 'CharacterStatusExpansion',
            'Hardcore' => 'CharacterStatusHardcore',
        ];

        $checkboxes = '';
        foreach ($statuses as $status => $dataKey) {
            $checked = $c->cData[$dataKey] ? 'checked' : '';
            $checkboxes .= <<<HTML
    <div class="col">
        <div class="form-check">
            <label class="form-check-label" for="{$c->cData['CharacterName']}-{$status}">{$status}: </label><br>
            <input class="form-check-input" {$checked} type="checkbox" id="{$c->cData['CharacterName']}-{$status}" name="CharacterStatus[{$status}]" value="{$c->cData[$dataKey]}">
        </div>
    </div>
HTML;
        }

        $sql = "SELECT ID,Stat,CSvBits FROM itemstatcost WHERE Saved=1";
        $ISCData = PDO_FetchAll($sql);

        foreach ($ISCData as $k) {
            $ISC[$k["Stat"]] = $k['CSvBits'];
        }

        // stats
        $cDataStats = $c->cData["stats"];
        $commonKeys = array_intersect_key($cDataStats, $ISC);

        foreach ($commonKeys as $key => $value) {
            $cDataStats[$key] = $ISC[$key];
        }

        $stats = '';

        foreach ($cDataStats as $stat => $bits) {
            // Calculate the maximum value for the number of bits
            $maxValue = pow(2, $bits) - 1;

            // Get the default value from $c->cData['stats']
            $defaultValue = isset($c->cData['stats'][$stat]) ? $c->cData['stats'][$stat] : 0;

            // Create an HTML input for the stat
            $stats .= '<div class="stats" id="statsdiv" class="statsdiv" style="border:1px solid #ccc;"><input cmd="stats" type="number" id="'. "{$c->cData['CharacterName']}-" . $stat . '" name="' . $stat . '" max="' . $maxValue . '" value="' . $defaultValue . '" class="' . $stat . '">';

            // Create an HTML label for the stat
            $stats .= '<label for="' . "{$c->cData['CharacterName']}-" . $stat . '">' . ucfirst($stat) . ':<br> Max Value - ' . $maxValue . '</label><br></div>';
        }





        // Create a new array to store objects sorted by container
        $sortedArray = [];
        // Iterate over the original array
        foreach ($c->cData['items'] as $item) {
            // Get the value of 'container' property
            $container = $item->iData['container'];

            // Check if the container key already exists in the sorted array
            if (!array_key_exists($container, $sortedArray)) {
                // If not, create a new empty array for the container
                $sortedArray[$container] = [];
            }

            // Add the current object to the corresponding container in the sorted array
            $sortedArray[$container][] = $item;
        }

// Sort the sub-arrays (objects with the same container) by the container property
        foreach ($sortedArray as &$containerArray) {
            usort($containerArray, function ($a, $b) {
                return strcmp($a->iData['container'], $b->iData['container']);
            });
        }



        $items = "<div class='accordion' id='accordionExample'>";

        foreach ($sortedArray as $index => $container) {
            $containerId = "container_" . $index;

            if (empty($index))
                $index = "Socketed";

            // Create accordion card for the container
            $items .= "<div class='card'>";
            $items .= "<div class='card-header' id='{$containerId}-heading'>";
            $items .= "<h5 class='mb-0'>";
            $items .= "<button class='btn btn-link' data-toggle='collapse' data-target='#{$containerId}-collapse' aria-expanded='" . ($index === 0 ? "true" : "false") . "' aria-controls='{$containerId}-collapse'>{$index}</button>";
            $items .= "</h5>";
            $items .= "</div>";

            // Create accordion content for the container
            $isExpanded = ($index === 0) ? "show" : "";
            $items .= "<div id='{$containerId}-collapse' class='collapse {$isExpanded}' aria-labelledby='{$containerId}-heading' data-parent='#accordionExample'>";
            $items .= "<div class='card-body'>";
            $items .= "<div class='row'>";

            foreach ($container as $item) {
                $i = array_filter($item->iData);

                $items .= "<div style='border: 1px solid #ccc;' class='col-md-3'>";
                $items .= "<p style='font-size: 120%;'>";
                $items .= "{$i['basename']}<br>";
                $items .= "<img src='/docs/{$_SESSION['modname']}/img/items/{$i['txt']['invfile']}.png'>";
                $items .= "</p>";
                $items .= "<ul>";
                $items .= "<li>Code: {$i['code']}</li>";
                $items .= "<li>Socketed: {$i['gems_in']}</li>";
                $items .= "<li>Item Level: {$i['ilvl']}</li>";
                $items .= "<li>Identified: {$i['identified']}</li>";
                $items .= "<li>Starting Item: {$i['startingItem']}</li>";
                $items .= "<li>Ethereal: {$i['ethereal']}</li>";
                $items .= "<li>Quality: {$i['iquality']}</li>";
                $items .= "<li>Container: {$i['container']}</li>";
                $items .= "<li>Runeword: {$i['runeword']}</li>";
                $items .= "</ul>";
                $items .= "</div>";
            }

            $items .= "</div>";
            $items .= "</div>";
            $items .= "</div>";

            $items .= "</div>";
        }

        $items .= "</div>";

        $qarray = ($c->cData['Quests'][0]);
        $qallOnes = 1;
        foreach ($qarray as $acts) {
            $values = array_values($acts);
            $result = array_reduce($values, function ($carry, $value) {
                return $carry && ($value === '1');
            }, 1);
            if (!$result) {
                $qallOnes = 0;
                break;
            }
        }
        $qchecked = $qallOnes ? "checked" : "";

        $wparray = ($c->cData['Waypoints']);
        $wpallOnes = 1;
        foreach ($wparray as $acts) {
            $values = array_values($acts);
            $result = array_reduce($values, function ($carry, $value) {
                return $carry && ($value === '1');
            }, 1);
            if (!$result) {
                $wpallOnes = 0;
                break;
            }
        }
        $wpchecked = $wpallOnes ? "checked" : "";

        $tabContent .= <<<EOT
<div style="background: white; margin-top: 10px;" class="tab-pane fade" id="{$c->cData['CharacterName']}" role="tabpanel" aria-labelledby="{$c->cData['CharacterName']}-tab">
    <form id='{$c->cData['CharacterName']}' class="charform {$c->cData['CharacterName']}" method="POST" action="/saveCharacter.php">
        <input type="hidden" name="filePath" id="filePath" value="{$c->cData['filePath']}">
        <div class="container" style="font-size: 14px;">
            <div class="row">
                <div class="col">
                    
                    Name: <input type="text" class="CharacterName" id="CharacterName" name="CharacterName" maxlength="16" value="{$c->cData['CharacterName']}" pattern="[A-Za-z0-9_-]+" title="Character name can only contain alphabets, numbers, underscore, and dash." data-oldvalue="{$c->cData['CharacterName']}">
<br>
                    <select id='CharacterClass' class='CharacterClass' name='CharacterClass'>
                        $option
                    </select><br>
                    Character Level: <input style="border: 1px solid black;width: 54px;" type="number" name="CharacterLevel" class="CharacterLevel" id="CharacterLevel" value="{$c->cData['CharacterLevel']}">
                    <hr>
                    <div style="background: #efe;">
                    Difficulty:<br>
                    $radio
                        </div>
                    <hr>

<!--Character Status -->
<div class="form-row">
$checkboxes
                        <br><br>
</div>

                 
                    <hr>
                    <div>
                        <h2>Stats</h2>
                        $stats
                     </div>
          
                <div style="background:;">
                        <h3 style="text-align: center">Skills</h3>
                        <div class="form-group">
                          <label for="{$c->cData['CharacterName']}-setallskills">Set all skills:</label>
                          <input name="setallskills" cmd="setallskills" type="number" id="{$c->cData['CharacterName']}-setallskills" class="form-control setallskills" min='0' max='99'>
                        </div>

                        <div class="accordion" id="accordionExample">
                             $skillDivs
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h3 style="text-align: center">Quests</h3>
<div class="form-group">
  <div class="form-check">
   <label for="{$c->cData['CharacterName']}-q_all" class="form-check-label">Enable/Disable All Quests</label>
   <input $qchecked type="checkbox" id="{$c->cData['CharacterName']}-q_all" name="q_all" value="$qallOnes" class="form-check-input q_all">
  </div>
</div>
                    $quests
                </div>
                <div class="col">
                    <h3 style="text-align: center">Waypoints</h3>
<div class="form-group">
  <div class="form-check">
   <label for="{$c->cData['CharacterName']}-wp_all" class="form-check-label">Enable/Disable All Waypoints</label>
   <input $wpchecked type="checkbox" id="{$c->cData['CharacterName']}-wp_all" name="wp_all" value="$wpallOnes" class="form-check-input wp_all">
  </div>
</div>

                    $wps
                </div>
            </div>


          <h2>Items on Character</h2>
          $items
        </div>
        <!--<input class="btn btn-danger" style="" type="submit" value="Save Character">-->
    </form>
</div>
EOT;

        echo $tabContent;
    }
    ?>

</div>
