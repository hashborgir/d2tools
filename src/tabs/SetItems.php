<div style="height: 40px; margin: 40px;"><h1>Set Item Maker</h1></div>
<form action="/index.php" method="post">

	<!-- First row -->
	<div class="form-group row">

		<div class="col-2" style="background: #efe;">
			<div class="input-group">				
				<input  name="index" placeholder="Index" type="text" aria-describedby="indexHelpBlock" required="required" class="form-control">			
			</div><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Index: string key to item's name in a .tbl file</span>

		</div>

		<div class="col-2" style="background: #cfc;">
			<div class="input-group">					
				<input  name="set" placeholder="Set" type="text" aria-describedby="setHelpBlock" required="required" class="form-control">
			</div><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">
				Set: string key to the index field in Sets.txt - the set the item is a part of.

			</span>

		</div>

		<div class="col-2" style="background: #ccd;">
			<select class="a-select custom-select" name="code[]" required="required">
				<option value="">Armor</option>
				<?php
				foreach ($armor as $a)
					if ($a['spawnable']) {
						echo '<option value="' . $a['code'] . '">' . $a['name'] . '</option>';
					} else {
						echo '<option disabled value="' . $a['code'] . '">' . $a['name'] . '</option>';
					}
				?>
			</select>
		</div>


		<div class="col-2" style="background: #ddc;">
			<select class="w-select custom-select" name="code[]" required="required">
				<option value="">Weapon</option>
				<?php
				foreach ($weapon as $a) {
					echo '<option value="' . $a['code'] . '">' . $a['name'] . '</option>';
				}
				?>
			</select>
		</div>		
		<input  type="hidden" name="item" value="">


		<div class="col-4" style="background: #dec;">
			<div class="input-group">
				<input  name="rarity" placeholder="Rarity" type="text" aria-describedby="rarityHelpBlock" required="required" class="form-control">
			</div><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Rarity: Chance to pick this set item if more then one set item of the same base item exist, this uses the common rarity/total_rarity formula, so if you have two set rings, one with a rarity of 100 the other with a rarity of 1, then the first will drop 100/101 percent of the time (99%) and the other will drop 1/101 percent of the time (1%), rarity can be anything between 0 and 255.</span>
		</div>


		<div class="col-2" style="background: #edd;">
			<input  name="lvl" placeholder="Lvl" type="text" aria-describedby="lvlHelpBlock" required="required" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">The quality level of this set item, monsters, cube recipes, vendors, objects and the like most be at least this level or higher to be able to drop this item, otherwise they would drop a magical item with twice normal durability.</span>
		</div>


		<div class="col-2" style="background: #edd;">
			<input  name="lvlreq" placeholder="Level Required" type="text" aria-describedby="lvlreqHelpBlock" required="required" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">lvl req: The character level required to use this set item.</span>
		</div>

		<div class="col-2">
			<input  name="chrtransform" placeholder="ChrTransform" type="text" aria-describedby="chrtransformHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">ChrTransform: palette shift to apply to the the DCC component-file and the DC6 flippy-file (whenever or not the color shift will apply is determined by Weapons.txt, Armor.txt or Misc.txt). This is an ID pointer from Colors.txt.</span>
		</div>

		<div class="col-2">
			<input  name="invtransform" type="text" placeholder="InvTransform" aria-describedby="invtransformHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">InvTransform: palette shift to apply to the the DC6 inventory-file (whenever or not the color shift will apply is determined by Weapons.txt, Armor.txt or Misc.txt). This is an ID pointer from Colors.txt.</span>
		</div>		

		<div class="col-2" style="background: #eef;">
			<input  name="invfile" placeholder="InvFile" type="text" aria-describedby="invfileHelpBlock" required="required" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">InvFile: overrides the invfile and uniqueinvfile specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains the file name of the DC6 inventory graphic.</span>
		</div>		

		<div class="col-2" style="background: #eef;">
			<input  name="flippyfile" placeholder="Flippy File" type="text" aria-describedby="flippyfileHelpBlock" class="form-control" required="required"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">FlippyFile: overrides the flippyfile specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains the file name of the DC6 flippy animation.</span>
		</div>		


		<div class="col-2" style="background: #fed;">
			<input  name="dropsound" placeholder="DropSound" type="text" aria-describedby="dropsoundHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">DropSound: overrides the dropsound (the sound played when the item hits the ground) specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains an ID pointer from Sounds.txt.</span>
		</div>


		<div class="col-2" style="background: #fed;">
			<input  name="dropsfxframe" placeholder="DropSfxFrame" type="text" aria-describedby="dropsfxframeHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">DropSfxFrame: how many frames after the flippy animation starts playing will the associated drop sound start to play. This overrides the values in Weapons.txt, Armor.txt or Misc.txt.</span>
		</div>		


		<div class="col-2" style="background: #fed;">
			<input  name="usesound" placeholder="UseSound" type="text" aria-describedby="usesoundHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span><span  class="form-text">UseSound: overrides the usesound (the sound played when the item is consumed by the player) specified in Weapons.txt, Armor.txt or Misc.txt for the base item. This field contains an ID pointer from Sounds.txt.</span>
		</div>		

		<div class="col-2" style="background: #def;">

			<input  value="" name="costmult" placeholder="Cost Mult" type="text" aria-describedby="costmultHelpBlock" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Cost Mult: the base item's price is multiplied by this value when sold, repaired or bought from a vendor.</span>
		</div>


		<div class="col-2" style="background: #def;">

			<input  value="" name="costadd" placeholder="Cost Add" type="text" aria-describedby="costaddHelpBlock" required="required" class="form-control"><span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">Cost Add: after the price has been multiplied, this amount of gold is added to the price on top.</span>
		</div>		



		<div class="col-2" style="background: #ffc">
			<div class="custom-control custom-radio custom-control-inline">
				<input name="addfunc" id="addfunc_0" type="radio" class="custom-control-input" value="0" aria-describedby="addfuncHelpBlock" checked="checked"> 
				<label for="addfunc_0" class="custom-control-label">0</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="addfunc" id="addfunc_1" type="radio" class="custom-control-input" value="1" aria-describedby="addfuncHelpBlock"> 
				<label for="addfunc_1" class="custom-control-label">1</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="addfunc" id="addfunc_2" type="radio" class="custom-control-input" value="2" aria-describedby="addfuncHelpBlock"> 
				<label for="addfunc_2" class="custom-control-label">2</label>
			</div> 			
			<br>
			<span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i></span>AddFunc
			<span  class="form-text">add func: a property mode field that controls how the variable attributes will appear and be functional on a set item. See the appendix for further details about this field's effects.</span>
		</div>		

		<div class="form-group row">	
			<div class="col-12">
				<h3 style="color: #8888FF;">Blue Attributes</h3>
			</div>
			<?php
			$html = '';
			foreach (range(1, 9) as $p) {
				?>
				<div class="col-2" style="background: #D6DDFF;">
					<select class="custom-select prop<?php echo $p ?>-select" name="prop<?php echo $p ?>">
						<option class="PropFirst" value="">Prop<?php echo $p ?></option>
						<?php
						foreach ($prop as $a) {
							echo '<option value="' . $a . '">' . $a. '</option>';
						}
						?>
					</select>
					<input  name="par<?php echo $p ?>" placeholder="Par<?php echo $p ?>" type="number" aria-describedby="par<?php echo $p ?>HelpBlock" class="form-control par<?php echo $p ?>">

					<input name="min<?php echo $p ?>" placeholder="min<?php echo $p ?>" type="number" aria-describedby="min<?php echo $p ?>HelpBlock" class="form-control min<?php echo $p ?>">

					<input name="max<?php echo $p ?>" placeholder="max<?php echo $p ?>" type="number" aria-describedby="max<?php echo $p ?>HelpBlock" class="form-control max<?php echo $p ?>">

				</div>
				<?php
			}
			?>
		</div>

	</div>
		<div class="form-group row">	
			<div class="col-12">
				<h3 style="color: green">Green Attributes</h3>
				<p style="font-family: lato;">Note: Edit boxes in Vertical Order. <br>[aprop1a, aprop1b][aprop2a, aprop2b]</p>

			</div>
			<?php
			$html = '';
			foreach (range(1, 5) as $p) {
				?>
				<div class="col-2" style="background: #DDFFD6">
					<select name="aprop<?php echo $p ?>a" class="custom-select">
						<option class="aPropFirst" value="">aProp<?php echo $p ?>a</option>
						<?php
						foreach ($prop as $a) {
							echo '<option value="' . $a . '">' . $a . '</option>';
						}
						?>
					</select>
					<input name="apar<?php echo $p ?>a" placeholder="apar<?php echo $p ?>a" type="number" aria-describedby="apar<?php echo $p ?>aHelpBlock" class="form-control apar<?php echo $p ?>a">

					<input name="amin<?php echo $p ?>a" placeholder="amin<?php echo $p ?>a" type="number" aria-describedby="amin<?php echo $p ?>aHelpBlock" class="form-control amin<?php echo $p ?>a">

					<input name="amax<?php echo $p ?>a" placeholder="amax<?php echo $p ?>a" type="number" aria-describedby="amax<?php echo $p ?>aHelpBlock" class="form-control amax<?php echo $p ?>a">
					<i class="fa fa-arrow-down" aria-hidden="true"></i>
					<!-- b -->	

					<div style="margin-top: 0px;">
						<i class="fa fa-arrow-down" aria-hidden="true"></i>

						<select name="aprop<?php echo $p ?>b" class="custom-select">
							<option class="aPropFirst" value="">aProp<?php echo $p ?>b</option>
							<?php
							foreach ($prop as $a) {
								echo '<option value="' . $a. '">' . $a . '</option>';
							}
							?>
						</select>

						<input  name="apar<?php echo $p ?>b" placeholder="apar<?php echo $p ?>b" type="number" aria-describedby="apar<?php echo $p ?>aHelpBlock" class="form-control apar<?php echo $p ?>b">

						<input  name="amin<?php echo $p ?>b" placeholder="amin<?php echo $p ?>b" type="number" aria-describedby="amin<?php echo $p ?>aHelpBlock" class="form-control amin<?php echo $p ?>b">

						<input  name="amax<?php echo $p ?>b" placeholder="amax<?php echo $p ?>b" type="number" aria-describedby="amax<?php echo $p ?>aHelpBlock" class="form-control amax<?php echo $p ?>b">
					</div>

				</div>
				<?php
			}
			?>

		</div>
		<div class="form-group row">
			<span class="help"><i class="fa fa-question-circle" aria-hidden="true"></i>
			</span>
			<span  class="form-text">
				<p><strong>aprop1a,aprop1b to aprop5a,aprop5b:</strong> An ID pointer of a property from Properties.txt, these columns control each of the five pairs of different variable (green) modifiers a set item can grant you at most.</p>
				<p><strong>apar1a,apar1b to apar5a,apar5b:</strong> The parameter passed on to the associated property, this is used to pass skill IDs, state IDs, monster IDs, montype IDs and the like on to the properties that require them, these fields support calculations.</p>
				<p><strong>amin1a,amin1b to amin5a,amin5b:</strong> Minimum value to assign to the associated property. Certain properties have special interpretations based on stat encoding (e.g. chance-to-cast and charged skills). See the File Guide for Properties.txt and ItemStatCost.txt for further details.</p>
				<p><strong>amax1a,amax1b to amax5a,amax5b:</strong> Maximum value to assign to the associated property. Certain properties have special interpretations based on stat encoding (e.g. chance-to-cast and charged skills). See the File Guide for Properties.txt and ItemStatCost.txt for further details.</p>



			</span>		
		</div>


		<input type="hidden" name="*eol" value="0">	
		<?php include '../optionSubmit.php'; ?>

		<!-- distinguish between forms -->
		<input type="hidden" name="formtype" value="setitems">	


</form>
<ul>
	<li><a href="https://d2mods.info/forum/kb/viewarticle?a=348">SetItems.txt</a></li>
	<li><a href="https://d2mods.info/forum/kb/viewarticle?a=349">Sets.txt</a></li>
	<li><a href="https://d2mods.info/forum/viewtopic.php?t=34455">Intro to the D2 Files and File Guide/Tutorial Masterlist</a></li>
	<li><a href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=2">Armor.txt by Kingpin et al. [ex Nefarius] (accurate)</a></li>
	<li><a href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=345">Properties.txt by Joel (revised by Myhrginoc 8/22/06)</a></li>
	<li><a href="https://d2mods.info/index.php?ind=reviews&amp;op=entry_view&amp;iden=346">Weapons.txt by Kingpin and Ric Faith (accurate)</a></li>

</ul>