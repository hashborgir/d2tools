<?php
/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
session_start();

require_once './D2Functions.php';
require_once './D2Database.php';
require_once '../_pdo.php';
include "../config.php";

// the form posts to it self, and then forwards to processFiles
if (!empty($_POST)) {

    $modname = str_replace(' ', '', $_POST['modname']);
    $_SESSION['modname'] = $modname;
    $time = time();
    $savePath = '';
    // write the D2Modder.db file and replace \ with \\
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $path = rtrim($_POST['path'], '\\');
        $path = str_replace('\\', '/', $path); // replace \\ (\' is invalid, so \\ escapes the first backslash) with /

        $modpath = $path;
        $savePath = $path . '/save/';
        $tbl = $path . '/data/local/lng/eng/';
        $path .= '/data/global/excel/';

        PDO_Connect("sqlite:../D2Modder.db");

        $sql = "
                CREATE TABLE IF NOT EXISTS D2Modder (
                modname VARCHAR(255),
                modpath VARCHAR(255),
                savepath VARCHAR(255),
                path VARCHAR(255),
                tbl VARCHAR(255),
                lastused INT,
                theme INT
        )";
        PDO_Execute($sql);

        $sql = '';
        if (!is_dir($path)) {
            echo '
                <center>
                    <h1 style="font-family:Lato; color:#880000;">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        ERROR: INVALID PATH
                    </h1>
                </center>
            ';
        } else {
            // set this mod to active mod in session
            $_SESSION['path'] = $path;
            $_SESSION['tbl'] = $tbl;
            $_SESSION['savepath'] = $savePath;
            $_SESSION['docpath'] = 'docs/' . $_SESSION['modname'];
            $_SESSION['modpath'] = $modpath;
            // Don't yell at me, security is the least of my considerations atm
            // check modname in db
            $sql = "SELECT * FROM D2Modder WHERE modname=?";
            $res = PDO_FetchAll($sql, [$modname]);

            if (empty($res)) {
                $sql = "INSERT INTO D2Modder(`modname`,`modpath`,`savepath`,`path`,`tbl`,`lastused`) VALUES(?, ?, ?, ?, ?, ?)";
                PDO_Execute($sql, [$modname, $modpath, $savePath, $path, $tbl, $time]);
            }
            header("Location: /processFiles.php");
        }
    }
}
?>
<!doctype html>
<html lang="en">
    <?php
    /* Require the <head> section */
    $css = '';
    require_once "head.php";
    ?>
    <style>
        body {
            background: white;
        }

        h1 {
            display: inline;
            font-weight: 900;
        }

        .btn-outline-danger {
            color: red;
            font-size: 18px;
            float: right;
        }

        hr {
            margin: 60px;
        }

        p {
            font-family: Lato;
        }

        .ctrl-config {
            margin-top: 20px;
        }

        form {
            font-family: Lato;
            font-size: 14pt;
        }

        input#path {
            width: 220px;
        }

        input#path[name="path"] {
            width: 420px;
        }

        .btn-config {
            /* Add any necessary styles */
        }

        #loading p {
            font-family: Lato;
        }
    </style>

    <body>
        <div class="center container container-top">
            <img src="/img/Diablo2.png" style="float:left">
            <h1><?php echo $title . " " . $version; ?><span style="font-family: Lato !important; font-size: 14px;"> <?php echo " By: " . $author ?></span></h1>
            <a class="btn btn-outline-danger" href="/">X</a>
            <hr>
            <div class="offset-2 col-8">
                <h2>Select Mod folder</h2>
                <p>Example: D:\Diablo II\MODS\your-mod-name\</p>
                <p>Input path to D2 Mod Directory.</p>
                <div class="ctrl-config">
                    <form enctype="multipart/form-data" action="" method="post">
                        <p>
                            <label for="path">Enter Mod Name</label><br>
                            <input id="path" required="required" name="modname" type="text">
                        </p>
                        <p>
                            <label for="path">Enter PATH </label><br>
                            <input id="path" required="required" name="path" type="text">
                            <input type="submit" value="Save" name="submit" class="btn-config">
                        </p>
                    </form>
                </div>
            </div>
            <div id="loading" class="offset-2 col-8 alert">
                <p>After saving path, app will convert txt files to SQL database.</p>
                <p>This may take a few minutes depending on the size of your txt files.<br>Please be patient. Once done, app will load.<br>Each time you set a new mod path, txt files will be processed.<br>Every mod will be saved to ModName.db.</p>
            </div>
        </div>
    </body>

</html>