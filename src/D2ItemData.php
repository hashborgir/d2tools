<?php

/**
 *
 */
class D2ItemData {

    /**
     * @var
     */
    public $strings;
    /**
     * @var
     */
    public $images;

    /**
     * @return void
     */
    public function getImages() {
        $sql = "SELECT code,uniqueinvfile,invfile,type,type2 FROM armor
					UNION ALL
				SELECT code,uniqueinvfile,invfile,type,type2 FROM misc
					UNION ALL
				SELECT code,uniqueinvfile,invfile,type,type2 FROM weapons";
        $res = PDO_FetchAll($sql);      
        $isc = null;
        foreach ($res as $r) {
            $isc1[$r['code']] = $r;
            $isc1[$r['code']]['code'] = $r['code'];
            $isc2[$r['type']] = $r;
            $isc2[$r['type']]['type'] = $r['type'];
        }
        $this->images = array_merge($isc1, $isc2);
        unset($isc1);
        unset($isc2);
    }

    /**
     * @param $index
     * @param $doc
     * @return false|string
     */
    public function uniqueItems($index, $doc = FALSE) {
        if (empty($this->images)) {
            $this->getImages();
        }      
        // This is where we get props for each item
        $sql = "SELECT * FROM uniqueitems WHERE enabled='1' AND `index`=?";
        $res = (PDO_FetchRow($sql, [$index]));        
               
        if (empty($res['uniqueinvfile'])){
            $sql = "SELECT InvGfx1 FROM itemtypes WHERE Code = '{$this->images[$res['code']]['type']}'";            
            $invfile = PDO_FetchOne($sql);
        }       
        if (empty($invfile)){
            $invfile = $this->images[$res['code']]['invfile'];
        }
        $res['invfile'] = $invfile;
        $return = $res;
        
         // for all props for this unique item, get ISC strings
        $props = array_filter([
            "Prop1" => array_filter(
                    ["prop1" => $return['prop1'], "par1" => $return['par1'], "min1" => $return['min1'], "max1" => $return['max1']]
            ),
            "Prop2" => array_filter(
                    ["prop2" => $return['prop2'], "par1" => $return['par2'], "min1" => $return['min2'], "max1" => $return['max2']]
            ),
            "Prop3" => array_filter(
                    ["prop3" => $return['prop3'], "par1" => $return['par3'], "min1" => $return['min3'], "max1" => $return['max3']]
            ),
            "Prop4" => array_filter(
                    ["prop4" => $return['prop4'], "par1" => $return['par4'], "min1" => $return['min4'], "max1" => $return['max4']]
            ),
            "Prop5" => array_filter(
                    ["prop5" => $return['prop5'], "par1" => $return['par5'], "min1" => $return['min5'], "max1" => $return['max5']]
            ),
            "Prop6" => array_filter(
                    ["prop6" => $return['prop6'], "par1" => $return['par6'], "min1" => $return['min6'], "max1" => $return['max6']]
            ),
            "Prop7" => array_filter(
                    ["prop7" => $return['prop7'], "par1" => $return['par7'], "min1" => $return['min7'], "max1" => $return['max7']]
            ),
            "Prop8" => array_filter(
                    ["prop8" => $return['prop8'], "par1" => $return['par8'], "min1" => $return['min8'], "max1" => $return['max8']]
            ),
            "Prop9" => array_filter(
                    ["prop9" => $return['prop9'], "par1" => $return['par9'], "min1" => $return['min9'], "max1" => $return['max9']]
            ),
            "Prop10" => array_filter(
                    ["prop10" => $return['prop10'], "par1" => $return['par10'], "min1" => $return['min10'], "max1" => $return['max10']]
            )
        ]);

        // need par, min, max value for each prop
        // ( Also need func1 and val1 for each prop )
        // for each prop, get stat
        $counter = 1;
        foreach ($props as $key => $val) {
            // val = Prop1,Prop2 etc.
            // for each propr, get all 7 stats in ISC
            // for now only get stat1, and for else, use dgrp
            // prop funcs
            /*
             * 1 - Applies a value to a stat, can use SetX parameter.
              2 - defensive function only, similar to 1 ???
              3 - Apply the same min-max range as used in the previous function block (see res-all).
              4 - not used ???
              5 - Dmg-min related ???
              6 - Dmg-max related ???
              7 - Dmg% related ???
              8 - ??? use for speed properties (ias, fcr, etc ...)
              9 - Apply the same param and value in min-max range, as used in the previous function block.
              10 - skilltab skill group ???
              11 - event-based skills ???
              12 - random selection of parameters for parameter-based stat ???
              13 - durability-related ???
              14 - inventory positions on item ??? (related to socket)
              15 - use min field only
              16 - use max field only
              17 - use param field only
              18 - Related to /time properties.
              19 - Related to charged item.
              20 - Simple boolean stuff. Use by indestruct.
              21 - Add to group of skills, group determined by stat ID, uses ValX parameter.
              22 - Individual skill, using param for skill ID, random between min-max.
              23 - ethereal
              24 - property applied to character or target monster ???
              25--32 can be used in custom code.  Check plugin documentation for syntax.
             *
             */


            // prop1 is dmg% then...


            $sql = "SELECT
                           func1,stat1
                           
                     FROM properties WHERE code = ?";
            $props[$key]['stat'] = PDO_FetchRow($sql, [$val['prop' . $counter]]);
            // if not empty, filter props key stat
            if (!empty($props[$key]['stat'])) {
                $props[$key]['stat'] = array_filter($props[$key]['stat']);
            }

            $counter++;
        }


        // this is where every stat from peroperties stat1 goes into getISCStrings
        // but what is stat1 is empty, then we generate custom info for it.

        $counter = 1;
        //each stat now goes into getIscStrings
        foreach ($props as $k => $v) {
            $props[$k]['desc'] = $this->getIscStrings($v['prop' . $counter]);
            $counter++;
        }

        // Values in params, so I can get generated string for that prop
        $counter = 1;
        foreach ($props as $k => $v) { // for each property Prop1 Prop2
            // if prop1 = our custom props like dmg%
            // then create new function in idesc-> as getCustomDesc()
            // and get it from there. we have min/max for the prop
            //ddump($v['prop1']);
            if ($v['prop1'] == 'dmg%' || $v['prop2'] == 'dmg%' || $v['prop3'] == 'dmg%' || $v['prop4'] == 'dmg%' || $v['prop5'] == 'dmg%' || $v['prop6'] == 'dmg%' || $v['prop7'] == 'dmg%') {
                $params = [
                    'string1' => "Enhanced Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-min' ||
                    $v['prop2'] == 'dmg-min' ||
                    $v['prop3'] == 'dmg-min' ||
                    $v['prop4'] == 'dmg-min' ||
                    $v['prop5'] == 'dmg-min' ||
                    $v['prop6'] == 'dmg-min' ||
                    $v['prop7'] == 'dmg-min') {
                $params = [
                    'string1' => " Minimum Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-max' ||
                    $v['prop2'] == 'dmg-max' ||
                    $v['prop3'] == 'dmg-max' ||
                    $v['prop4'] == 'dmg-max' ||
                    $v['prop5'] == 'dmg-max' ||
                    $v['prop6'] == 'dmg-max' ||
                    $v['prop7'] == 'dmg-max') {
                $params = [
                    'string1' => "Maximum Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'res-all' ||
                    $v['prop2'] == 'res-all' ||
                    $v['prop3'] == 'res-all' ||
                    $v['prop4'] == 'res-all' ||
                    $v['prop5'] == 'res-all' ||
                    $v['prop6'] == 'res-all' ||
                    $v['prop7'] == 'res-all') {
                $params = [
                    'string1' => "Resist All",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'res-all-max' ||
                    $v['prop2'] == 'res-all-max' ||
                    $v['prop3'] == 'res-all-max' ||
                    $v['prop4'] == 'res-all-max' ||
                    $v['prop5'] == 'res-all-max' ||
                    $v['prop6'] == 'res-all-max' ||
                    $v['prop7'] == 'res-all-max') {
                $params = [
                    'string1' => "Resist All Max",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'fireskill' ||
                    $v['prop2'] == 'fireskill' ||
                    $v['prop3'] == 'fireskill' ||
                    $v['prop4'] == 'fireskill' ||
                    $v['prop5'] == 'fireskill' ||
                    $v['prop6'] == 'fireskill' ||
                    $v['prop7'] == 'fireskill') {
                $params = [
                    'string1' => "to Fire Skills",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-min' ||
                    $v['prop2'] == 'dmg-min' ||
                    $v['prop3'] == 'dmg-min' ||
                    $v['prop4'] == 'dmg-min' ||
                    $v['prop5'] == 'dmg-min' ||
                    $v['prop6'] == 'dmg-min' ||
                    $v['prop7'] == 'dmg-min') {
                $params = [
                    'string1' => "Increased Chance of Finding Gems",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'regen-dur' ||
                    $v['prop2'] == 'regen-dur' ||
                    $v['prop3'] == 'regen-dur' ||
                    $v['prop4'] == 'regen-dur' ||
                    $v['prop5'] == 'regen-dur' ||
                    $v['prop6'] == 'regen-dur' ||
                    $v['prop7'] == 'regen-dur') {
                $params = [
                    'string1' => "Repair Durability",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'gems%/lvl' ||
                    $v['prop2'] == 'gems%/lvl' ||
                    $v['prop3'] == 'gems%/lvl' ||
                    $v['prop4'] == 'gems%/lvl' ||
                    $v['prop5'] == 'gems%/lvl' ||
                    $v['prop6'] == 'gems%/lvl' ||
                    $v['prop7'] == 'gems%/lvl') {
                $params = [
                    'string1' => "Increase chance of finding gems per character level",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-slash' ||
                    $v['prop2'] == 'dmg-slash' ||
                    $v['prop3'] == 'dmg-slash' ||
                    $v['prop4'] == 'dmg-slash' ||
                    $v['prop5'] == 'dmg-slash' ||
                    $v['prop6'] == 'dmg-slash' ||
                    $v['prop7'] == 'dmg-slash') {
                $params = [
                    'string1' => "Slashing Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-slash%' ||
                    $v['prop2'] == 'dmg-slash%' ||
                    $v['prop3'] == 'dmg-slash%' ||
                    $v['prop4'] == 'dmg-slash%' ||
                    $v['prop5'] == 'dmg-slash%' ||
                    $v['prop6'] == 'dmg-slash%' ||
                    $v['prop7'] == 'dmg-slash%') {
                $params = [
                    'string1' => "% Slashing Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-crush' ||
                    $v['prop2'] == 'dmg-crush' ||
                    $v['prop3'] == 'dmg-crush' ||
                    $v['prop4'] == 'dmg-crush' ||
                    $v['prop5'] == 'dmg-crush' ||
                    $v['prop6'] == 'dmg-crush' ||
                    $v['prop7'] == 'dmg-crush') {
                $params = [
                    'string1' => "Crushing Blow",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-crush%' ||
                    $v['prop2'] == 'dmg-crush%' ||
                    $v['prop3'] == 'dmg-crush%' ||
                    $v['prop4'] == 'dmg-crush%' ||
                    $v['prop5'] == 'dmg-crush%' ||
                    $v['prop6'] == 'dmg-crush%' ||
                    $v['prop7'] == 'dmg-crush%') {
                $params = [
                    'string1' => "% Crushing Blow",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-thrust' ||
                    $v['prop2'] == 'dmg-thrust' ||
                    $v['prop3'] == 'dmg-thrust' ||
                    $v['prop4'] == 'dmg-thrust' ||
                    $v['prop5'] == 'dmg-thrust' ||
                    $v['prop6'] == 'dmg-thrust' ||
                    $v['prop7'] == 'dmg-thrust') {
                $params = [
                    'string1' => "Thrust Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-thrust%' ||
                    $v['prop2'] == 'dmg-thrust%' ||
                    $v['prop3'] == 'dmg-thrust%' ||
                    $v['prop4'] == 'dmg-thrust%' ||
                    $v['prop5'] == 'dmg-thrust%' ||
                    $v['prop6'] == 'dmg-thrust%' ||
                    $v['prop7'] == 'dmg-thrust%') {
                $params = [
                    'string1' => "% Thrust Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'abs-slash' ||
                    $v['prop2'] == 'abs-slash' ||
                    $v['prop3'] == 'abs-slash' ||
                    $v['prop4'] == 'abs-slash' ||
                    $v['prop5'] == 'abs-slash' ||
                    $v['prop6'] == 'abs-slash' ||
                    $v['prop7'] == 'abs-slash') {
                $params = [
                    'string1' => "Absorb Slashing Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'abs-crush' ||
                    $v['prop2'] == 'abs-crush' ||
                    $v['prop3'] == 'abs-crush' ||
                    $v['prop4'] == 'abs-crush' ||
                    $v['prop5'] == 'abs-crush' ||
                    $v['prop6'] == 'abs-crush' ||
                    $v['prop7'] == 'abs-crush') {
                $params = [
                    'string1' => "Absorb Crushing Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'abs-thrust' ||
                    $v['prop2'] == 'abs-thrust' ||
                    $v['prop3'] == 'abs-thrust' ||
                    $v['prop4'] == 'abs-thrust' ||
                    $v['prop5'] == 'abs-thrust' ||
                    $v['prop6'] == 'abs-thrust' ||
                    $v['prop7'] == 'abs-thrust') {
                $params = [
                    'string1' => "Absorb Thrusting Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'abs-slash%' ||
                    $v['prop2'] == 'abs-slash%' ||
                    $v['prop3'] == 'abs-slash%' ||
                    $v['prop4'] == 'abs-slash%' ||
                    $v['prop5'] == 'abs-slash%' ||
                    $v['prop6'] == 'abs-slash%' ||
                    $v['prop7'] == 'abs-slash%') {
                $params = [
                    'string1' => "% Absorb Slashing Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'abs-crush%' ||
                    $v['prop2'] == 'abs-crush%' ||
                    $v['prop3'] == 'abs-crush%' ||
                    $v['prop4'] == 'abs-crush%' ||
                    $v['prop5'] == 'abs-crush%' ||
                    $v['prop6'] == 'abs-crush%' ||
                    $v['prop7'] == 'abs-crush%') {
                $params = [
                    'string1' => "% Absorb Crushing Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'abs-thrust%' ||
                    $v['prop2'] == 'abs-thrust%' ||
                    $v['prop3'] == 'abs-thrust%' ||
                    $v['prop4'] == 'abs-thrust%' ||
                    $v['prop5'] == 'abs-thrust%' ||
                    $v['prop6'] == 'abs-thrust%' ||
                    $v['prop7'] == 'abs-thrust%') {
                $params = [
                    'string1' => "% Absorb Thrusting Damage",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "2",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg-mon' ||
                    $v['prop2'] == 'dmg-mon' ||
                    $v['prop3'] == 'dmg-mon' ||
                    $v['prop4'] == 'dmg-mon' ||
                    $v['prop5'] == 'dmg-mon' ||
                    $v['prop6'] == 'dmg-mon' ||
                    $v['prop7'] == 'dmg-mon') {
                $params = [
                    'string1' => "Damage vs.",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "22",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'dmg%-mon' ||
                    $v['prop2'] == 'dmg%-mon' ||
                    $v['prop3'] == 'dmg%-mon' ||
                    $v['prop4'] == 'dmg%-mon' ||
                    $v['prop5'] == 'dmg%-mon' ||
                    $v['prop6'] == 'dmg%-mon' ||
                    $v['prop7'] == 'dmg%-mon') {
                $params = [
                    'string1' => "% Damage vs.",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "22",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'att%-mon' ||
                    $v['prop2'] == 'att%-mon' ||
                    $v['prop3'] == 'att%-mon' ||
                    $v['prop4'] == 'att%-mon' ||
                    $v['prop5'] == 'att%-mon' ||
                    $v['prop6'] == 'att%-mon' ||
                    $v['prop7'] == 'att%-mon') {
                $params = [
                    'string1' => "To Hit % vs. ",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "22",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'ac-mon' ||
                    $v['prop2'] == 'ac-mon' ||
                    $v['prop3'] == 'ac-mon' ||
                    $v['prop4'] == 'ac-mon' ||
                    $v['prop5'] == 'ac-mon' ||
                    $v['prop6'] == 'ac-mon' ||
                    $v['prop7'] == 'ac-mon') {
                $params = [
                    'string1' => "Defense against ",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "22",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'ac%-mon' ||
                    $v['prop2'] == 'ac%-mon' ||
                    $v['prop3'] == 'ac%-mon' ||
                    $v['prop4'] == 'ac%-mon' ||
                    $v['prop5'] == 'ac%-mon' ||
                    $v['prop6'] == 'ac%-mon' ||
                    $v['prop7'] == 'ac%-mon') {
                $params = [
                    'string1' => "% Defense against",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "22",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'indestruct' ||
                    $v['prop2'] == 'indestruct' ||
                    $v['prop3'] == 'indestruct' ||
                    $v['prop4'] == 'indestruct' ||
                    $v['prop5'] == 'indestruct' ||
                    $v['prop6'] == 'indestruct' ||
                    $v['prop7'] == 'indestruct') {
                $params = [
                    'string1' => "Indestructable",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'ethereal' ||
                    $v['prop2'] == 'ethereal' ||
                    $v['prop3'] == 'ethereal' ||
                    $v['prop4'] == 'ethereal' ||
                    $v['prop5'] == 'ethereal' ||
                    $v['prop6'] == 'ethereal' ||
                    $v['prop7'] == 'ethereal') {
                $params = [
                    'string1' => "Ethereal (Cannot be repaired)",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'skill-rand' ||
                    $v['prop2'] == 'skill-rand' ||
                    $v['prop3'] == 'skill-rand' ||
                    $v['prop4'] == 'skill-rand' ||
                    $v['prop5'] == 'skill-rand' ||
                    $v['prop6'] == 'skill-rand' ||
                    $v['prop7'] == 'skill-rand') {
                $params = [
                    'string1' => "+ to Random Skill",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "1",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'aura-rand' ||
                    $v['prop2'] == 'aura-rand' ||
                    $v['prop3'] == 'aura-rand' ||
                    $v['prop4'] == 'aura-rand' ||
                    $v['prop5'] == 'aura-rand' ||
                    $v['prop6'] == 'aura-rand' ||
                    $v['prop7'] == 'aura-rand') {
                $params = [
                    'string1' => "to Random Aura When Equipped",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "16",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else if ($v['prop1'] == 'sock' ||
                    $v['prop2'] == 'sock' ||
                    $v['prop3'] == 'sock' ||
                    $v['prop4'] == 'sock' ||
                    $v['prop5'] == 'sock' ||
                    $v['prop6'] == 'sock' ||
                    $v['prop7'] == 'sock') {
                $params = [
                    'string1' => "Sockets",
                    'string2' => "",
                    'gstring1' => "",
                    'gstring2' => "",
                    'descfunc' => "9",
                    'descval' => "1",
                    'dgrp' => "",
                    'dgrpfunc' => "",
                    'dgrpval' => "",
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            } else {
                $params = [
                    'string1' => $v['desc']['string1'],
                    'string2' => $v['desc']['string2'],
                    'gstring1' => $v['desc']['gstring1'],
                    'gstring2' => $v['desc']['gstring2'],
                    'descfunc' => $v['desc']['descfunc'],
                    'descval' => $v['desc']['descval'],
                    'dgrp' => $v['desc']['dgrp'],
                    'dgrpfunc' => $v['desc']['dgrpfunc'],
                    'dgrpval' => $v['desc']['dgrpval'],
                    "prop" => $return['prop' . $counter],
                    "par" => $return['par' . $counter],
                    "min" => $return['min' . $counter],
                    "max" => $return['max' . $counter],
                ];
            }



            /*
             * getDesc should process string1, not file guide copy pasta
             *
             */
            if ((!empty($v['desc']))) {
                require_once 'D2ItemDesc.php';
                $idesc = new D2ItemDesc();

                // process all properties.txt func1 here, total 25 in comments above
                //process property func 12, random skill
                if ($v['stat']['func1'] == '12') {
                    $props[$k]['string'][] = "+{$params['par']} to Random Skill";
                } else {
                    $props[$k]['string'][] = ($idesc->getDesc($params));
                }


                // ddump($props[$k]['string']);
            }
            $counter++;
        }

        $return['props'] = $props;

        $sqlArmor = "SELECT * FROM armor WHERE code=?";
        $sqlMisc = "SELECT * FROM misc WHERE code=?";
        $sqlWeapons = "SELECT * FROM weapons WHERE code=?";

        $baseItemInfo = PDO_FetchRow($sqlArmor, [$res['code']]);
        $uTable = "armor";

        if (empty($baseItemInfo)) {
            $baseItemInfo = PDO_FetchRow($sqlMisc, [$res['code']]);
        }
        if (empty($baseItemInfo)) {
            $baseItemInfo = PDO_FetchRow($sqlWeapons, [$res['code']]);
        }

        $return['baseItemInfo'] = $baseItemInfo;

        if (!empty($baseItemInfo)) {
            $return['baseItemInfo'] = array_filter($baseItemInfo);
        }

        if (!$doc) {
            header('Content-Type: application/json');
            return json_encode($return, JSON_INVALID_UTF8_IGNORE | JSON_PRETTY_PRINT);
        } else {
            return $return;
        }
    }

    /**
     * @param $iscStat
     * @return mixed
     */
    public function getIscStrings($iscStat) {
        if (empty($this->strings)) {
            $this->getStrings();
        }
        return ($this->strings[$iscStat]);
    }

    /**
     * @return void
     */
    public function getStrings() {
        $sql = "
				SELECT p.`code` as prop,
					p.stat1,
					i.descstrpos,
					i.descstr2,
					i.descfunc,
					i.descval,
					i.dgrp,
					i.dgrpfunc,
					i.dgrpval,
					i.dgrpstrpos,
					i.dgrpstrneg,
					s1.String as string1,
					s2.String as string2,
					s3.String as gstring1,
					s4.String as gstring2
				FROM properties as p

				LEFT JOIN itemstatcost as i
				ON p.stat1 = i.Stat

				LEFT JOIN strings as s1
				ON i.descstrpos = s1.Key
				LEFT JOIN strings as s2
				ON i.descstr2 = s2.Key

				LEFT JOIN strings as s3
				ON i.dgrpstrpos = s3.Key
				LEFT JOIN strings as s4
				ON i.dgrpstr2= s4.Key
				";
        $strings = PDO_FetchAll($sql);
        foreach ($strings as $k => $v) {
            $this->strings[$v['prop']] = $v;
        }
    }

}
