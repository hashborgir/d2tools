<?php

/*

	Copyright (C) 2021 Hash Borgir

	This file is part of D2Modder 

	Redistribution and use in source and binary forms, with
	or without modification, are permitted provided that the
	following conditions are met:

	* Redistributions of source code must retain the above
	  copyright notice, this list of conditions and the
	  following disclaimer.

	* Redistributions in binary form must reproduce the above
	  copyright notice, this list of conditions and the
	  following disclaimer in the documentation and/or other
	  materials provided with the distribution.

	* This software must not be used for commercial purposes 
	* without my consent. Any sales or commercial use are prohibited
	* without my express knowledge and consent.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY! 

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
	CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 *
 */
class D2SaveTXT {

    /**
     * @var mixed
     */
    public $path;

    /**
     * @param $file
     * @param $data
     * @return void
     */
    public function save($file, $data) {
	
		$fp = fopen($this->path.DIRECTORY_SEPARATOR.$file, 'a+');
		$this->saveBackup($file);		
		fputcsv($fp, $data, "\t");		
	}


    /**
     * @param $file
     * @return void
     */
    public function saveBackup($file){
		
		// if dir doesn't exist, create it
		if (!is_dir($this->path.DIRECTORY_SEPARATOR."backup")) mkdir($this->path."backup", 0700);
		
		$oldfile = $this->path.$file;
		
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		// set new file location to copy to (backup)
		$newfile = $this->path."\\backup\\$file";
		} else {
			$newfile = $this->path."backup/$file";
		}
		if (!copy($oldfile, $newfile)) {
			echo "Failed to create backup of $file...\n";
		}
	}

    /**
     * @param $filename
     * @return void
     */
    public function saveTblEnries($filename) {
		$post = $_POST;

		if (!is_dir($this->path."tblEntries")) mkdir($this->path."tblEntries", 0700);
		
		// write for .tbl
		$str = '"'.$post['index'].'"'."\t".'"'.$post['index'].'"'.PHP_EOL;
		$file = $this->path."\\tblEntries\\$filename";
		file_put_contents($file, $str, FILE_APPEND);
	}

    /**
     *
     */
    public function __construct() {
		$this->path = TXT_PATH;
	}

}
?>