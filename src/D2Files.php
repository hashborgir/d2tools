<?php

/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * D2Files class.
 */
class D2Files {

    /**
     * @var array Holds the list of files.
     */
    public $files = [];

    /**
     * @var array Holds the character files.
     */
    public $charFiles;

    /**
     * D2Files constructor.
     */
    public function __construct() {
        $filesToIgnore = [
            "aiparms.txt",
            "shrines.txt",
            "cubemain.txt", // cubemain is processed manually in sqlite cli tool
            "misc.txt", // grew too large, process manually in processmanually function
            "treasureclass.txt",
            "treasureclassex.txt"
        ];

        $glob = glob($_SESSION['path'] . '*.txt'); // Get a list of all ".txt" files in the specified path.

        foreach ($glob as $g) {
            $files[] = basename($g); // Add the base name of each file to the $files array.
        }

        $this->files = array_udiff($files, $filesToIgnore, 'strcasecmp'); // Remove the ignored files from the list, case-insensitive.
        return $this->files; // Return the list of files.
    }

    /**
     * Get the list of character save files.
     *
     * @return mixed The list of character save files.
     */
    public function getSaveFiles() {
        $glob = glob($_SESSION['savepath'] . '*.d2s'); // Get a list of all ".d2s" files in the specified save path.

        foreach ($glob as $g) {
            $this->charFiles[] = basename($g); // Add the base name of each file to the $charFiles array.
        }

        return $this->charFiles; // Return the list of character save files.
    }

}
