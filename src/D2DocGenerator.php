<?php

/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes 
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 *
 */
class D2DocGenerator {

    /**
     *
     */
    public function __construct() {
        require_once './config.php';
        require_once './_pdo.php';
        require_once './src/D2ItemData.php';
        require_once './src/D2ItemDesc.php';

        $idata = new D2ItemData();
        $idesc = new D2ItemDesc();

        define('DB_FILE', $_SESSION['modname'] . ".db");
        PDO_Connect("sqlite:" . DB_FILE);

        require_once "./src/D2Functions.php";

        $idata = new D2ItemData();
    }

    /**
     * @return array
     */
    public function getIscProps() {
        $sql = "
		SELECT p.`code` as prop,
			p.stat1,
			i.descstrpos,
			i.descstr2,
			i.descfunc,
			i.descval,
			i.dgrp,
			i.dgrpfunc,
			i.dgrpval,
			i.dgrpstrpos,
			i.dgrpstrneg,
			s1.String as string1,
			s2.String as string2,
			s3.String as gstring1,
			s4.String as gstring2
		FROM properties as p

		LEFT JOIN itemstatcost as i
		ON p.stat1 = i.Stat

		LEFT JOIN strings as s1
		ON i.descstrpos = s1.Key
		LEFT JOIN strings as s2
		ON i.descstr2 = s2.Key

		LEFT JOIN strings as s3
		ON i.dgrpstrpos = s3.Key
		LEFT JOIN strings as s4
		ON i.dgrpstr2= s4.Key
		";
        $res = PDO_FetchAll($sql);

        $isc = null;
        foreach ($res as $r) {
            $isc[$r['prop']] = $r;
            $isc[$r['prop']]['prop'] = $r['prop'];
        }
        return $isc;
    }

    /**
     * @return array
     */
    public function getStrings() {
        // load strings
        $sql = 'SELECT * FROM strings';
        $strings = array_filter(PDO_FetchAssoc($sql));
        return $strings;
    }

    /**
     * @return array
     */
    public function getItemTypesTbl() {
        // load itemtypes table in memory
        $sql = "SELECT ItemType,Code FROM itemtypes";
        $itemtypesTbl = PDO_FetchAssoc($sql);

        return $itemtypesTbl;
    }

    /**
     * @return array
     */
    public function getNameStr() {
        // load namestr from 3 files
        $sql = "SELECT code,namestr FROM armor
        UNION
        SELECT code,namestr FROM misc
        UNION
        SELECT code,namestr FROM weapons";
        $namestr = PDO_FetchAssoc($sql);

        return $namestr;
    }

    /**
     * @param $code
     * @return string
     */
    public function getImage($code) {
        $sql = "SELECT invfile FROM armor WHERE `code`=\"$code\" OR `type`=\"$code\" OR `type2`=\"$code\"";
        $img = PDO_FetchOne($sql);
        if (empty($img)) {
            $sql = "SELECT invfile FROM misc WHERE `code`=\"$code\" OR `type`=\"$code\" OR `type2`=\"$code\"";
            $img = PDO_FetchOne($sql);
        }
        if (empty($img)) {
            $sql = "SELECT invfile FROM weapons WHERE `code`=\"$code\" OR `type`=\"$code\" OR `type2`=\"$code\"";
            $img = PDO_FetchOne($sql);
        }
        return $img = (!empty($img)) ? "$img.png" : "1.png";
    }

    /**
     * @param $code
     * @return false
     */
    public function getItemName($code) {
        $sql = "SELECT name FROM armor WHERE `code`=\"$code\" OR `type`=\"$code\" OR `type2`=\"$code\"";
        $name = PDO_FetchOne($sql);
        if (empty($name)) {
            $sql = "SELECT name FROM misc WHERE `code`=\"$code\" OR `type`=\"$code\" OR `type2`=\"$code\"";
            $name = PDO_FetchOne($sql);
        }
        if (empty($name)) {
            $sql = "SELECT name FROM weapons WHERE `code`=\"$code\" OR `type`=\"$code\" OR `type2`=\"$code\"";
            $name = PDO_FetchOne($sql);
        }
        return $name;
    }

    /**
     * @return void
     */
    public function generateDocs() {
        
    }

}
