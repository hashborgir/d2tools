<?php

class CRUD {
    private $table;

    public function __construct($table) {
        $this->table = $table;
    }

    /**
     * Insert a new row into the table
     *
     * @param array $data Associative array of column names and values
     * @return bool True on success, false on failure
     */
    public function create($data) {
        $keys = implode(', ', array_keys($data));
        $values = implode(', ', array_fill(0, count($data), '?'));

        $query = "INSERT INTO {$this->table} ($keys) VALUES ($values)";
        $params = array_values($data);

        return PDO_Execute($query, $params);
    }

    /**
     * Retrieve a row by its ROWID
     *
     * @param int $rowid The ROWID of the row to retrieve
     * @return mixed|null The row as an associative array, or null if not found
     */
    public function read($rowid) {
        $query = "SELECT * FROM {$this->table} WHERE ROWID = ?";
        $params = [$rowid];

        return PDO_FetchRow($query, $params);
    }

    /**
     * Retrieve all rows from the table
     *
     * @return array An array of rows, each represented as an associative array
     */
    public function readAll() {
        $query = "SELECT ROWID,* FROM {$this->table}";
        return PDO_FetchAll($query);
    }

    /**
     * Update a row by its ROWID
     *
     * @param int $rowid The ROWID of the row to update
     * @param array $data Associative array of column names and new values
     * @return bool True on success, false on failure
     */
    public function update($rowid, $data) {
        $setStatements = [];
        $params = [$rowid];

        foreach ($data as $key => $value) {
            $setStatements[] = "$key = ?";
            $params[] = $value;
        }

        $setClause = implode(', ', $setStatements);
        $query = "UPDATE {$this->table} SET $setClause WHERE ROWID = ?";

        return PDO_Execute($query, $params);
    }

    /**
     * Delete a row by its ROWID
     *
     * @param int $rowid The ROWID of the row to delete
     * @return bool True on success, false on failure
     */
    public function delete($rowid) {
        $query = "DELETE FROM {$this->table} WHERE ROWID = ?";
        $params = [$rowid];

        return PDO_Execute($query, $params);
    }
}
