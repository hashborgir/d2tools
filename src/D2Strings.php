<?php

/**
 *
 */
class D2Strings {
    /**
     * @var array
     */
    public array $strings;

    /**
     *
     */
    public function __construct(){
        $sql = "SELECT * FROM strings";
        $this->strings = PDO_FetchAssoc($sql); 
        $_SESSION['strings'] = $this->strings;
        return $this->strings;        
    }
}
