<?php

/**
 * D2BitReader class.
 */
class D2BitReader {
    /**
     * @var string The bit string.
     */
    private string $bits = '';
    
    /**
     * @var int The offset position.
     */
    private int $offset = 0;

    /**
     * D2BitReader constructor.
     *
     * @param string $bits The bit string.
     */
    public function __construct(string $bits = '') {
        $this->bits = $bits;
        return true;
    }

    /**
     * Skips a specified number of bits.
     *
     * @param int $numBits The number of bits to skip.
     * @return int The new offset position.
     */
    public function skip(int $numBits): int {
        $this->offset += $numBits;
        return $this->offset;
    }

    /**
     * Reads a specified number of bits.
     *
     * @param int $numBits The number of bits to read.
     * @param bool $str Determines whether to return a string or an array of bits.
     * @return string|array The read bits.
     */
public function read(int $numBits = 0, bool $str = true): string|array {
    $bits = null;  // Initialize a variable to store the extracted bits

    // Loop from the current offset to the current offset plus the specified number of bits
    // Extract each bit from the internal property $this->bits at index $i

    // If $str is true, concatenate the extracted bit to the $bits string
    // Otherwise, append the extracted bit to the $bits array    
    for ($i = $this->offset; $i < $this->offset + $numBits; $i++) {
        $str ? $bits .= $this->bits[$i] : $bits[] = $this->bits[$i];
        // Depending on the value of the $str parameter, concatenate the bits to form a string
        // or append them to an array
    }

    $this->offset += $numBits;  // Update the offset to move to the next position in the data

    return $bits;  // Return the extracted bits as a string or an array, based on the $str parameter
}


    /**
     * Writes bits to a specified offset in the bit string.
     *
     * @param string $bits The original bit string.
     * @param string $bitsToWrite The bits to write.
     * @param int $offset The offset position to write at.
     * @return array|string|string[] The modified bit string.
     */
    public function writeBits(string $bits, string $bitsToWrite, int $offset) {
        return substr_replace($bits, $bitsToWrite, $offset, strlen($bitsToWrite));
    }

    /**
     * Reads a specified number of bits in reverse order.
     *
     * @param int $numBits The number of bits to read.
     * @param bool $str Determines whether to return a string or an array of bits.
     * @return string The read bits in reverse order.
     */
    public function readb(int $numBits = 0, bool $str = true): string {
        $bits = '';
        for ($i = $this->offset; $i < $this->offset + $numBits; $i++) {
            $str ? $bits .= $this->bits[$i] : $bits[] = $this->bits[$i];
        }
        $this->offset += $numBits;
        return strrev(str_pad($bits, 16, 0, STR_PAD_RIGHT));
    }

    /**
     * Reads a specified number of bits in reverse order.
     *
     * @param int $numBits The number of bits to read.
     * @return string The read bits in reverse order.
     */
    public function readr(int $numBits = 0): string {
        $bits = null;
        for ($i = $this->offset; $i < $this->offset + $numBits; $i++) {
            $bits .= $this->bits[$i];
        }
        $this->offset += $numBits;
        return strrev($bits);
    }

    /**
     * Sets the offset position.
     *
     * @param int $pos The new offset position.
     * @return bool Returns true if the offset position is valid, false otherwise.
     */
    public function seek(int $pos): bool {
        if ($pos < 0 || $pos > strlen($this->bits)) {
            return false;
        }
        $this->offset = $pos;
        return true;
    }

    /**
     * Rewinds the offset position to 0.
     *
     * @return bool Returns true on success.
     */
    public function rewind(): bool {
        $this->offset = 0;
        return true;
    }

    /**
     * Retrieves the value of a bit at a given position.
     *
     * @param string $bitNum The position of the bit.
     * @return int The value of the bit (0 or 1).
     */
    public function getBit(string $bitNum): int {
        if ($bitNum < 0 || $bitNum > strlen($this->bits)) {
            return false;
        }
        return ((int) $this->bits[$bitNum] ? 1 : 0);
    }

    /**
     * Sets the value of a bit at a given position.
     *
     * @param int $bitNum The position of the bit.
     * @param int $bitVal The value to set (0 or 1).
     * @return bool Returns true on success.
     */
    public function setBit(int $bitNum, int $bitVal): bool {
        if ($bitVal < 0 || $bitVal > 1) {
            return false;
        }
        $this->bits[$bitNum] = $bitVal;
        return true;
    }

    /**
     * Retrieves the bit string.
     *
     * @return string The bit string.
     */
    public function getBits(): string {
        return $this->bits;
    }

    /**
     * Sets the bit string.
     *
     * @param string $bits The new bit string.
     * @return void
     */
    public function setBits(string $bits) {
        $this->bits = $bits;
    }

    /**
     * Retrieves the offset position.
     *
     * @return int The offset position.
     */
    public function getOffset(): int {
        return $this->offset;
    }

    /**
     * Sets the offset position.
     *
     * @param int $offset The new offset position.
     * @return bool Returns true if the offset position is valid, false otherwise.
     */
    public function setOffset(int $offset): bool {
        if ($offset < 0 || $offset > strlen($this->bits)) {
            return false;
        }
        $this->offset = $offset;
        return true;
    }
}
