<?php

/**
 *
 */
class D2ItemLocation {

    //location
    /**
     *
     */
    const STORED = 0;
    /**
     *
     */
    const EQUIPPED = 1;
    /**
     *
     */
    const BELT = 2;
    /**
     *
     */
    const CURSOR = 4;
    /**
     *
     */
    const SOCKET = 6;

}

/**
 *
 */
class D2ItemLocationStored {

    //storage
    /**
     *
     */
    const NONE = 0;
    /**
     *
     */
    const INVENTORY = 1;
    /**
     *
     */
    const CUBE = 4;
    /**
     *
     */
    const STASH = 5;

}

/**
 *
 */
class D2ItemLocationBody {

    //body parts
    /**
     *
     */
    const HELMET = 1;
    /**
     *
     */
    const AMULET = 2;
    /**
     *
     */
    const ARMOR = 3;
    /**
     *
     */
    const WEAPONR = 4;
    /**
     *
     */
    const WEAPONL = 5;
    /**
     *
     */
    const RINGR = 6;
    /**
     *
     */
    const RINGL = 7;
    /**
     *
     */
    const BELT = 8;
    /**
     *
     */
    const BOOTS = 9;
    /**
     *
     */
    const GLOVES = 10;
    /**
     *
     */
    const WEAPONR2 = 11;
    /**
     *
     */
    const WEAPONL2 = 12;

}

class D2ItemQuality {
    const LOW_QUALITY = 1;
    const NORMAL = 2;
    const HIGH_QUALITY = 3;
    const MAGIC = 4;
    const SET = 5;
    const RARE = 6;
    const UNIQUE = 7;
    const CRAFTED = 8;
}
