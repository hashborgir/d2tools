<?php
/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
?>
<!doctype html>
<html lang="en">
    <?php
    /* Require the <head> section */
    require_once "head.php";
    ?>

    <body>
        <div class="container container-top bg_grad">
            <div style="">
                <div class="row">
                    <div class="col">

                        <img src="/img/Diablo2.png" style="float:right"><h1 syle="display:inline; font-weight: 900;"><?php echo $title . " " . $version; ?><span style="font-family: Lato !important; font-size: 14px;"> <?php echo " By" . $author ?></span></h1>
                        <div style="font-family: Lato !important; text-align:right; color: tomato">
                            Active Mod: <span style="color: purple"><?php echo $_SESSION['modname'] ?></span>
                            <span style="color: #8888FF">[<?php echo $_SESSION['path'] ?>]</span>
                        </div>
                        <ul class="nav nav-tabs" id="Tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="Unique-tab" data-toggle="tab" href="#Unique" role="tab" aria-controls="Unique" aria-selected="true">Unique Items</a>
                            </li>
                            <!--						<li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="Set-tab" data-toggle="tab" href="#Set" role="tab" aria-controls="Set" aria-selected="false">Set Items</a>
                                                    </li>-->
                            <!--						<li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="Gem-tab" data-toggle="tab" href="#Gem" role="tab" aria-controls="Set" aria-selected="false">Gems</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="Runewords-tab" data-toggle="tab" href="#Runewords" role="tab" aria-controls="Set" aria-selected="false">Runewords</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link" id="Cube-tab" data-toggle="tab" href="#Cube" role="tab" aria-controls="Set" aria-selected="false">Cube</a>
                                                    </li>
                            -->
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Chars-tab" data-toggle="tab" href="#Chars" role="tab" aria-controls="Chars" aria-selected="false">Character Editor</a>
                            </li>

                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Doc-tab" data-toggle="tab" href="#Doc" role="tab" aria-controls="Doc" aria-selected="false">Documentation Generator</a>
                            </li>

                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="Debug-tab" data-toggle="tab" href="#Debug" role="tab" aria-controls="Debug" aria-selected="false">Debug</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="about-tab" data-toggle="tab" href="#About" role="tab" aria-controls="About" aria-selected="false">About</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content" id="TabContent">
                    <div class="tab-pane fade show active" id="Unique" role="tabpanel" aria-labelledby="Unique-tab">
                        <?php require_once 'tabs/UniqueItems.php'; ?>
                    </div>
                    <div class="tab-pane fade" id="Set" role="tabpanel" aria-labelledby="Set-tab">
                        <?php require_once 'tabs/SetItems.php'; ?>
                    </div>
                    <!--				<div class="tab-pane fade" id="Gem" role="tabpanel" aria-labelledby="Gem-tab">
                    <?php //require_once 'tabs/Gems.php'; ?>
                                    </div>
                                    <div class="tab-pane fade" id="Runewords" role="tabpanel" aria-labelledby="Runewords-tab">
                    <?php //require_once 'tabs/Runewords.php'; ?>
                                    </div>
                                    <div class="tab-pane fade" id="Cube" role="tabpanel" aria-labelledby="Cube-tab">
                    <?php //require_once 'tabs/Cube.php'; ?>
                                    </div>-->

                    <div class="tab-pane fade" id="Doc" role="tabpanel" aria-labelledby="Doc-tab">
                        <?php require_once 'tabs/Doc.php'; ?>
                    </div>
                    <div class="tab-pane fade" id="Chars" role="tabpanel" aria-labelledby="Chars-tab">
                        <?php require_once 'tabs/Chars.php'; ?>
                    </div>
                    <div class="tab-pane fade" id="Debug" role="tabpanel" aria-labelledby="Debug-tab">
                        <?php require_once 'tabs/Debug.php'; ?>
                    </div>
                    <div class="tab-pane fade" id="About" role="tabpanel" aria-labelledby="about-tab">
                        <?php require_once 'tabs/about.php'; ?>
                    </div>
                </div>

            </div>