<?php

/*

  Copyright (C) 2021 Hash Borgir

  This file is part of D2Modder

  Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that the
  following conditions are met:

 * Redistributions of source code must retain the above
  copyright notice, this list of conditions and the
  following disclaimer.

 * Redistributions in binary form must reproduce the above
  copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other
  materials provided with the distribution.

 * This software must not be used for commercial purposes
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY!

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * @param $var
 * @return void
 */
function ddump($var) {
    //echo "<pre>";
    var_dump($var);
    //echo "</pre>";
    // 'Content-Type: application/json');
    // echo json_encode($var, JSON_INVALID_UTF8_IGNORE | JSON_PRETTY_PRINT);
    die();
}

/**
 * @param $var
 * @return void
 */
function dump($var) {
    //echo "<pre>";
    var_dump($var);
    //echo "</pre>";
    //'Content-Type: application/json');
    //echo json_encode($var, JSON_INVALID_UTF8_IGNORE | JSON_PRETTY_PRINT);
}

/**
 * @param string $str
 * @return string
 */
function strtobits(string $str): string {
    $ret = "";
    for ($i = 0; $i < strlen($str); ++$i) {
        $ord = ord($str[$i]);
        for ($bitnum = 7; $bitnum >= 0; --$bitnum) {
            if ($ord & (1 << $bitnum)) {
                $ret .= "1";
            } else {
                $ret .= "0";
            }
        }
    }
    return $ret;
}

/**
 * @param string $hex
 * @return string
 */
function swapEndianness(string $hex) {
    return implode('', array_reverse(str_split($hex, 2)));
}

/**
 * @param int $n
 * @param int $p
 * @param bool $b
 * @return int
 */
function setBit(int $n, int $p, bool $b) {
    return ($b ? ($n | (1 << $p)) : ($n & ~(1 << $p)) );
}

/**
 * @param int $b
 * @param int $p
 * @return int
 */
function getBit(int $b, int $p) {
    return intval(($b & (1 << $p)) !== 0);
}

/**
 * Calculate D2S Checksum
 * @param $data
 * @return string
 */
function checksum($fileData) {
    $nSignature = 0;
    foreach ($fileData as $k => $byte) {
        if ($k == 12 || $k == 13 || $k == 14 || $k == 15) {
            $byte = 0;
        }
        $nSignature = ((($nSignature << 1) | ($nSignature >> 31)) + $byte & 0xFFFFFFFF);
    }
    return swapEndianness(str_pad(dechex($nSignature), 8, 0, STR_PAD_LEFT));
}

/**
 * @param string $filePath
 * @return string
 */
function checksumFix(string $filePath) {
    return trim(shell_exec("bin\d2scs.exe \"$filePath\""));
}

/**
 * Find the position of the Xth occurrence of a substring in a string
 * @param $haystack
 * @param $needle
 * @param $number integer > 0
 * @return int
 */
function strposX($haystack, $needle, $number) {
    if ($number == 1) {
        return strpos($haystack, $needle);
    } elseif ($number > 1) {
        return strpos($haystack, $needle, strposX($haystack, $needle, $number - 1) + strlen($needle));
    } else {
        return error_log('Error: Value for parameter $number is out of range');
    }
}

/**
 * @param string $bit
 * @return int
 */
function isBit(string $bit): int {
    return ((int) $bit ? 1 : 0 );
}

/**
 * @param $input
 * @return string
 */
function toBits($input): string {
    $output = '';
    if (is_string($input)) {
        foreach (str_split($input) as $i) {
            $output .= str_pad(decbin(ord($i)), 8, 0, STR_PAD_LEFT);
        }
        return $output;
    } else if (is_int($input)) {
        return str_pad(decbin($input), 8, 0, STR_PAD_LEFT);
    } else if (is_array($input)) {
        foreach ($input as $i) {
            $output .= tobits($i);
        }
        return $output;
    }
}

/**
 * @return void
 */
function print_mem() {
    /* Currently used memory */
    $mem_usage = memory_get_usage();

    /* Peak memory usage */
    $mem_peak = memory_get_peak_usage();

    echo 'D2Modder is now using: <strong>' . round($mem_usage / 1024 / 1024) . 'MB</strong> RAM.<br>';
    echo 'Peak usage: <strong>' . round($mem_peak / 1024 / 1024) . 'MB</strong> RAM.<br><br>';
}

/**
 * @param $src
 * @param $dst
 * @return void
 */
function rcopy($src, $dst) {
    // open the source directory
    $dir = opendir($src);
    // Make the destination directory if not exist
    @mkdir($dst);
    // Loop through the files in source directory
    while ($file = readdir($dir)) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if (is_dir($src . '/' . $file)) {
                rcopy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

/**
 * @param $src
 * @return void
 */
function rrmdir($src) {
    $dir = opendir($src);
    while (false !== ( $file = readdir($dir))) {
        if (( $file != '.' ) && ( $file != '..' )) {
            $full = $src . '/' . $file;
            if (is_dir($full)) {
                rrmdir($full);
            } else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}
