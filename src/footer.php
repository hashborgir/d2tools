<div class="container">
	<div class=" row" style="background: none;">
		<div class="offset-9 col-3" style="background: none;">
			<a style="font-weight: bold;" class="btn btn-info" href="/src/D2Config.php">Setup New Mod</a>
			<a style="font-weight: bold;" class="btn btn-warning" href="/switchMods.php">Switch Mods</a>
			<br><hr>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="theme" id="theme_0" type="radio" class="custom-control-input" value="0"> 
				<label for="theme_0" class="custom-control-label">Light</label>
			</div>
			<div class="custom-control custom-radio custom-control-inline">
				<input name="theme" id="theme_1" type="radio" class="custom-control-input" value="1"> 
				<label for="theme_1" class="custom-control-label">Dark</label>
			</div>			
			<input type="hidden" name="modname" value="<?php echo $_SESSION['modname'] ?>">

		</div>

		<span id="themeHelpBlock" class="form-text text-muted">Theme</span>
	</div>
</div>
<footer>
<!--	<h1 id="credits-">Credits:</h1>
	<p><a target="_blank" href="https://d2mods.info">Thanks Phrozen Keep! My favorite mod community since 2002!</a></p>-->
	<p style="text-align: center">Copyright HashCasper 2021 - All Rights Reserved</p>
    <p style="text-align: center"><?php print_mem(); ?></p>
</footer>