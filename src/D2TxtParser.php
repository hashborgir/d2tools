<?php

/*

	Copyright (C) 2021 Hash Borgir

	This file is part of D2Modder 

	Redistribution and use in source and binary forms, with
	or without modification, are permitted provided that the
	following conditions are met:

	* Redistributions of source code must retain the above
	  copyright notice, this list of conditions and the
	  following disclaimer.

	* Redistributions in binary form must reproduce the above
	  copyright notice, this list of conditions and the
	  following disclaimer in the documentation and/or other
	  materials provided with the distribution.

	* This software must not be used for commercial purposes 
	* without my consent. Any sales or commercial use are prohibited
	* without my express knowledge and consent.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY! 

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
	CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
	INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
	NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
	HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
	OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 *
 */
class D2TxtParser {

    /**
     * @var mixed
     */
    public $path = TXT_PATH;

    /**
     * @var
     */
    public $db;

    /**
     *
     */
    public function __construct() {
	}

    /**
     * @param $file
     * @return array
     */
    public function parseFile($file) {
		return $this->parseData($file);
	}

    /**
     * @param $file
     * @return array
     */
    function filterProps($file) {
		$data = $this->parseData($file);
		$propsToFilter = file(FILTER_PROPERTIES_FILE, FILE_IGNORE_NEW_LINES);
		foreach ($data as $d) {
			$allProps[] = $d['code'];
		}
		$filteredProps = array_diff($allProps, $propsToFilter);
		sort($filteredProps);
		return $filteredProps;
	}

    /**
     * @param $file
     * @return array
     */
    public function parseData($file) {
		$file = $this->path . $file;
		$rows = array_map(function ($v) {
			return str_getcsv($v, "\t");
		}, file($file));
		$header = array_shift($rows);		
		$data = null;
		foreach ($rows as $row) {
			$data[] = @array_combine($header, $row);
		}
		unset($rows);
		return $data;
	}
}
?>